package com.lrns123.pvpve.pvp;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.EventHandler;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pve.PvEArea;
import com.lrns123.pvpve.pvp.modules.PvPArenaGames;
import com.lrns123.pvpve.pvp.modules.PvPBuildMode;
import com.lrns123.pvpve.pvp.modules.PvPCooldown;
import com.lrns123.pvpve.pvp.modules.PvPRegions;
import com.lrns123.pvpve.pvp.modules.PvPToggle;
import com.lrns123.pvpve.pvp.modules.killtracker.PvPKillTracker;
import com.lrns123.pvpve.util.RegionType;

/**
 * PvP Manager - Handles management of PvP related functionality
 * 
 * @author Lourens
 * 
 */
public class PvPManager implements CommandExecutor, Listener {

	private List<PvPModule> modules = new ArrayList<PvPModule>();

	private final PvPvE plugin;
	private final PvPKillTracker killtracker;
	private final PvPRegions pvpregions;
	private final PvPBuildMode pvpbuildmode;

	public PvPManager(final PvPvE plugin) {
		this.plugin = plugin;

		plugin.getCommand("pvp").setExecutor(this);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		
		registerModule(new PvPCooldown());
		pvpregions = new PvPRegions();
		registerModule(pvpregions);
		registerModule(new PvPToggle());
		killtracker = new PvPKillTracker();
		registerModule(killtracker);
		pvpbuildmode = new PvPBuildMode(plugin, pvpregions);
		registerModule(pvpbuildmode);
		// Delay registration by a tick to allow all components to load first		
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			
			@Override
			public void run() {
				registerModule(new PvPArenaGames(plugin));
			}
		});
		
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			
			@Override
			public void run() {
				// Re-check players in build mode
			    HashMap<String, PvEArea> buildMode = pvpbuildmode.buildMode;
				for (String Sply : buildMode.keySet()) {
					Player ply = Bukkit.getPlayerExact(Sply);
					if(ply != null && ply.isOnline()) {
						PvEArea pvearea = pvpbuildmode.buildMode.get(ply.getName());
						if(pvearea.getRegionType() == RegionType.CUBOID) {
	                        if(!pvearea.getBoundingBox().containsLocation(ply.getLocation())) {
	                            pvpbuildmode.buildMode.remove(ply.getName());
	                            ply.sendMessage(ChatColor.RED + "You steped out of your arena Build mode exited!");
	                        }
						} else {
						    Point p = new Point(ply.getLocation().getBlockX(), ply.getLocation().getBlockZ());
						    if(!pvearea.getPolygonRegion().containsLocation(p)) {
						        if(!pvearea.getPolygonRegion().insideLimit(ply.getLocation())) {
						            pvpbuildmode.buildMode.remove(ply.getName());
	                                ply.sendMessage(ChatColor.RED + "You steped out of your arena Build mode exited!");
						        }
						    }
						}
					}
				}
			}
		}, 1200, 1200);
	}

	/**
	 * Registers a PvP module
	 * 
	 * @param module
	 *            the module to register
	 */
	public void registerModule(PvPModule module) {
		modules.add(module);
		module.onLoad(this, plugin);
	}

	/**
	 * Shuts down the PvP modules
	 */
	public void shutdown() {
		for (PvPModule module : modules)
			module.onShutdown(this, plugin);
	}
	
	private PvPAction updateAction(PvPAction currentAction, PvPAction newAction)
	{
		return newAction.getPriority() > currentAction.getPriority() ? newAction : currentAction;
	}

	/**
	 * Returns whether or not the specified players can attack each other
	 * 
	 * @param attacker
	 *            - The attacker
	 * @param victim
	 *            - The victim
	 * @return true if the players can attack each other, false otherwise
	 */
	public boolean canAttackEachother(Player attacker, Player victim, Event event) {
			PvPAction attackerAction = PvPAction.Deny;
			PvPAction victimAction = PvPAction.Deny;
			
			// Check individual PvP states first
			for (PvPModule module : modules)
			{
				PvPAction response = module.canPvP(attacker);
				attackerAction = updateAction(attackerAction, response);
				
				response = module.canPvP(victim);
				victimAction = updateAction(victimAction, response);
			}
			
			if (attackerAction == PvPAction.Never || victimAction == PvPAction.Never)
				return false;	// Never cannot be overridden
			
			// Pick lowest priority
			PvPAction finalAction = attackerAction.getPriority() < victimAction.getPriority() ? attackerAction : victimAction;
			
			// Check for overrides
				for (PvPModule module : modules)
				{
					PvPAction response = module.canAttackEachother(attacker, victim, event);
					finalAction = updateAction(finalAction, response);
				}

				for (PvPModule module : modules)
				{
					module.postProcessEvent(finalAction, attacker, victim, event);
				}
				
			

			if (finalAction == PvPAction.Allow)
				return true;
			
			return false;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pvp")) {
			if (!(sender instanceof Player)) {
				return true;
			}

			final Player ply = (Player) sender;

			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reload")) {
					if (ply.hasPermission("pvpve.admin")) {
						for (PvPModule module : modules)
							module.onReload(this, plugin);

						sender.sendMessage(ChatColor.BLUE + "PvP modules reloaded");
					} else {
						sender.sendMessage(ChatColor.RED + "You do not have permission to use this command");
					}
					return true;
				}
				
				for (PvPModule module : modules)
				{
					module.processCommand(sender, cmd, label, args);
				}
				
				return true;
			} else {
				sender.sendMessage("Not yet implemented");
				return true;
			}
		}
		return false;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityDamage(EntityDamageEvent ev) {
		if (ev.getEntity() instanceof Player) {
			Player victim = (Player) ev.getEntity();

			if (ev instanceof EntityDamageByEntityEvent) {
				Player damager = null;
				EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) ev;

				if (event.getDamager() instanceof Player) {
					damager = (Player) event.getDamager();
				} else if (event.getDamager() instanceof Projectile) {
					Projectile proj = (Projectile) event.getDamager();
					if (proj.getShooter() instanceof Player)
						damager = (Player) proj.getShooter();
				}

				if (victim != null && damager != null) {				
					if (!canAttackEachother(damager, victim, ev)) {
						ev.setCancelled(true);
					}
				
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityCombustByEntity(EntityCombustByEntityEvent ev) {
		if (ev.getEntity() instanceof Player && ev.getCombuster() instanceof Projectile) {
			Projectile proj = (Projectile)ev.getCombuster();
			if (proj.getShooter() instanceof Player) {
				
				if (!canAttackEachother((Player) proj.getShooter(), (Player) ev.getEntity(), ev)) {
					ev.setCancelled(true);
				}

			}
		}
	}

	/**
	 * Returns the kill tracker class
	 * 
	 * @return the kill tracker class
	 */
	public PvPKillTracker getKillTracker() {
		return killtracker;
	}

}
