package com.lrns123.pvpve.pvp;

/**
 * Specifies the possible actions to take in the event of PvP fighting
 * 
 * @author Lourens
 * 
 * @note Different PvP modules can override each other based on their response.
 *       The order of priority is: Never, Allow, Deny, Default
 */

public enum PvPAction {

	/**
	 * Allow the players to attack each other
	 */
	Allow(1),

	/**
	 * Prevents the players from attacking each other
	 */
	Deny(0),

	/**
	 * Prevent prevent players from attacking each other under any circumstance,
	 * overrides all other PvP module
	 */
	Never(2),

	/**
	 * Use default behavior
	 */
	Default(-1);
	
	
	private final int priority;
	private PvPAction(int priority)
	{
		this.priority = priority;
	}
	
	public int getPriority()
	{
		return priority;	
	}
	
	public String getString(PvPAction pvp) {
		switch (pvp) {
		case Allow:
				return "Allow";
		case Deny:
				return "Deny";
		case Never:
				return "Never";
		case Default:
				return "Default";
		default:
			break;
		}
		return "none";
	}

}
