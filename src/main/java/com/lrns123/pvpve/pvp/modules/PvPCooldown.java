package com.lrns123.pvpve.pvp.modules;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;

public class PvPCooldown implements PvPModule {

	/**
	 * Stores the PvP cooldown timers for players
	 */
	private HashMap<Player, Long> cooldownTimers = new HashMap<Player, Long>();
	
	@Override
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
		return PvPAction.Default;
	}

	@Override
	public PvPAction canPvP(Player player) {
		if (cooldownTimers.containsKey(player)) {
			if ( (System.currentTimeMillis() / 1000) - cooldownTimers.get(player) < 5 )
				return PvPAction.Allow;
		}
		return PvPAction.Default;
	}

	@Override
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {
		if (action == PvPAction.Allow) {
			cooldownTimers.put(attacker, System.currentTimeMillis() / 1000);
			cooldownTimers.put(victim, System.currentTimeMillis() / 1000);
		}
	}

	@Override
	public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {
		return false;
	}

	@Override
	public void onLoad(PvPManager manager, PvPvE plugin) {
		cooldownTimers.clear();
	}

	@Override
	public void onReload(PvPManager manager, PvPvE plugin) {}

	@Override
	public void onShutdown(PvPManager manager, PvPvE plugin) {}

	@Override
	public String moduleName() {
		return "PvPCooldown";
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent ev)
	{
		cooldownTimers.remove(ev.getPlayer());
	}
}
