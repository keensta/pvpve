package com.lrns123.pvpve.pvp.modules;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.GameManager;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;

public class PvPArenaGames implements PvPModule {

	private final GameManager gameManager;

	public PvPArenaGames(PvPvE plugin) {
		this.gameManager = plugin.getGameManager();
	}
	
	@Override
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
		Game attackerGame = gameManager.getPlayerGame(attacker);
		Game victimGame = gameManager.getPlayerGame(victim);
		
		if (attackerGame != null || victimGame != null) {
			if (attackerGame == victimGame) {
				if (attackerGame.canAttackEachother(attacker, victim)) {
					return PvPAction.Allow;
				} else {
					return PvPAction.Never;
				}
			} else {
				return PvPAction.Never;
			}
		}
		
		return PvPAction.Default;
	}

	@Override
	public PvPAction canPvP(Player player) {
		return PvPAction.Default;
	}

	@Override
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {
	
	}

	@Override
	public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {

		return false;
	}

	@Override
	public void onLoad(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public void onReload(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public void onShutdown(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public String moduleName() {
		return "PvPArenaGames";
	}

}
