package com.lrns123.pvpve.pvp.modules.killtracker;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.db.DBManager;
import com.lrns123.pvpve.db.DBTransaction;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;
import com.lrns123.pvpve.util.Region;

public class PvPKillTracker implements Listener, PvPModule {

	private PvPvE plugin;
	private DBManager dbManager;
	private HashMap<String, KDPlayerRecord> playerRecords = new HashMap<String, KDPlayerRecord>();

	/**
	 * Stores a list of regions where kills are counted by the kill tracker
	 */
	private HashMap<String, List<Region>> trackedAreas = new HashMap<String, List<Region>>();

	/**
	 * Stores a list of worlds and whether tracking is globally enabled on them
	 */
	private HashMap<String, Boolean> trackedWorlds = new HashMap<String, Boolean>();

	public void prepareTables()
	{
		if (!dbManager.hasConnection())
			return;
		
		Connection conn = null;
		try {
			conn = dbManager.getConnection();
			DatabaseMetaData meta = conn.getMetaData();
			
			if (!meta.getTables(null, null, "pvpve_killtracker", null).next())
			{
				Statement stat = conn.createStatement();
				plugin.getLogger().info("Creating table 'pvpve_killtracker'");
				stat.execute("CREATE TABLE `pvpve_killtracker` (name VARCHAR(32) NOT NULL UNIQUE, kills SMALLINT NOT NULL, deaths SMALLINT NOT NULL)");
				stat.close();
			}
			
			if (!meta.getTables(null, null, "pvpve_pvplogs", null).next())
			{
				Statement stat = conn.createStatement();
				plugin.getLogger().info("Creating table 'pvpve_pvplogs'");
				stat.execute("CREATE TABLE `pvpve_pvplogs` (id INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT, killer VARCHAR(32) NOT NULL, victim VARCHAR(32) NOT NULL, type TINYINT UNSIGNED NOT NULL, weapon SMALLINT UNSIGNED NOT NULL, damage SMALLINT UNSIGNED NOT NULL, meta VARCHAR(64) NOT NULL)");
				stat.close();
			}
						
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	private void prepareCache() {
		playerRecords.clear();
		
		if (!dbManager.hasConnection())
			return;
		
		Connection conn = null;
		try {
			conn = dbManager.getConnection();
			Statement stat = conn.createStatement();
			ResultSet result = stat.executeQuery("SELECT `name`, `kills`, `deaths` FROM `pvpve_killtracker`");
			while (result.next())
			{
				KDPlayerRecord record = new KDPlayerRecord(result.getString(1));
				record.setKills(result.getInt(2));
				record.setDeaths(result.getInt(3));
				
				playerRecords.put(record.getPlayerName(), record);
			}
			result.close();
			stat.close();			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}

	/**
	 * Returns whether or not the area is a tracked area, and if kills should be
	 * counted there
	 * 
	 * @param ply
	 * @return true if the player is in a tracked area, false otherwise
	 */
	private boolean isInTrackedArea(Player ply) {
		String world = ply.getLocation().getWorld().getName();
		if (trackedWorlds.containsKey(world)) {
			if (trackedWorlds.get(world) == true)
				return true;
		}

		if (trackedAreas.containsKey(world)) {
			for (Region reg : trackedAreas.get(world)) {
				if (reg.containsLocation(ply.getLocation()))
					return true;
			}
		}

		return false;
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent ev) {
		Player victim = null;
		Player killer = null;

		ItemStack item = null;
		Projectile projectile = null;

		double damage = 0;

		if (ev.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent ev2 = (EntityDamageByEntityEvent) ev.getEntity().getLastDamageCause();

			damage = ev2.getDamage();
			victim = (Player) ev.getEntity();

			if (ev2.getDamager() instanceof Player) {
				killer = (Player) ev2.getDamager();
				item = killer.getItemInHand();
			} else if (ev2.getDamager() instanceof Projectile) {
				Projectile proj = (Projectile) ev2.getDamager();
				projectile = proj;
				if (proj.getShooter() instanceof Player)
					killer = (Player) proj.getShooter();
			}
		}

		if (killer != null && victim != null && killer != victim) {

			if (!isInTrackedArea(victim))
				return;

			final KDKillRecord killRecord;
			if (item != null)
				killRecord = new KDKillRecord(killer, victim, item, damage);
			else
				killRecord = new KDKillRecord(killer, victim, projectile, damage);

			dbManager.queueTransaction(new DBTransaction() {
				
				@Override
				public void executeQuery(Connection conn) {
					PreparedStatement stat;
					try {
						stat = conn.prepareStatement("INSERT INTO `pvpve_pvplogs` (`killer`, `victim`, `type`, `weapon`, `damage`, `meta`) VALUES (?,?,?,?,?,?)");
						stat.setString(1, killRecord.getAttacker());
						stat.setString(2, killRecord.getVictim());
						stat.setInt(3, killRecord.getType());
						stat.setInt(4, killRecord.getId());
						stat.setDouble(5, killRecord.getDamage());
						stat.setString(6, killRecord.getMetaData());
						stat.execute();
						stat.close();
					} catch (SQLException e) {
						plugin.getLogger().severe("Failed to add kill record");
						e.printStackTrace();
					}					
				}
			});

			
			// Process killer
			final KDPlayerRecord killerKDRecord;			
			if (playerRecords.containsKey(killer.getName())) {
				killerKDRecord = playerRecords.get(killer.getName());
				killerKDRecord.addKill();				
			} else {
				killerKDRecord = new KDPlayerRecord(killer.getName());
				killerKDRecord.addKill();
				playerRecords.put(killer.getName(), killerKDRecord);
			}
			
			// Process victim
			final KDPlayerRecord victimKDRecord;			
			if (playerRecords.containsKey(victim.getName())) {
				victimKDRecord = playerRecords.get(victim.getName());
				victimKDRecord.addDeath();
			} else {
				victimKDRecord = new KDPlayerRecord(victim.getName());
				victimKDRecord.addDeath();
				playerRecords.put(victim.getName(), victimKDRecord);
			}
			
			// Handle tokens
			plugin.getTokenManager().addTokens(killer.getName(), 10);
			plugin.getTokenManager().removeTokens(victim.getName(), 10);
			
			dbManager.queueTransaction(new DBTransaction() {
				
				@Override
				public void executeQuery(Connection conn) {
					PreparedStatement stat;
					try {
						stat = conn.prepareStatement("INSERT INTO `pvpve_killtracker` (`name`, `kills`, `deaths`) VALUES (?,?,?) ON DUPLICATE KEY UPDATE `kills`=?, `deaths`=?");
						stat.setString(1, killerKDRecord.getPlayerName());
						stat.setInt(2, killerKDRecord.getKills());
						stat.setInt(3, killerKDRecord.getDeaths());
						stat.setInt(4, killerKDRecord.getKills());
						stat.setInt(5, killerKDRecord.getDeaths());
						stat.execute();
						stat.close();
						
						stat = conn.prepareStatement("INSERT INTO `pvpve_killtracker` (`name`, `kills`, `deaths`) VALUES (?,?,?) ON DUPLICATE KEY UPDATE `kills`=?, `deaths`=?");
						stat.setString(1, victimKDRecord.getPlayerName());
						stat.setInt(2, victimKDRecord.getKills());
						stat.setInt(3, victimKDRecord.getDeaths());
						stat.setInt(4, victimKDRecord.getKills());
						stat.setInt(5, victimKDRecord.getDeaths());
						stat.execute();
						stat.close();
					} catch (SQLException e) {
						plugin.getLogger().severe("Failed to update kill records");
						e.printStackTrace();
					}				
				}
			});
		}
	}

	public int getKills(String playerName) {
		if (playerRecords.containsKey(playerName))
			return playerRecords.get(playerName).getKills();
		else
			return 0;
	}

	public int getDeaths(String playerName) {
		if (playerRecords.containsKey(playerName))
			return playerRecords.get(playerName).getDeaths();
		else
			return 0;
	}

	@Override
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
		return PvPAction.Default;
	}

	@Override
	public PvPAction canPvP(Player player) {
		return PvPAction.Default;
	}

	@Override
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {
	}

	@Override
	public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player))
			return true;
		if (args.length > 0 && args[0].equalsIgnoreCase("kd")) {
			if (playerRecords.containsKey(sender.getName())) {
				KDPlayerRecord record = playerRecords.get(sender.getName());
				sender.sendMessage(ChatColor.BLUE + "PvP kills: " + record.getKills());
				sender.sendMessage(ChatColor.BLUE + "PvP deaths: " + record.getDeaths());
				sender.sendMessage(ChatColor.BLUE + "PvP K/D ratio: " + (record.getDeaths() == 0 ? record.getKills() : (double) record.getKills() / (double) record.getDeaths()));
			} else {
				sender.sendMessage(ChatColor.RED + "No PvP kill records found");
			}
			return true;
		}
		return false;
	}

	@Override
	public void onLoad(PvPManager manager, PvPvE plugin) {		
		this.dbManager = plugin.getDBManager();
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		prepareTables();
		prepareCache();
		loadConfig(plugin);
	}

	private void loadConfig(PvPvE plugin) {
		trackedWorlds.clear();
		
		FileConfiguration config = plugin.loadConfig("pvp.yml", true);
		if (config == null)
			return;
		
		if (!config.contains("worlds")) {
			return;
		}
		Set<String> worlds = config.getConfigurationSection("worlds").getKeys(false);

		for (String world : worlds) {
			ConfigurationSection worldconfig = config.getConfigurationSection("worlds." + world);

			if (worldconfig.getBoolean("trackAll", false)) {
				trackedWorlds.put(world, true);
			} else {
				trackedWorlds.put(world, false);

				List<Region> trackedRegions = new ArrayList<Region>();

				Set<String> areas;
				if (worldconfig.getConfigurationSection("trackedAreas") != null) {
					areas = worldconfig.getConfigurationSection("trackedAreas").getKeys(false);

					for (String area : areas) {

						List<Integer> coords = worldconfig.getIntegerList("trackedAreas." + area);

						if (coords.size() != 6)
							continue;

						Vector min = new Vector(coords.get(0), coords.get(1), coords.get(2));
						Vector max = new Vector(coords.get(3), coords.get(4), coords.get(5));

						trackedRegions.add(new Region(world, min, max));
					}
				}
				trackedAreas.put(world, trackedRegions);
			}
		}

	}

	@Override
	public void onReload(PvPManager manager, PvPvE plugin) {
		loadConfig(plugin);
	}

	@Override
	public void onShutdown(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public String moduleName() {
		return "PvPKillTracker";
	}

}
