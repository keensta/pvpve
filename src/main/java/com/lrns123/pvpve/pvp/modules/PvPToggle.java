package com.lrns123.pvpve.pvp.modules;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;

public class PvPToggle implements PvPModule, Listener {

	/**
	 * Stores whether or not PvP has been explicitly enabled by this player (using the /pvp command)
	 */
	private HashMap<Player, Boolean> playerPvPEnabled = new HashMap<Player, Boolean>();
	
	@Override
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
		return PvPAction.Default;
	}

	@Override
	public PvPAction canPvP(Player player) {
		if (playerPvPEnabled.containsKey(player)) {
			if (playerPvPEnabled.get(player) == true)
				return PvPAction.Allow;
		}
		return PvPAction.Deny;
	}

	@Override
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {}

	@Override
	public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1)
			return false;
		
		if (!(sender instanceof Player))
			return false;
		
		Player ply = (Player)sender;
		
		if (args[0].equalsIgnoreCase("toggle")) {
			if (ply.hasPermission("pvpve.togglepvp"))
			{
				if (playerPvPEnabled.get(ply)) {
					sender.sendMessage(ChatColor.BLUE + "PvP mode disabled");
					playerPvPEnabled.put(ply, false);
				} else {
					sender.sendMessage(ChatColor.BLUE + "PvP mode enabled");
					playerPvPEnabled.put(ply, true);
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission to use this command");
			}
		} else if (args[0].equalsIgnoreCase("on")) {
			if (ply.hasPermission("pvpve.togglepvp"))
			{
				if (playerPvPEnabled.get(ply)) {
					sender.sendMessage(ChatColor.BLUE + "PvP mode already enabled");
				} else {
					sender.sendMessage(ChatColor.BLUE + "PvP mode enabled");
					playerPvPEnabled.put(ply, true);
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission to use this command");
			}
		} else if (args[0].equalsIgnoreCase("off")) {
			if (ply.hasPermission("pvpve.togglepvp"))
			{
				if (!playerPvPEnabled.get(ply)) {
					sender.sendMessage(ChatColor.BLUE + "PvP mode already disabled");
				} else {
					sender.sendMessage(ChatColor.BLUE + "PvP mode disabled");
					playerPvPEnabled.put(ply, false);
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission to use this command");
			}
		}  else {
			return false;
		}
		return true;
	}

	@Override
	public void onLoad(PvPManager manager, PvPvE plugin) {
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		for (Player ply : Bukkit.getOnlinePlayers())
		{
			playerPvPEnabled.put(ply, false);
		}
	}

	@Override
	public void onReload(PvPManager manager, PvPvE plugin) {}

	@Override
	public void onShutdown(PvPManager manager, PvPvE plugin) {}

	@Override
	public String moduleName() {
		return "PvPToggle";
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent ev)
	{
		playerPvPEnabled.put(ev.getPlayer(), false);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent ev)
	{
		playerPvPEnabled.remove(ev.getPlayer());
	}
}
