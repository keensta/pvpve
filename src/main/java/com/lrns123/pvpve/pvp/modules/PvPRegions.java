package com.lrns123.pvpve.pvp.modules;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.util.Vector;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;
import com.lrns123.pvpve.util.PolygonRegion;
import com.lrns123.pvpve.util.Region;

public class PvPRegions implements PvPModule {
	
	/**
	 * Stores a list of PvP Areas, grouped by world name
	 */
	private HashMap<String, List<Region>> pvpAreas = new HashMap<String, List<Region>>();
	private HashMap<String, List<PolygonRegion>> polygonPvpAreas = new HashMap<String, List<PolygonRegion>>();
	
	/**
	 * Stores a list of No-PvP areas, grouped by world name 
	 */
	private HashMap<String, List<Region>> nopvpAreas = new HashMap<String, List<Region>>();
	
	/**
	 * Stores a list of worlds whose PvP state is controlled by PvPvE, and whether PvP is enabled there by default
	 */
	private HashMap<String, Boolean> worldPvpEnabled = new HashMap<String, Boolean>();
	
	@Override
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
		return PvPAction.Default;
	}

	@Override
	public PvPAction canPvP(Player player) {
		String world = player.getLocation().getWorld().getName();
		Location loc = player.getLocation();
		Point p = new Point(loc.getBlockX(), loc.getBlockZ());
		
		// Check for non-PvP zones. These disable PvP in all circumstances
		if (nopvpAreas.containsKey(world))
		{
			for (Region reg : nopvpAreas.get(world))
			{
				if (reg.containsLocation(player.getLocation()))
					return PvPAction.Never;
			}
		}
		
		if (worldPvpEnabled.containsKey(world)) {
			if (worldPvpEnabled.get(world)) {
				return PvPAction.Allow;
			} else {
				// PvP is disabled, first check for PvP zones and player state
				if (pvpAreas.containsKey(world))
				{
					for (Region reg : pvpAreas.get(world))
					{
						if (reg.containsLocation(player.getLocation()))
							return PvPAction.Allow;
					}
				}
				if(polygonPvpAreas.containsKey(world))
				{
				    for(PolygonRegion reg : polygonPvpAreas.get(world))
				    {
				        if(reg.containsLocation(p))
				        {
				            if(reg.insideLimit(loc))
				                return PvPAction.Allow;
				        }
				    }
				}
				return PvPAction.Deny;
			}		
		} else {
			// World is not managed by PvPvE, fall back to default PvP setting
			if (!player.getLocation().getWorld().getPVP()) {
				return PvPAction.Deny;
			}
			return PvPAction.Allow;
		}
	}

	@Override
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {
			
	}

	@Override
	public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLoad(PvPManager manager, PvPvE plugin) {
	
		pvpAreas.clear();
		nopvpAreas.clear();
				
		FileConfiguration config = plugin.loadConfig("pvp.yml", true);
		
		if (config == null)
			return;
		
		if (!config.contains("worlds")) {
			plugin.getLogger().severe("No worlds defined!");
			return;
		}
		Set<String> worlds = config.getConfigurationSection("worlds").getKeys(false);
		
		for (String world : worlds)
		{
			ConfigurationSection worldconfig = config.getConfigurationSection("worlds." + world);
			
			worldPvpEnabled.put(world, worldconfig.getBoolean("pvpEnabled"));
						
			Set<String> areas;
			if (worldconfig.getConfigurationSection("pvpAreas") != null) {
				areas = worldconfig.getConfigurationSection("pvpAreas").getKeys(false);
				
				List<Region> pvpRegions = new ArrayList<Region>();
				List<PolygonRegion> polygonRegions = new ArrayList<PolygonRegion>();
				
				for(String area : areas)
				{
					plugin.getLogger().info("Loading PvP area " + area);
					String regionType = worldconfig.getString("pvpAreas." + area + ".regiontype");
					
					if(regionType.equalsIgnoreCase("square")) {
	                    List<Integer> coords = worldconfig.getIntegerList("pvpAreas." + area + ".bbox");
	                    
	                    if (coords.size() != 6)
	                        continue;
	                    
	                    Vector min = new Vector(coords.get(0), coords.get(1), coords.get(2));
	                    Vector max = new Vector(coords.get(3), coords.get(4), coords.get(5));
	                    pvpRegions.add(new Region(world, min, max));
					} else {
					    List<String> points = worldconfig.getStringList("pvpAreas." + area + ".points");
		                if(points == null || points.size() < 2) {
		                    plugin.getLogger().severe("PvE area '" + area + "' has an invalid polygon region, ignoring...");
		                    continue;
		                }

		                int celling = worldconfig.getInt("pvpAreas." + area + ".celling");
		                int floor = worldconfig.getInt("pvpAreas." + area + ".floor");

		                if(celling == 0 || floor == 0) {
		                    plugin.getLogger().severe("PvE area '" + area + "' has an invalid polygon region, ignoring...");
		                    continue;
		                }
		                
		                PolygonRegion pr = new PolygonRegion(points);
		                pr.setCelling(celling);
		                pr.setFloor(floor);
		                
		                polygonRegions.add(pr);
					}
					
				}
				
				pvpAreas.put(world, pvpRegions);
				polygonPvpAreas.put(world, polygonRegions);
			}
			

			
			if (worldconfig.getConfigurationSection("nopvpAreas") != null) {
				areas = worldconfig.getConfigurationSection("nopvpAreas").getKeys(false);
				
				List<Region> nonpvpRegions = new ArrayList<Region>();
				for(String area : areas)
				{
					List<Integer> coords = worldconfig.getIntegerList("nopvpAreas." + area);
					
					if (coords.size() != 6)
						continue;
					
					Vector min = new Vector(coords.get(0), coords.get(1), coords.get(2));
					Vector max = new Vector(coords.get(3), coords.get(4), coords.get(5));
					nonpvpRegions.add(new Region(world, min, max));
				}
				
				nopvpAreas.put(world, nonpvpRegions);
			}
		}
	}

	@Override
	public void onReload(PvPManager manager, PvPvE plugin) {
		onLoad(manager, plugin);
	}

	@Override
	public void onShutdown(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public String moduleName() {
		return "PvPRegions";
	}
	
	public HashMap<String, List<Region>> getPvPAreas() {
		return pvpAreas;
	}

	public HashMap<String, List<PolygonRegion>> getPolygonAreas() {
	    return polygonPvpAreas;
	}
	
}
