package com.lrns123.pvpve.pvp.modules.killtracker;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class KDPlayerRecord {
	private String playerName;
	private int kills = 0;
	private int deaths = 0;

	public KDPlayerRecord(String name)
	{
		this.setPlayerName(name);
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerName() {
		return playerName;
	}
	
	public Player getPlayer()
	{
		return Bukkit.getPlayerExact(playerName);
	}
	
	public void addKill()
	{
		this.kills++;
	}

	public void addDeath()
	{
		this.deaths++;
	}
	
	public void setKills(int kills) {
		this.kills = kills;
	}

	public int getKills() {
		return kills;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getDeaths() {
		return deaths;
	}
}
