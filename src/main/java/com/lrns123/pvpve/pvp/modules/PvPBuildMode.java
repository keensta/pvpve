package com.lrns123.pvpve.pvp.modules;

import java.awt.Point;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pve.PvEArea;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;
import com.lrns123.pvpve.util.PolygonRegion;
import com.lrns123.pvpve.util.Region;
import com.lrns123.pvpve.util.RegionType;

public class PvPBuildMode implements PvPModule, Listener {

    private PvPvE plugin;
    private PvPRegions pvpregions;
    public HashMap<String, PvEArea> buildMode = new HashMap<String, PvEArea>();
    public HashMap<String, HashMap<PvEArea, Integer>> pendingBuildMode = new HashMap<String, HashMap<PvEArea, Integer>>();

    public PvPBuildMode(PvPvE plugin, PvPRegions pvpregions) {
        this.plugin = plugin;
        this.pvpregions = pvpregions;
        
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
        return PvPAction.Default;
    }

    @Override
    public PvPAction canPvP(Player player) {
        Player ply = player;

        if(pendingBuildMode.containsKey(ply.getName())) {
            for(int id : pendingBuildMode.get(ply.getName()).values()) {
                Bukkit.getScheduler().cancelTask(id);
            }
            ply.sendMessage(ChatColor.RED + "You have attacked or been attacked. Build mode will not be activated!");
            pendingBuildMode.remove(ply.getName());
            return PvPAction.Default;
        } else {
            if(buildMode.containsKey(ply.getName()))
                return PvPAction.Never;

            return PvPAction.Default;
        }
    }

    @Override
    public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) {
            return true;
        }

        final Player ply = (Player) sender;

        if(args[0].equalsIgnoreCase("build")) {
            if(pendingBuildMode.containsKey(ply.getName())) {
                ply.sendMessage(ChatColor.RED
                        + "You are either currently waiting for buildmode to activate or its already active");
                return true;
            }

            String world = ply.getLocation().getWorld().getName();
            HashMap<String, List<Region>> pvpAreas = pvpregions.getPvPAreas();
            HashMap<String, List<PolygonRegion>> polygonPvpAreas = pvpregions.getPolygonAreas();
            if(pvpAreas.containsKey(world)) {
                for(Region reg : pvpAreas.get(world)) {
                    if(reg.containsLocation(ply.getLocation())) {
                        if(!checkLocation(ply)) {
                            ply.sendMessage(ChatColor.RED + "You don't have permission for that here!");
                            return true;
                        }

                        final PvEArea pvearea = getArea(ply);

                        ply.sendMessage(ChatColor.BLUE + "You have 20 seconds before going into build mode!");
                        int taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                            @Override
                            public void run() {
                                buildMode.put(ply.getName(), pvearea);
                                pendingBuildMode.remove(ply.getName());
                                ply.sendMessage(ChatColor.BLUE + "Build mode active!");
                            }
                        }, 20 * 20);

                        if(pvearea == null) {
                            Bukkit.getScheduler().cancelTask(taskId);
                            return true;
                        }

                        HashMap<PvEArea, Integer> areaId = new HashMap<PvEArea, Integer>();
                        areaId.put(pvearea, taskId);
                        pendingBuildMode.put(ply.getName(), areaId);
                        return true;
                    }
                }
            }

            if(polygonPvpAreas.containsKey(world)) {
                Point p = new Point(ply.getLocation().getBlockX(), ply.getLocation().getBlockZ());
                for(PolygonRegion reg : polygonPvpAreas.get(world)) {
                    if(reg.containsLocation(p)) {
                        if(reg.insideLimit(ply.getLocation())) {
                            if(!checkLocation(ply)) {
                                ply.sendMessage(ChatColor.RED + "You don't have permission for that here!");
                                return true;
                            }

                            final PvEArea pvearea = getArea(ply);

                            ply.sendMessage(ChatColor.BLUE + "You have 20 seconds before going into build mode!");
                            int taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                                @Override
                                public void run() {
                                    buildMode.put(ply.getName(), pvearea);
                                    pendingBuildMode.remove(ply.getName());
                                    ply.sendMessage(ChatColor.BLUE + "Build mode active!");
                                }
                            }, 20 * 20);

                            if(pvearea == null) {
                                Bukkit.getScheduler().cancelTask(taskId);
                                return true;
                            }

                            HashMap<PvEArea, Integer> areaId = new HashMap<PvEArea, Integer>();
                            areaId.put(pvearea, taskId);
                            pendingBuildMode.put(ply.getName(), areaId);
                            return true;
                        }
                    }
                }
            }

            ply.sendMessage(ChatColor.RED + "You are not standing in a vaild area!");
        } else if(args[0].equalsIgnoreCase("exitbuild")) {
            if(buildMode.containsKey(ply.getName())) {
                buildMode.remove(ply.getName());
                ply.sendMessage(ChatColor.BLUE + "Build mode exited!");
            } else {
                ply.sendMessage(ChatColor.RED + "You are not in build mode!");
            }
        }
        return false;
    }

    private boolean checkLocation(Player ply) {
        Location loc = ply.getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());
        HashMap<String, PvEArea> pveAreas = plugin.getPvEManager().getPvEArea();
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    if(ply.hasPermission("pvpve.arenabuild." + area.getName())) {
                        return true;
                    }
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    private PvEArea getArea(Player ply) {
        Location loc = ply.getLocation();
        HashMap<String, PvEArea> pveAreas = plugin.getPvEManager().getPvEArea();
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    if(ply.hasPermission("pvpve.arenabuild." + area.getName())) {
                        return area;
                    }
                }
            } else {
                Point p = new Point(loc.getBlockX(), loc.getBlockZ());
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName())) {
                            return area;
                        }
                    }
                }
            }
            
        }
        return null;
    }

    @Override
    public void onLoad(PvPManager manager, PvPvE plugin) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onReload(PvPManager manager, PvPvE plugin) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onShutdown(PvPManager manager, PvPvE plugin) {
        // TODO Auto-generated method stub

    }

    @Override
    public String moduleName() {
        return "PvPBuildMode";
    }
    
    @EventHandler
    public void onMobTarget(EntityTargetLivingEntityEvent ev) { 
        if(ev.getTarget() instanceof Player) {
            Player p = (Player) ev.getTarget();
            if(buildMode.containsKey(p.getName())) {
                ev.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onMobTarget2(EntityTargetEvent ev) {
        if(ev.getTarget() instanceof Player) {
            Player p = (Player) ev.getTarget();
            if(buildMode.containsKey(p.getName())) {
                ev.setCancelled(true);
            }
        }
    }

}
