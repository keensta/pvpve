package com.lrns123.pvpve.pvp.modules;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pvp.PvPAction;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.pvp.PvPModule;

public class PvPMobDisguise implements Listener, PvPModule {
	
	@Override
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event) {
		/*if (MobDisguiseAPI.isDisguised(attacker))
		{
			return PvPAction.Never;
		}*/
		return PvPAction.Default;
	}

	@Override
	public PvPAction canPvP(Player player) {
		return PvPAction.Default;
	}

	@Override
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event) {
		
	}

	@Override
	public boolean processCommand(CommandSender sender, Command command, String label, String[] args) {
		return false;
	}

	@Override
	public void onLoad(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public void onReload(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public void onShutdown(PvPManager manager, PvPvE plugin) {
		
	}

	@Override
	public String moduleName() {
		return "PvPMobDisguise";
	}
	

}
