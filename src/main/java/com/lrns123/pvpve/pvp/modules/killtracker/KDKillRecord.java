package com.lrns123.pvpve.pvp.modules.killtracker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;

public class KDKillRecord {

	private final String attacker;
	private final String victim;
	private final int type;
	private final int id;
	private final String meta;
	private final double damage;
	
	@SuppressWarnings("deprecation")
    public KDKillRecord(Player attacker, Player victim, ItemStack weapon, double damage)
	{
		this.attacker = attacker.getName();
		this.victim = victim.getName();
		
		this.type = 1;
		this.id = weapon.getTypeId();
		this.damage = damage;
		
		StringBuilder meta = new StringBuilder();
		
		if (this.id == Material.POTION.getId()) {
			// Special case
			Potion potion = Potion.fromItemStack(weapon);
			for (PotionEffect effect : potion.getEffects())
			{
				meta.append(effect.getType().getId());
				meta.append(":");
				meta.append(effect.getDuration());
				meta.append(":");
				meta.append(effect.getAmplifier());
				meta.append(";");
			}
		} else {
			for (Enchantment ench : weapon.getEnchantments().keySet())
			{
				meta.append(ench.getId());
				meta.append(":");
				meta.append(weapon.getEnchantments().get(ench));
				meta.append(";");
			}
		}
		this.meta = meta.toString();		
	}
	
	@SuppressWarnings("deprecation")
    public KDKillRecord(Player attacker, Player victim, Projectile weapon, double damage)
	{
		this.attacker = attacker.getName();
		this.victim = victim.getName();

		
		this.type = 2;
		this.id = weapon.getType().getTypeId();
		this.damage = damage;
		
		StringBuilder meta = new StringBuilder();
		
		if (weapon instanceof Arrow) {
			// Try getting the enchantments from the bow of the attacker
			if (attacker.getItemInHand().getType() == Material.BOW)
			{
				for (Enchantment ench : attacker.getItemInHand().getEnchantments().keySet())
				{
					meta.append(ench.getId());
					meta.append(":");
					meta.append(attacker.getItemInHand().getEnchantments().get(ench));
					meta.append(";");
				}
			}
		} else if (weapon instanceof ThrownPotion) {
			ThrownPotion potion = (ThrownPotion)weapon;
			for (PotionEffect effect : potion.getEffects())
			{
				meta.append(effect.getType().getId());
				meta.append(":");
				meta.append(effect.getDuration());
				meta.append(":");
				meta.append(effect.getAmplifier());
				meta.append(";");
			}
		}
		
		this.meta = meta.toString();		
	}
	
	public int getType()
	{
		return type;
	}
	
	public String getAttacker()
	{
		return attacker;
	}
	
	public String getVictim()
	{
		return victim;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getMetaData()
	{
		return meta;
	}
	
	public double getDamage()
	{
		return damage;
	}
}
