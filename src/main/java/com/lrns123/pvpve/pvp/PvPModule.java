package com.lrns123.pvpve.pvp;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.lrns123.pvpve.PvPvE;

public interface PvPModule {
		
	/**
	 * Returns whether or not the specified two players can attack each other. This allows for a last-moment override of PvP behavior depending on the attacker and victim.
	 * 
	 * @param attacker The attacking player
	 * @param victim The attacked player
	 * @param event The event in question
	 * @return true if these two players can attack each other, false otherwise
	 */
	public PvPAction canAttackEachother(Player attacker, Player victim, Event event);
	
	
	/**
	 * Returns whether or not this player is in PvP mode
	 * 
	 * @param player The player whose PvP mode is to be returned
	 * @return true if this player is allowed to PvP, false otherwise
	 */
	public PvPAction canPvP(Player player);
	
	/**
	 * Allows post processing of events based on the action taken. Useful for doing things that should only happen if a PvP event is allowed.
	 * 
	 * @param action The final action taken
	 * @param attacker The attacker
	 * @param victim The victim
	 * @param event The event
	 */
	public void postProcessEvent(PvPAction action, Player attacker, Player victim, Event event);
	
	/**
	 * Allows modules to handle the /pvp command arguments
	 * @param sender Player (or console) that issued the command
	 * @param command The command that was issued
	 * @param label The command label
	 * @param args The command arguments
	 * @return true if the module handled the command, false otherwise
	 */
	public boolean processCommand(CommandSender sender, Command command, String label, String args[]);
	
	/**
	 * Called when the module is loaded
	 * @param manager
	 * @param plugin
	 */
	public void onLoad(PvPManager manager, PvPvE plugin);
	
	/**
	 * Called when the module is reloaded
	 * @param manager
	 * @param plugin
	 */
	public void onReload(PvPManager manager, PvPvE plugin);
	
	/**
	 * Called when the module is shut down
	 * @param manager
	 * @param plugin
	 */
	public void onShutdown(PvPManager manager, PvPvE plugin);
	
	/**
	 * Returns the name of the module
	 * @return
	 */
	public String moduleName();

}
