package com.lrns123.pvpve.util;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Region {
	private String world;
	private Vector min;
	private Vector max;
	
	
	public Region(String world, Vector min, Vector max)
	{
		this.world = world;
		this.min = min;
		this.max = max;
	}
	
	public boolean containsLocation(Location loc)
	{
		if (!this.world.equalsIgnoreCase(loc.getWorld().getName())) {
            return false;
        }
        if (!(loc.getBlockX() >= min.getBlockX() && loc.getBlockX() <= max.getBlockX())) {   
            return false;
        }
        if (!(loc.getBlockZ() >= min.getBlockZ() && loc.getBlockZ() <= max.getBlockZ())) {
            return false;
        }
        if (!(loc.getBlockY() >= min.getBlockY() && loc.getBlockY() <= max.getBlockY())) {
            return false;
        }
        return true;
	}
	
	
	// Skips the world check
	public boolean containsLocationFast(Location loc)
	{
        if (!(loc.getBlockX() >= min.getBlockX() && loc.getBlockX() <= max.getBlockX())) {   
            return false;
        }
        if (!(loc.getBlockZ() >= min.getBlockZ() && loc.getBlockZ() <= max.getBlockZ())) {
            return false;
        }
        if (!(loc.getBlockY() >= min.getBlockY() && loc.getBlockY() <= max.getBlockY())) {
            return false;
        }
        return true;
	}
	
	public String toString()
	{
		return "Region{" + world + "(" + min.toString() + "/" + max.toString() + ")}";
	}
	
}
