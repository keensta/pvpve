package com.lrns123.pvpve.util;

import java.awt.Point;
import java.awt.Polygon;
import java.util.List;

import org.bukkit.Location;

public class PolygonRegion {

	Polygon poly = new Polygon();
	int celling = 0;
	int floor = 0;

	public PolygonRegion(List<String> points) {
		if(points.isEmpty())
			return;
		
		for(String s : points) {
			String[] point = s.split(":");
			poly.addPoint(Integer.parseInt(point[0]), Integer.parseInt(point[1]));
		}
		
	}

	public boolean insideBounds(Point p) {
	    if(poly.getBounds().contains(p)) {
	        return true;
	    }
        return false;
	}
	
	public boolean containsLocation(Point p) {
		if(poly.getBounds().contains(p)) {
			if(poly.contains(p)) {
				return true;
			}
		}
		return false;
	}

	public boolean insideLimit(Location loc) {
		double plyY = loc.getY();

		if(!(plyY > celling) && !(plyY < floor)) {
			return true;
		}
		return false;
	}
	
	public void addPoint(Point p) {
		poly.addPoint(p.x, p.y);
	}
	
	/*
	public void removePoint(Point p) {
		Needs to be written.
	}*/
	
	public void setCelling(int c) {
		this.celling = c;
	}
	
	public void setFloor(int f) {
		this.floor = f;
	}
	
}
