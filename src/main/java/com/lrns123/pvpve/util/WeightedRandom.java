package com.lrns123.pvpve.util;

import java.util.Random;

public class WeightedRandom {
	
	private IWeightedItem[] options = null;

	public void setOptions(IWeightedItem[] options)
	{
		this.options = options;
	}
	
	public IWeightedItem getWeightedRandom()
	{
		int totalWeight = 0;
		for (IWeightedItem item : options)
		{
			totalWeight += item.getRandomWeight();
		}
		
		if (totalWeight == 0)
			return null;
		
		Random rn = new Random();
		
		int num = rn.nextInt(totalWeight) + 1;
	
		for (IWeightedItem item : options)
		{
			num -= item.getRandomWeight();
			if (num <= 0)
				return item;
			
		}
		return null;
	}
}
