package com.lrns123.pvpve.util;

import java.lang.reflect.Field;
import java.util.Map;

import net.minecraft.server.v1_7_R1.NBTTagCompound;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.craftbukkit.v1_7_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class Util {
	
	@SuppressWarnings("unchecked")
	public static ConfigurationSection mapToConfiguration(Map<?, ?> map) {		
		MemoryConfiguration memCfg = new MemoryConfiguration();
		memCfg.addDefaults((Map<String, Object>) map);

		return memCfg.getDefaultSection();
	}
	
	public static NBTTagCompound getItemTag(ItemStack item) {
		try {
			Field handleField = CraftItemStack.class.getDeclaredField("handle");
			handleField.setAccessible(true);
			return ((net.minecraft.server.v1_7_R1.ItemStack)handleField.get((CraftItemStack)item)).tag;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static NBTTagCompound getOrCreateItemTag(ItemStack item) {
		try {
			Field handleField = CraftItemStack.class.getDeclaredField("handle");
			handleField.setAccessible(true);
			net.minecraft.server.v1_7_R1.ItemStack s = (net.minecraft.server.v1_7_R1.ItemStack) handleField.get((CraftItemStack) item);
			if (s.tag == null) {
				s.tag = new NBTTagCompound();
			}
			return s.tag;
		} catch (Exception e) {
			return null;
		}
	}
}
