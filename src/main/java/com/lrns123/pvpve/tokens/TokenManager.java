package com.lrns123.pvpve.tokens;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.db.DBManager;
import com.lrns123.pvpve.db.DBTransaction;
import com.lrns123.pvpve.tokens.shops.TokenShops;

public class TokenManager implements CommandExecutor, Listener {

	private final PvPvE plugin;
	private final DBManager dbManager;
	private final TokenShops tokenShops;
	
	private Map<String, Integer> tokens = new HashMap<String, Integer>();
	
	public TokenManager(PvPvE plugin) {
		this.plugin = plugin;
		this.dbManager = plugin.getDBManager();
		this.tokenShops = new TokenShops(this, plugin);
		
		plugin.getCommand("tokens").setExecutor(this);
		
		prepareTables();
		prepareCache();
	}
	
	public void prepareTables()
	{
		if (!dbManager.hasConnection())
			return;
		
		Connection conn = null;
		try {
			conn = dbManager.getConnection();
			DatabaseMetaData meta = conn.getMetaData();
			
			if (!meta.getTables(null, null, "pvpve_tokens", null).next())
			{
				Statement stat = conn.createStatement();
				plugin.getLogger().info("Creating table 'pvpve_tokens'");
				stat.execute("CREATE TABLE `pvpve_tokens` (name VARCHAR(32) NOT NULL UNIQUE, tokens INT NOT NULL)");
				stat.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	private void prepareCache() {
		tokens.clear();
		
		if (!dbManager.hasConnection())
			return;
		
		Connection conn = null;
		try {
			conn = dbManager.getConnection();
			Statement stat = conn.createStatement();
			ResultSet result = stat.executeQuery("SELECT `name`, `tokens` FROM `pvpve_tokens`");
			while (result.next())
			{			
				tokens.put(result.getString(1), result.getInt(2));
			}
			result.close();
			stat.close();			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void addTokens(final String player, int amount) {
		final int newAmount;
		if (tokens.containsKey(player)) {
			newAmount = tokens.get(player) + amount;
			tokens.put(player, newAmount);
		} else {
			newAmount = amount;
			tokens.put(player, newAmount);
		}
		
		dbManager.queueTransaction(new DBTransaction() {
			
			@Override
			public void executeQuery(Connection conn) {
				PreparedStatement stat;
				try {
					stat = conn.prepareStatement("INSERT INTO `pvpve_tokens` (`name`, `tokens`) VALUES (?,?) ON DUPLICATE KEY UPDATE `tokens` = ?");
					stat.setString(1, player);
					stat.setInt(2, newAmount);
					stat.setInt(3, newAmount);
					stat.execute();
					stat.close();
				} catch (SQLException e) {
					plugin.getLogger().severe("Failed to update token records");
					e.printStackTrace();
				}						
			}
		});
	}
	
	public boolean hasTokens(String player, int amount) {
		if (!tokens.containsKey(player)) {
			return false;
		} else {
			return tokens.get(player) >= amount;
		}
	}
	
	public void clearTokens(final String player) {
		tokens.remove(player);
		
		dbManager.queueTransaction(new DBTransaction() {
			
			@Override
			public void executeQuery(Connection conn) {
				PreparedStatement stat;
				try {
					stat = conn.prepareStatement("DELETE FROM `pvpve_tokens` WHERE `name` = ?");
					stat.setString(1, player);
					stat.execute();
					stat.close();
				} catch (SQLException e) {
					plugin.getLogger().severe("Failed to update token records");
					e.printStackTrace();
				}						
			}
		});
	}
	
	public void removeTokens(final String player, int amount) {
		final int newAmount;
		if (tokens.containsKey(player)) {
			newAmount = tokens.get(player) > amount ? tokens.get(player) - amount : 0;
			tokens.put(player, newAmount);
		} else {
			newAmount = 0;
			tokens.put(player, newAmount);
		}
		
		dbManager.queueTransaction(new DBTransaction() {
			
			@Override
			public void executeQuery(Connection conn) {
				PreparedStatement stat;
				try {
					stat = conn.prepareStatement("UPDATE `pvpve_tokens` SET `tokens` = ? WHERE `name` = ?");
					stat.setInt(1, newAmount);
					stat.setString(2, player);
					stat.execute();
					stat.close();
				} catch (SQLException e) {
					plugin.getLogger().severe("Failed to update token records");
					e.printStackTrace();
				}						
			}
		});
	}
	
	public int getTokens(String player) {
		if (tokens.containsKey(player)) {
			return tokens.get(player);
		} else {
			return 0;
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command is only available for players");
		}
		if (args.length < 1) {
			Player ply = (Player)sender;
			ply.sendMessage(ChatColor.BLUE + "You currently have " + ChatColor.AQUA + getTokens(ply.getName()) + ChatColor.BLUE + " tokens");
		} else {
			String command = args[0];
			if (command.equalsIgnoreCase("reload")) {
				if (sender.hasPermission("pvpve.admin")) {
					dbManager.forceQueue();
					prepareCache();
				}
			} else if (command.equalsIgnoreCase("reloadshops")) {
				if (sender.hasPermission("pvpve.admin")) {
					tokenShops.loadConfiguration();
				}
			} else if (command.equalsIgnoreCase("give")) {
				if (sender.hasPermission("pvpve.token.admin")) {
					if (args.length < 3) {
						sender.sendMessage(ChatColor.RED + "Syntax: /tokens give <name> <amount>");
					} else {
						String name = args[1];
						int amount = -1;
						try {
							amount = Integer.parseInt(args[2]);
						} catch (Exception e) {	}
						
						if (amount < 0) {
							sender.sendMessage(ChatColor.RED + "Invalid amount specified");
						} else {
							addTokens(name, amount);
							sender.sendMessage(ChatColor.BLUE + "Giving " + ChatColor.AQUA + amount + ChatColor.BLUE + " tokens to " + ChatColor.AQUA + name + ChatColor.BLUE + ".");
						}
					}
				}
			} else if (command.equalsIgnoreCase("take")) {
				if (sender.hasPermission("pvpve.token.admin")) {
					if (args.length < 3) {
						sender.sendMessage(ChatColor.RED + "Syntax: /tokens give <name> <amount>");
					} else {
						String name = args[1];
						int amount = -1;
						try {
							amount = Integer.parseInt(args[2]);
						} catch (Exception e) {	}
						
						if (amount < 0) {
							sender.sendMessage(ChatColor.RED + "Invalid amount specified");
						} else {
							removeTokens(name, amount);
							sender.sendMessage(ChatColor.BLUE + "Taking " + ChatColor.AQUA + amount + ChatColor.BLUE + " tokens from " + ChatColor.AQUA + name + ChatColor.BLUE + ".");
						}
					}
				}
			}  else if (command.equalsIgnoreCase("clear")) {
				if (sender.hasPermission("pvpve.token.admin")) {
					if (args.length < 2) {
						sender.sendMessage(ChatColor.RED + "Syntax: /tokens give <name>");
					} else {
						String name = args[1];

						clearTokens(name);
						sender.sendMessage(ChatColor.BLUE + "Cleared tokens from " + ChatColor.AQUA + name + ChatColor.BLUE + ".");
					}
				}
			}
		}
		return true;
	}	
	
}
