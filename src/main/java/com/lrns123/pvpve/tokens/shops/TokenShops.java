package com.lrns123.pvpve.tokens.shops;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.tokens.TokenManager;

public class TokenShops implements Listener {

	private Map<String, TokenItem> items = new HashMap<String, TokenItem>();
	private final PvPvE plugin;
	private final TokenManager manager;
	
	public TokenShops(TokenManager manager, PvPvE plugin) {
		this.plugin = plugin;
		this.manager = manager;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		loadConfiguration();
	}
	
	public void loadConfiguration() {
		YamlConfiguration cfg = plugin.loadConfig("token.yml", true);
		items.clear();
		
		if (cfg == null)
			return;
		
		if (cfg.contains("items")) {
			Set<String> itemIds = cfg.getConfigurationSection("items").getKeys(false);
			for (String itemId : itemIds) {
				ConfigurationSection itemCfg = cfg.getConfigurationSection("items." + itemId);
				TokenItem item = new TokenItem(itemId, itemCfg);
				items.put(itemId.toLowerCase(), item);
			}
		}
		
	}
	
	@EventHandler
	public void onSignChange(SignChangeEvent ev) {
		if (ev.getLine(0).equalsIgnoreCase("[tokenshop]")) {
			if (!ev.getPlayer().hasPermission("pvpve.token.createshop")) {
				ev.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to create token shops");
				ev.getBlock().breakNaturally();
				ev.setCancelled(true);
			} else {
				String itemId = ev.getLine(1);
				if (!items.containsKey(itemId.toLowerCase())) {
					ev.getPlayer().sendMessage(ChatColor.RED + "Unknown item '" + ev.getLine(1) + "'");
					ev.getBlock().breakNaturally();
					ev.setCancelled(true);
				} else {
					ev.getPlayer().sendMessage(ChatColor.GREEN + "Token shop created successfully");
				}
			}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent ev) {
		if (ev.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = ev.getClickedBlock();
			if (block != null) {
				if (block.getState() instanceof Sign) {
					Sign sign = (Sign)block.getState();
					if (sign.getLine(0).equalsIgnoreCase("[tokenshop]")) {
						
						try {
							int price = Integer.parseInt(sign.getLine(2));
							String itemId = sign.getLine(1);
							if (manager.hasTokens(ev.getPlayer().getName(), price)) {
								if (!items.containsKey(itemId.toLowerCase())) {
									ev.getPlayer().sendMessage(ChatColor.RED + "Unknown item '" + itemId + "'");
								} else {
									TokenItem item = items.get(itemId.toLowerCase());
									HashMap<Integer, ItemStack> cantFit = ev.getPlayer().getInventory().addItem(item.createItem());
									if (cantFit != null && cantFit.size() != 0) {
										ev.getPlayer().sendMessage(ChatColor.RED + "You do not have enough room in your inventory");
									} else {
										manager.removeTokens(ev.getPlayer().getName(), price);
										ev.getPlayer().sendMessage(ChatColor.GREEN + "You've bought " + item.getDescription() + " for " + price + " tokens");
									}
								}
							} else {
								ev.getPlayer().sendMessage(ChatColor.RED + "You have insufficient tokens to buy this item");
							}
						} catch (NumberFormatException e) {
							ev.getPlayer().sendMessage(ChatColor.RED + "Invalid price on sign");
							return;
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent ev) {
		if (ev.getBlock().getState() instanceof Sign) {
			Sign sign = (Sign)ev.getBlock().getState();
			if (sign.getLine(0).equalsIgnoreCase("[tokenshop]") && !ev.getPlayer().hasPermission("pvpve.token.createshop")) {
				ev.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to destroy token shops");
				ev.setCancelled(true);
			}
		}
	}
	
}
