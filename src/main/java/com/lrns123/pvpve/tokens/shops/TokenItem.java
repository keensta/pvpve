package com.lrns123.pvpve.tokens.shops;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.lrns123.pvpve.util.Util;

public class TokenItem {
	private int itemId;
	private short subId;
	private int amount;
	private Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
	private String description;

	@SuppressWarnings("deprecation")
    public TokenItem(String id, ConfigurationSection cfg) {
		itemId = cfg.getInt("itemId", 0);
		subId = (short)cfg.getInt("subId", 0);
		amount = cfg.getInt("amount", 0);
		
		description = cfg.getString("description", id);
		
		if (cfg.contains("enchantments")) {
			List<Map<?,?>> entries = cfg.getMapList("enchantments");
			for (Map<?,?> enchantData : entries) {
				final ConfigurationSection enchCfg = Util.mapToConfiguration(enchantData);
				final int eid = enchCfg.getInt("id", -1);
				final int elvl = enchCfg.getInt("lvl", 0);
				if (eid < 0 || elvl <= 0)
					continue;
				
				enchantments.put(Enchantment.getById(eid), elvl);
			}
		}
	}
	
	public String getDescription() {
		return description;
	}
	
	public ItemStack createItem() {
		@SuppressWarnings("deprecation")
        ItemStack item = new ItemStack(itemId, amount, subId);
		item.addUnsafeEnchantments(enchantments);
		return item;
	}
}
