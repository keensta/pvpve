package com.lrns123.pvpve.party;

import org.bukkit.OfflinePlayer;

public class PartyInvitation {
	private final Party party;
	private final OfflinePlayer inviter;
	private final OfflinePlayer invitee;
	
	public PartyInvitation(Party party, OfflinePlayer inviter, OfflinePlayer invitee)
	{
		this.party = party;
		this.inviter = inviter;
		this.invitee = invitee;
	}
	
	/**
	 * @return the party
	 */
	public Party getParty() {
		return party;
	}
	/**
	 * @return the inviter
	 */
	public OfflinePlayer getInviter() {
		return inviter;
	}
	/**
	 * @return the invitee
	 */
	public OfflinePlayer getInvitee() {
		return invitee;
	}
	
}
