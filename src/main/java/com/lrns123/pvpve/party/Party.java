package com.lrns123.pvpve.party;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.party.vote.PartyVote;

public class Party {

	private PartyVote pendingVote = null;

	private PartyParticipant leader;
	private Game game = null;
	private Map<OfflinePlayer, PartyParticipant> participants = new HashMap<OfflinePlayer, PartyParticipant>();
	private int stake = 0;

	public Party(Player leader) {
		PartyParticipant leaderParticipant = new PartyParticipant(leader);
		participants.put(leader, leaderParticipant);
		this.leader = leaderParticipant;
		leaderParticipant.setLeader(true);
	}

	public boolean inGame() {
		return (game != null);
	}

	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}

	public boolean startGame(Game game) {
		String error = game.startGame();
		if (error != null) {
			for (PartyParticipant participant : participants.values()) {
				if (participant.isOnline())
					participant.getPlayer().sendMessage(ChatColor.RED + "Could not start the game: " + error);
			}
			return false;
		}
		return true;
	}

	public boolean abortGame(Game game) {
		game.abortGame();
		return true;
	}

	public PartyParticipant getParticipant(OfflinePlayer player) {
		return participants.get(player);
	}

	public PartyParticipant[] getParticipants() {
		return participants.values().toArray(new PartyParticipant[0]);
	}

	public PartyParticipant[] getOnlineParticipants() {
		List<PartyParticipant> list = new ArrayList<PartyParticipant>();
		for (PartyParticipant pcp : participants.values()) {
			if (pcp.isOnline())
				list.add(pcp);
		}
		return list.toArray(new PartyParticipant[0]);
	}

	public int getParticipantCount() {
		return participants.size();
	}

	public int getOnlineParticipantCount() {
		int online = 0;
		for (OfflinePlayer ply : participants.keySet()) {
			if (ply.isOnline())
				online++;
		}
		return online;
	}

	public void addParticipant(OfflinePlayer player, boolean notifyOthers) {
		if (participants.containsKey(player))
			return;

		participants.put(player, new PartyParticipant(player));

		if (notifyOthers) {
			for (PartyParticipant participant : participants.values()) {
				if (participant.getPlayer() != player && participant.isOnline())
					participant.getPlayer().sendMessage(ChatColor.AQUA + player.getName() + ChatColor.BLUE + " joined the party");
			}
		}
	}

	public boolean isParticipant(OfflinePlayer player) {
		return participants.containsKey(player);
	}

	public boolean isParticipant(PartyParticipant participant) {
		return participants.containsValue(participant);
	}

	public void removeParticipant(OfflinePlayer player, boolean notifyOthers) {
		if (!participants.containsKey(player))
			return;
		participants.remove(player);

		if (notifyOthers) {
			for (PartyParticipant participant : participants.values()) {
				if (participant.getPlayer() != player && participant.isOnline())
					participant.getPlayer().sendMessage(ChatColor.AQUA + player.getName() + ChatColor.BLUE + " left the party");
			}
		}
	}

	public void disband(boolean notifyOthers) {
		for (PartyParticipant participant : participants.values()) {
			if (participant.isOnline())
				participant.getPlayer().sendMessage(ChatColor.BLUE + "The party has been disbanded");
		}

		participants.clear();
		leader = null;
	}

	public PartyParticipant getLeader() {
		return leader;
	}

	public void setLeader(OfflinePlayer leader, boolean notifyOthers) {
		if (!participants.containsKey(leader)) {
			addParticipant(leader, false);
		}
		PartyParticipant leaderParticipant = participants.get(leader);
		if (this.leader != null)
			this.leader.setLeader(false);

		this.leader = leaderParticipant;
		leaderParticipant.setLeader(true);

		if (notifyOthers) {
			for (PartyParticipant participant : participants.values()) {
				if (!participant.isLeader() && participant.isOnline()) {
					participant.getPlayer().sendMessage(ChatColor.AQUA + leaderParticipant.getName() + ChatColor.BLUE + " is now the leader of the party");
				} else if (participant.isLeader() && participant.isOnline()) {
					participant.getPlayer().sendMessage(ChatColor.BLUE + "You are now the leader of the party");
				}
			}
		}
	}

	public void veto(OfflinePlayer player, boolean notifyOthers) {
		if (!participants.containsKey(player))
			return;

		if (!hasPartyVote())
			return;

		if (notifyOthers) {
			for (PartyParticipant participant : participants.values()) {
				if (participant.getPlayer() != player && participant.isOnline()) {
					participant.getPlayer().sendMessage(ChatColor.AQUA + player.getName() + ChatColor.BLUE + " has vetoed the current vote.");
				} else if (participant.isOnline()) {
					participant.getPlayer().sendMessage(ChatColor.BLUE + "You have vetoed the current vote.");
				}
			}
		}

		getPartyVote().abort();
	}

	public boolean hasPartyVote() {
		return pendingVote != null;
	}

	public PartyVote getPartyVote() {
		return pendingVote;
	}

	public void setPartyVote(PartyVote vote) {
		if (pendingVote != null)
			clearPartyVote();

		pendingVote = vote;
	}

	public void clearPartyVote() {
		pendingVote = null;
	}

	public int getStake() {
		return stake;
	}

	public void setStake(int stake, boolean notifyOthers) {
		setStake(stake, notifyOthers, null);
	}
	
	public void setStake(int stake, boolean notifyOthers, Economy eco) {
		this.stake = stake;
		
		if (notifyOthers) {
			for (PartyParticipant participant : participants.values()) {
				if (participant.isOnline()) {
					if (eco == null) {
						participant.getPlayer().sendMessage(ChatColor.BLUE + "The stake is changed to " + ChatColor.AQUA + this.stake + ChatColor.BLUE + (this.stake == 1 ? " coin" : " coins"));
					} else {
						participant.getPlayer().sendMessage(ChatColor.BLUE + "The stake is changed to " + ChatColor.AQUA + this.stake + ChatColor.BLUE + " " + (this.stake == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()));
					}
				}
			}
		}
	}
}
