package com.lrns123.pvpve.party.vote;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.GameManager;
import com.lrns123.pvpve.party.Party;
import com.lrns123.pvpve.party.PartyParticipant;

public class FinishGameVote implements PartyVote {

	private final Party party;
	private final Game game;
	private final GameManager manager;
	private final PvPvE plugin;
	private boolean isCancelled = false;
	
	public FinishGameVote(Party party, Game game, GameManager manager, PvPvE plugin) {
		this.party = party;
		this.game = game;
		this.manager = manager;
		this.plugin = plugin;
	}
	
	@Override
	public void run() {
		if (isCancelled)
			return;
		
		manager.finishGame(game, true);
		party.clearPartyVote();
	}

	@Override
	public void abort() {
		isCancelled = true;
		for (PartyParticipant participant : party.getParticipants()) {
			if (participant.isOnline()) {
				participant.getPlayer().sendMessage(ChatColor.RED + "Game finishing cancelled.");
			}
		}
		party.clearPartyVote();
	}

	@Override
	public void queue() {
		for (PartyParticipant participant : party.getParticipants()) {
			if (participant.isOnline()) {
				participant.getPlayer().sendMessage(ChatColor.BLUE + "Finish Game button pressed!");
				participant.getPlayer().sendMessage(ChatColor.BLUE + "The game will finish in 10 seconds.");
				participant.getPlayer().sendMessage(ChatColor.BLUE + "Use " + ChatColor.AQUA + "/party veto" + ChatColor.BLUE + " to cancel.");
			}
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, this, 200);
	}

}
