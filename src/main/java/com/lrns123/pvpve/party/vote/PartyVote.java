package com.lrns123.pvpve.party.vote;

public interface PartyVote extends Runnable {
	void abort();
	void queue();
}
