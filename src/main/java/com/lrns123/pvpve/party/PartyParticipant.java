package com.lrns123.pvpve.party;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class PartyParticipant {
	private final OfflinePlayer player;
	private int team;
	private Party party;
	private boolean leader;
	
	public PartyParticipant(Player player) {
		this.player = player;
	}
	
	public PartyParticipant(OfflinePlayer player) {
		this.player = player;
	}
	
	public boolean isOnline() {
		return player.isOnline();
	}
	
	public boolean isLeader() {
		return leader;
	}
	
	public void setLeader(boolean leader) {
		this.leader = leader;
	}
	
	public String getName() {
		return player.getName();
	}
	
	public OfflinePlayer getOfflinePlayer() {
		return player;
	}
	
	public Player getPlayer() {
		return player.getPlayer();
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
}
