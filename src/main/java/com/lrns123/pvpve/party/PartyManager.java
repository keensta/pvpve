package com.lrns123.pvpve.party;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.signbutton.SignButtonHandler;

public class PartyManager implements CommandExecutor {

	// List of all currently existing parties
	private List<Party> parties = new ArrayList<Party>();

	// List of all players in a party, and they party they are in
	private Map<OfflinePlayer, Party> playerParties = new HashMap<OfflinePlayer, Party>();

	// List of pending invitations
	private Map<OfflinePlayer, Map<OfflinePlayer, PartyInvitation>> pendingInvitations = new HashMap<OfflinePlayer, Map<OfflinePlayer, PartyInvitation>>();

	// List of sent invitations
	private Map<OfflinePlayer, Map<OfflinePlayer, PartyInvitation>> sentInvitations = new HashMap<OfflinePlayer, Map<OfflinePlayer, PartyInvitation>>();

	private final PvPvE plugin;

	public PartyManager(PvPvE plugin) {
		this.plugin = plugin;

		plugin.getCommand("party").setExecutor(this);

		plugin.getSignButtonListener().registerHandler("[PartyTP]", new SignButtonHandler() {

			@Override
			public void buttonPressed(Player activator, String[] lines) {
				Party party = getParty(activator);
				if (party == null) {
					activator.sendMessage(ChatColor.RED + "You must be in a party to use this");
				} else {
					if (party.inGame()) {
						activator.sendMessage(ChatColor.RED + "You cannot use this while playing a game");
					} else if (!party.getParticipant(activator).isLeader()) {
						activator.sendMessage(ChatColor.RED + "You must be the leader of your party to use this");
					} else {
						activator.sendMessage(ChatColor.BLUE + "Summoning your party members...");
						for (PartyParticipant pcp : party.getParticipants()) {
							if (pcp.isOnline() && pcp.getPlayer() != activator) {
								pcp.getPlayer().sendMessage(ChatColor.BLUE + "You were " + ChatColor.AQUA + "summoned" + ChatColor.BLUE + " by your party leader");
								pcp.getPlayer().teleport(activator);
							}
						}
					}
				}

			}
		});
	}

	/**
	 * Handles commands for the party system
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}

		Player ply = (Player) sender;

		if (args.length < 1) {
			// Show current party
			if (hasParty(ply)) {
				Party party = getParty(ply);
				sender.sendMessage(ChatColor.BLUE + "Players in your party:");
				for (PartyParticipant player : party.getParticipants()) {
					sender.sendMessage((player.isOnline() ? ChatColor.AQUA : ChatColor.GRAY) + (player.isLeader() ? "[L] " : "") + ChatColor.ITALIC + player.getName());
				}
			} else {
				sender.sendMessage(ChatColor.BLUE + "You are not in a party.");
			}
		} else {
			String command = args[0];
			if (command.equalsIgnoreCase("help")) {
				ply.sendMessage(ChatColor.BLUE + "Party command list:");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.GRAY + " - " + ChatColor.BLUE + "View your current party");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "create" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Create a new party");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "invite <name>" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Invite " + ChatColor.ITALIC + "name" + ChatColor.RESET + ChatColor.BLUE + " to your party");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "invites" + ChatColor.GRAY + " - " + ChatColor.BLUE + "List pending invitations");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "accept <name>" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Accept " + ChatColor.ITALIC + "name" + ChatColor.RESET + ChatColor.BLUE + "'s party invitation");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "reject <name>" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Reject " + ChatColor.ITALIC + "name" + ChatColor.RESET + ChatColor.BLUE + "'s party invitation");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "say <message>" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Send message to your party members");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "leave" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Leave your party");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "disband" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Disband your party");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "kick <name>" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Kick " + ChatColor.ITALIC + "name" + ChatColor.RESET + ChatColor.BLUE + " from the party");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "setleader <name>" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Set " + ChatColor.ITALIC + "name" + ChatColor.RESET + ChatColor.BLUE + " as the party leader");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "veto" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Veto the pending vote");
				ply.sendMessage(ChatColor.BLUE + "/party " + ChatColor.AQUA + "stake [amount]" + ChatColor.GRAY + " - " + ChatColor.BLUE + "Shows or changes the current stake");
			} else if (command.equalsIgnoreCase("say")) {
				Party party = getParty(ply);
				if (party == null) {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				} else {
					StringBuilder sb = new StringBuilder();
					for (int i = 1; i < args.length; i++) {
						sb.append(args[i]);
						sb.append(" ");
					}

					if (plugin.getChat() != null) {
						Chat chat = plugin.getChat();
						for (PartyParticipant pcp : party.getParticipants()) {
							if (pcp.isOnline()) {
								pcp.getPlayer().sendMessage(ChatColor.DARK_PURPLE + "<" + ChatColor.WHITE + chat.getPlayerPrefix(pcp.getPlayer()).replaceAll("(?i)&([0-9A-FKLMNOR])", "\u00A7$1") + ply.getName() + chat.getPlayerSuffix(pcp.getPlayer()).replaceAll("(?i)&([0-9A-FKLMNOR])", "\u00A7$1") + ChatColor.DARK_PURPLE + "> " + ChatColor.LIGHT_PURPLE + sb.toString());
							}
						}
					} else {
						for (PartyParticipant pcp : party.getParticipants()) {
							if (pcp.isOnline()) {
								pcp.getPlayer().sendMessage(ChatColor.DARK_PURPLE + "<" + ChatColor.WHITE + ply.getName() + ChatColor.DARK_PURPLE + "> " + ChatColor.LIGHT_PURPLE + sb.toString());
							}
						}
					}
				}
			} else if (command.equalsIgnoreCase("create")) {
				if (hasParty(ply)) {
					ply.sendMessage(ChatColor.RED + "You are already in a party!");
				} else {
					createParty(ply);
					ply.sendMessage(ChatColor.BLUE + "You have created a party!");
					ply.sendMessage(ChatColor.BLUE + "You can invite others using " + ChatColor.AQUA + "/party invite");
				}
			} else if (command.equalsIgnoreCase("invite")) {
				if (!hasParty(ply)) {
					ply.sendMessage(ChatColor.RED + "You must be in a party in order to invite someone.");
				} else {
					if (args.length < 2) {
						ply.sendMessage(ChatColor.RED + "Please specify who you'd like to invite to your party.");
					} else {
						Player target = Bukkit.getPlayerExact(args[1]);
						if (target == null || !target.isOnline()) {
							ply.sendMessage(ChatColor.RED + "Unknown player specified.");
						} else {
							if (getParty(ply).isParticipant(target)) {
								ply.sendMessage(ChatColor.AQUA + target.getName() + ChatColor.BLUE + " is already member of your party.");
							} else {
								addInvitation(ply, target, true);
							}
						}
					}
				}
			} else if (command.equalsIgnoreCase("invites")) {
				PartyInvitation[] invitations = getInvitations(ply);
				if (invitations == null) {
					ply.sendMessage(ChatColor.BLUE + "You have no pending invitations");
				} else {
					ply.sendMessage(ChatColor.BLUE + "You have the following pending invitations:");
					for (PartyInvitation invite : invitations) {
						ply.sendMessage(ChatColor.BLUE + invite.getInviter().getName());
					}
				}
			} else if (command.equalsIgnoreCase("accept")) {
				if (hasParty(ply)) {
					ply.sendMessage(ChatColor.RED + "You cannot accept an invitation if you are already in a party.");
					ply.sendMessage(ChatColor.RED + "Leave your party first, then try again.");
				} else {
					if (args.length < 2) {
						ply.sendMessage(ChatColor.RED + "Please specify whose invitation you'd like to accept.");
					} else {
						OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
						if (target == null) {
							ply.sendMessage(ChatColor.RED + "Unknown player specified.");
						} else {
							if (hasInvitation(target, ply)) {
								PartyInvitation invite = getInvitation(target, ply);
								acceptInvitation(invite, true);
							} else {
								ply.sendMessage(ChatColor.RED + "You have no pending invitations from " + ChatColor.AQUA + target.getName() + ".");
							}
						}
					}
				}
			} else if (command.equalsIgnoreCase("reject")) {
				if (args.length < 2) {
					ply.sendMessage(ChatColor.RED + "Please specify whose invitation you'd like to reject.");
				} else {
					OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
					if (target == null) {
						ply.sendMessage(ChatColor.RED + "Unknown player specified.");
					} else {
						if (hasInvitation(target, ply)) {
							PartyInvitation invite = getInvitation(target, ply);
							rejectInvitation(invite, true);
						} else {
							ply.sendMessage(ChatColor.RED + "You have no pending invitations from " + ChatColor.AQUA + target.getName() + ".");
						}
					}
				}
			} else if (command.equalsIgnoreCase("leave")) {
				Party party = getParty(ply);
				if (party != null) {
					if (party.inGame()) {
						ply.sendMessage(ChatColor.RED + "You cannot leave the party while you're playing a game.");
					} else if (party.getParticipant(ply).isLeader()) {
						ply.sendMessage(ChatColor.RED + "You are the leader of this party.");
						ply.sendMessage(ChatColor.RED + "To leave, either choose another leader and leave, or disband the party.");
					} else {
						removeFromParty(ply);
						ply.sendMessage(ChatColor.BLUE + "You have left the party.");
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				}
			} else if (command.equalsIgnoreCase("disband")) {
				Party party = getParty(ply);
				if (party != null) {
					if (party.inGame()) {
						ply.sendMessage(ChatColor.RED + "You cannot disband the party while you're playing a game.");
					} else if (!party.getParticipant(ply).isLeader()) {
						ply.sendMessage(ChatColor.RED + "You must be the leader of the party to disband it.");
					} else {
						disbandParty(party);
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				}
			} else if (command.equalsIgnoreCase("kick")) {
				Party party = getParty(ply);
				if (party != null) {
					if (party.inGame()) {
						ply.sendMessage(ChatColor.RED + "You cannot kick players from the party while you're playing a game.");
					} else if (!party.getParticipant(ply).isLeader()) {
						ply.sendMessage(ChatColor.RED + "You must be the leader of the party to kick members.");
					} else if (args.length < 2) {

					} else {
						Player target = Bukkit.getPlayerExact(args[1]);
						if (target != null && party.isParticipant(target) && target != ply) {
							removeFromParty(target);
							target.sendMessage(ChatColor.BLUE + "You have been kicked from the party.");
						} else {
							ply.sendMessage(ChatColor.RED + "Player is not in the party or cannot be kicked.");
						}
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				}
			} else if (command.equalsIgnoreCase("setleader")) {
				Party party = getParty(ply);
				if (party != null) {
					if (!party.getParticipant(ply).isLeader()) {
						ply.sendMessage(ChatColor.RED + "You must be the leader of the party to appoint someone else.");
					} else if (args.length < 2) {

					} else {
						Player target = Bukkit.getPlayerExact(args[1]);
						if (target != null && party.isParticipant(target) && target != ply) {
							party.setLeader(target, true);
						} else {
							ply.sendMessage(ChatColor.RED + "Player is not in the party or cannot be set as leader.");
						}
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				}
			} else if (command.equalsIgnoreCase("veto")) {
				Party party = getParty(ply);
				if (party != null) {
					if (party.hasPartyVote()) {
						party.veto(ply, true);
					} else {
						ply.sendMessage(ChatColor.RED + "There is no vote ongoing");
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				}
			} else if (command.equalsIgnoreCase("stake")) {
				Party party = getParty(ply);
				if (party != null) {
					if (args.length < 2) {
						Economy eco = plugin.getEconomy();
						if (eco == null) {
							ply.sendMessage(ChatColor.BLUE + "The stake is currently " + ChatColor.AQUA + party.getStake() + ChatColor.BLUE + (party.getStake() == 1 ? " coin" : " coins"));
						} else {
							ply.sendMessage(ChatColor.BLUE + "The stake is currently " + ChatColor.AQUA + party.getStake() + ChatColor.BLUE + " " + (party.getStake() == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()));
						}	
					} else {
						if (!party.getParticipant(ply).isLeader()) {
							ply.sendMessage(ChatColor.RED + "Only the leader can set the stake");
						} else {
							try {
								int newStake = Integer.parseInt(args[1]);
								party.setStake(newStake, true, plugin.getEconomy());
							} catch (NumberFormatException e) {
								ply.sendMessage(ChatColor.RED + "Invalid stake specified");
							}
						}
					}
				} else {
					ply.sendMessage(ChatColor.RED + "You are not in a party.");
				}
			}
		}
		return true;
	}

	/**
	 * Returns whether or not this player has pending invitations
	 * 
	 * @param player
	 * @return
	 */
	public boolean hasInvitations(OfflinePlayer player) {
		return pendingInvitations.containsKey(player);
	}

	/**
	 * Returns whether the player has a pending invitation from the specified
	 * player
	 * 
	 * @param player
	 * @return
	 */
	public boolean hasInvitation(OfflinePlayer inviter, OfflinePlayer invitee) {
		if (pendingInvitations.containsKey(invitee))
			return pendingInvitations.get(invitee).containsKey(inviter);
		return false;
	}

	/**
	 * Returns the pending invitations from the specified player
	 * 
	 * @param player
	 * @return
	 */
	public PartyInvitation[] getInvitations(OfflinePlayer invitee) {
		if (pendingInvitations.containsKey(invitee))
			return pendingInvitations.get(invitee).values().toArray(new PartyInvitation[0]);
		return null;
	}

	/**
	 * Returns the pending invitation from the specified inviter and invitee
	 * 
	 * @param player
	 * @return
	 */
	public PartyInvitation getInvitation(OfflinePlayer inviter, OfflinePlayer invitee) {
		if (pendingInvitations.containsKey(invitee))
			return pendingInvitations.get(invitee).get(inviter);
		return null;
	}

	/**
	 * Rejects the specified invitation
	 * 
	 * @param invitation
	 * @param notify
	 */
	public void rejectInvitation(PartyInvitation invitation, boolean notify) {
		OfflinePlayer invitee = invitation.getInvitee();
		OfflinePlayer inviter = invitation.getInviter();

		if (inviter.isOnline()) {
			inviter.getPlayer().sendMessage(ChatColor.AQUA + invitee.getName() + ChatColor.BLUE + " has rejected the invitation to join your party");
		}

		if (invitee.isOnline()) {
			invitee.getPlayer().sendMessage(ChatColor.BLUE + "You have rejected " + ChatColor.AQUA + inviter.getName() + ChatColor.BLUE + "'s party invitation");
		}

		// Remove from pending invitations
		if (pendingInvitations.containsKey(invitee)) {
			pendingInvitations.get(invitee).remove(inviter);
			if (pendingInvitations.get(invitee).size() == 0)
				pendingInvitations.remove(invitee);
		}

		// Remove from sent invitations
		if (sentInvitations.containsKey(inviter)) {
			sentInvitations.get(inviter).remove(invitee);
			if (sentInvitations.get(inviter).size() == 0)
				sentInvitations.remove(inviter);
		}
	}

	/**
	 * Accepts the specified invitation
	 * 
	 * @param invitation
	 * @param notify
	 */
	public void acceptInvitation(PartyInvitation invitation, boolean notify) {
		OfflinePlayer invitee = invitation.getInvitee();
		OfflinePlayer inviter = invitation.getInviter();

		if (playerParties.containsKey(invitee))
			return;

		if (!invitee.isOnline())
			return;

		if (inviter.isOnline()) {
			inviter.getPlayer().sendMessage(ChatColor.AQUA + invitee.getName() + ChatColor.BLUE + " has accepted the invitation to join your party");
		}

		invitee.getPlayer().sendMessage(ChatColor.BLUE + "You have accepted " + ChatColor.AQUA + inviter.getName() + ChatColor.BLUE + "'s party invitation");

		Party party = invitation.getParty();
		party.addParticipant(invitee.getPlayer(), true);

		playerParties.put(invitee, party);

		// Remove from pending invitations
		if (pendingInvitations.containsKey(invitee)) {
			pendingInvitations.get(invitee).remove(inviter);
			if (pendingInvitations.get(invitee).size() == 0)
				pendingInvitations.remove(invitee);
		}

		// Remove from sent invitations
		if (sentInvitations.containsKey(inviter)) {
			sentInvitations.get(inviter).remove(invitee);
			if (sentInvitations.get(inviter).size() == 0)
				sentInvitations.remove(inviter);
		}
	}

	/**
	 * Sends an invitation to the specified player
	 * 
	 * @param inviter
	 * @param invitee
	 * @param notify
	 */
	public void addInvitation(OfflinePlayer inviter, OfflinePlayer invitee, boolean notify) {
		Party party = getParty(inviter);
		if (party == null)
			return;

		PartyInvitation invite = new PartyInvitation(party, inviter, invitee);
		if (notify) {
			if (invitee.isOnline()) {
				invitee.getPlayer().sendMessage(ChatColor.BLUE + "You have received an invitation to join " + ChatColor.AQUA + inviter.getName() + ChatColor.BLUE + "'s party.");
				invitee.getPlayer().sendMessage(ChatColor.BLUE + "Use /party accept " + ChatColor.AQUA + inviter.getName() + ChatColor.BLUE + " to accept");
			}
			if (inviter.isOnline()) {
				inviter.getPlayer().sendMessage(ChatColor.BLUE + "You have sent an invitation to " + ChatColor.AQUA + invitee.getName());
			}
		}

		// Add to pending invitations
		if (pendingInvitations.containsKey(invitee)) {
			pendingInvitations.get(invitee).put(inviter, invite);
		} else {
			Map<OfflinePlayer, PartyInvitation> invites = new HashMap<OfflinePlayer, PartyInvitation>();
			invites.put(inviter, invite);
			pendingInvitations.put(invitee, invites);
		}

		// Add to sent invitations
		if (sentInvitations.containsKey(inviter)) {
			sentInvitations.get(inviter).put(invitee, invite);
		} else {
			Map<OfflinePlayer, PartyInvitation> invites = new HashMap<OfflinePlayer, PartyInvitation>();
			invites.put(invitee, invite);
			sentInvitations.put(invitee, invites);
		}
	}

	/**
	 * Returns the party the player is in, or null if the player has no party
	 * 
	 * @param player
	 * @return
	 */
	public Party getParty(OfflinePlayer player) {
		if (playerParties.containsKey(player)) {
			return playerParties.get(player);
		}
		return null;
	}

	/**
	 * Returns whether the player has a party or not
	 * 
	 * @param player
	 * @return
	 */
	public boolean hasParty(OfflinePlayer player) {
		if (playerParties.containsKey(player))
			return true;

		return false;
	}

	/**
	 * Cancels all invitations sent out by this player
	 * 
	 * @param player
	 */
	public void cancelAllInvitations(OfflinePlayer player) {
		if (sentInvitations.containsKey(player)) {
			for (PartyInvitation invite : sentInvitations.get(player).values()) {
				try {
					pendingInvitations.get(invite.getInvitee()).remove(invite.getInviter());
				} catch (Exception e) {
				}
			}
			sentInvitations.remove(player);
		}
	}

	/**
	 * Removes the specified player from their party If the player is the leader
	 * of their party, the party will be disbanded.
	 * 
	 * @param player
	 */
	public boolean removeFromParty(OfflinePlayer player) {
		if (playerParties.containsKey(player)) {
			Party party = playerParties.get(player);
			if (party.getParticipant(player).isLeader()) {
				return false;
			} else {
				// Remove from party
				party.removeParticipant(player, true);
				playerParties.remove(player);
				return true;
			}
		}
		return false;
	}

	public void disbandParty(Party party) {
		PartyParticipant[] participants = party.getParticipants();
		PartyParticipant leader = party.getLeader();
		party.disband(true);
		for (PartyParticipant ply : participants) {
			playerParties.remove(ply.getOfflinePlayer());
		}
		parties.remove(party);
		cancelAllInvitations(leader.getOfflinePlayer());
	}

	/**
	 * Creates a new party for the specified player
	 * 
	 * @param player
	 */
	public void createParty(Player leader) {
		Party party = new Party(leader);

		parties.add(party);
		playerParties.put(leader, party);
	}

}
