package com.lrns123.pvpve.db;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class DBManager {
	private final static int poolsize = 10;
	private final static long timeToLive = 300000;
	private final Vector<JDCConnection> connections;
	private final ConnectionReaper reaper;
	private final String url, user, pass;
	private final Lock lock = new ReentrantLock();

	private boolean noDB = true;
	private DBTransactionHandler handler = null;
	private BukkitTask handlerTask = null;
	
	private final Plugin plugin;

	public DBManager(Plugin plugin) {
		this.plugin = plugin;
		
		FileConfiguration cfg = plugin.getConfig();
		if (cfg == null) {
			noDB = true;
			user = "";
			pass = "";
			url = "";
			connections = null;
			reaper = null;
			return;
		}
		
		final String host = cfg.getString("database.host");
		final String port = cfg.getString("database.port");
		user = cfg.getString("database.user");
		pass = cfg.getString("database.pass");
		final String database = cfg.getString("database.database");
		url = "jdbc:mysql://" + host + ":" + port + "/" + database;
	
		connections = new Vector<JDCConnection>(poolsize);
		reaper = new ConnectionReaper();
		reaper.start();
		
		if (establishConnection()) {
			noDB = false;
		} else {
			noDB = true;
		}
	}

	public Logger getLogger()
	{
		return plugin.getLogger();
	}
	/**
	 * Returns whether or not a database connection is available
	 * @return
	 */
	public boolean hasConnection()
	{
		return !noDB;
	}

	/**
	 * Gets the connection handle for the database. Call this function every time a database operation needs to be performed, do not store the handles!
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		lock.lock();
		try {
			final Enumeration<JDCConnection> conns = connections.elements();
			while (conns.hasMoreElements()) {
				final JDCConnection conn = conns.nextElement();
				if (conn.lease()) {
					if (conn.isValid())
						return conn;
					connections.remove(conn);
					conn.terminate();
				}
			}
			final JDCConnection conn = new JDCConnection(DriverManager.getConnection(url, user, pass));
			conn.lease();
			if (!conn.isValid()) {
				conn.terminate();
				throw new SQLException("Failed to validate a brand new connection");
			}
			connections.add(conn);
			return conn;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Connects to the database and sets up the transaction handler
	 * @return
	 */
	private boolean establishConnection() {
		try {
			Connection conn = getConnection();
			handler = new DBTransactionHandler(this);
			
			handlerTask = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, handler, 20, 20);
			if (handlerTask == null)
				plugin.getLogger().severe("Could not create async task");
			
			conn.close();
			return true;
		} catch (SQLException e) {
			plugin.getLogger().severe("Could not connect to database: " + e.getMessage());
			return false;
		}
	}
	
	
	/**
	 * Forces all pending transactions to execute and closes all database connections. The database is no longer accessible after this call.
	 */	
	public void shutdown()
	{
		if (handler != null && handlerTask != null) {
			handlerTask.cancel();
			handler.run();	// Run to finish all pending transactions
		}
		
		final Enumeration<JDCConnection> conns = connections.elements();
		while (conns.hasMoreElements()) {
			final JDCConnection conn = conns.nextElement();
			connections.remove(conn);
			conn.terminate();
		}
	}
	
	/**
	 * Forces all pending transactions to execute at once. NOTE: These will execute on the current thread!
	 */
	public void forceQueue()
	{
		if (handler != null && handlerTask != null) {
			handler.run();	// Run to finish all pending transactions
		}
	}
	
	/**
	 * Queues the specified transaction to be executed async
	 * @param transaction
	 */
	public void queueTransaction(DBTransaction transaction)
	{
		if (!noDB)
			handler.queueTransaction(transaction);		
	}

	/**
	 * Removes old connection handles
	 */
	private void reapConnections() {
		lock.lock();
		final long stale = System.currentTimeMillis() - timeToLive;
		final Iterator<JDCConnection> itr = connections.iterator();
		while (itr.hasNext()) {
			final JDCConnection conn = itr.next();
			if (conn.inUse() && stale > conn.getLastUse() && !conn.isValid())
				itr.remove();
		}
		lock.unlock();
	}

	private class ConnectionReaper extends Thread
	{
		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(300000);
				} catch (final InterruptedException e) {}
				reapConnections();
			}
		}
	}

	private class JDCConnection implements Connection
	{
		private final Connection conn;
		private boolean inuse;
		private long timestamp;
		private int networkTimeout;
		private String schema;

		JDCConnection(Connection conn) {
			this.conn = conn;
			inuse = false;
			timestamp = 0;
			networkTimeout = 30;
			schema = "default";
		}

		@Override
		public void clearWarnings() throws SQLException {
			conn.clearWarnings();
		}

		@Override
		public void close() {
			inuse = false;
			try {
				if (!conn.getAutoCommit())
					conn.setAutoCommit(true);
			} catch (final SQLException ex) {
				connections.remove(conn);
				terminate();
			}
		}

		@Override
		public void commit() throws SQLException {
			conn.commit();
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return conn.createArrayOf(typeName, elements);
		}

		@Override
		public Blob createBlob() throws SQLException {
			return conn.createBlob();
		}

		@Override
		public Clob createClob() throws SQLException {
			return conn.createClob();
		}

		@Override
		public NClob createNClob() throws SQLException {
			return conn.createNClob();
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			return conn.createSQLXML();
		}

		@Override
		public Statement createStatement() throws SQLException {
			return conn.createStatement();
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			return conn.createStatement(resultSetType, resultSetConcurrency);
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return conn.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return conn.createStruct(typeName, attributes);
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			return conn.getAutoCommit();
		}

		@Override
		public String getCatalog() throws SQLException {
			return conn.getCatalog();
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			return conn.getClientInfo();
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			return conn.getClientInfo(name);
		}

		@Override
		public int getHoldability() throws SQLException {
			return conn.getHoldability();
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return conn.getMetaData();
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			return conn.getTransactionIsolation();
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return conn.getTypeMap();
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			return conn.getWarnings();
		}

		@Override
		public boolean isClosed() throws SQLException {
			return conn.isClosed();
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			return conn.isReadOnly();
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			return conn.isValid(timeout);
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return conn.isWrapperFor(iface);
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			return conn.nativeSQL(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return conn.prepareCall(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return conn.prepareCall(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return conn.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			return conn.prepareStatement(sql);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			return conn.prepareStatement(sql, autoGeneratedKeys);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return conn.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return conn.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			return conn.prepareStatement(sql, columnIndexes);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			return conn.prepareStatement(sql, columnNames);
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			conn.releaseSavepoint(savepoint);
		}

		@Override
		public void rollback() throws SQLException {
			conn.rollback();
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			conn.rollback(savepoint);
		}

		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			conn.setAutoCommit(autoCommit);
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			conn.setCatalog(catalog);
		}

		@Override
		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			conn.setClientInfo(properties);
		}

		@Override
		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			conn.setClientInfo(name, value);
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {
			conn.setHoldability(holdability);
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			conn.setReadOnly(readOnly);
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			return conn.setSavepoint();
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return conn.setSavepoint(name);
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			conn.setTransactionIsolation(level);
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			conn.setTypeMap(map);
		}

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return conn.unwrap(iface);
		}

		public int getNetworkTimeout() throws SQLException {
			return networkTimeout;
		}

		public void setNetworkTimeout(Executor exec, int timeout) throws SQLException {
			networkTimeout = timeout;
		}

		public void abort(Executor exec) throws SQLException {
			// Not implemented really...
		}

		public String getSchema() throws SQLException {
			return schema;
		}

		public void setSchema(String str) throws SQLException {
			schema = str;
		}

		long getLastUse() {
			return timestamp;
		}

		boolean inUse() {
			return inuse;
		}

		boolean isValid() {
			try {
				return conn.isValid(1);
			} catch (final SQLException ex) {
				return false;
			}
		}

		synchronized boolean lease() {
			if (inuse)
				return false;
			inuse = true;
			timestamp = System.currentTimeMillis();
			return true;
		}

		void terminate() {
			try {
				conn.close();
			} catch (final SQLException ex) {}
		}
	}
}
