package com.lrns123.pvpve.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import com.lrns123.pvpve.db.DBTransaction;

public class DBTransactionHandler implements Runnable {

	private Queue<DBTransaction> queue = new ConcurrentLinkedQueue<DBTransaction>();
	private Lock lock = new ReentrantLock();
	private final DBManager manager;
	
	public DBTransactionHandler(DBManager manager)
	{
		this.manager = manager;
	}

	@Override
	public void run() {
		if (!lock.tryLock())
			return;

		Connection conn = null;
		
		try {
			conn = manager.getConnection();
			
			while (!queue.isEmpty()) {
				final DBTransaction trans = queue.poll();
				trans.executeQuery(conn);
			}
			
		} catch (SQLException e) {
			manager.getLogger().severe("Failed to execute query: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		lock.unlock();
	}

	public void queueTransaction(DBTransaction transaction) {
		queue.add(transaction);
	}

}
