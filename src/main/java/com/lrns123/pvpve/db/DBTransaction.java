package com.lrns123.pvpve.db;

import java.sql.Connection;

public interface DBTransaction {
	void executeQuery(Connection conn);	
}
