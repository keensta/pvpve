package com.lrns123.pvpve;

import java.io.File;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.lrns123.pvpve.arena.ArenaManager;
import com.lrns123.pvpve.db.DBManager;
import com.lrns123.pvpve.game.GameManager;
import com.lrns123.pvpve.party.PartyManager;
import com.lrns123.pvpve.pve.PvEManager;
import com.lrns123.pvpve.pvp.PvPManager;
import com.lrns123.pvpve.signbutton.SignButtonListener;
import com.lrns123.pvpve.tokens.TokenManager;

/**
 * 
 * PvPvE
 * 
 * @author Lourens Elzinga
 * @author Andi Keen - Updated and Modified to current version
 * 
 */
public class PvPvE extends JavaPlugin {

	private static PvPvE instance = null;
	
	public static PvPvE getInstance()
	{
		return instance;
	}
	
	private PvPManager pvpManager = null;
	private PvEManager pveManager = null;
	private ArenaManager arenaManager = null;
	private PartyManager partyManager = null;
	private GameManager gameManager = null;
	private DBManager dbManager = null;
	private TokenManager tokenManager = null;
	private SignButtonListener signButtonListener = null;

	private Economy economy = null;
	private Chat chat = null;
	private Permission permission = null;

	@Override
	public void onDisable() {
		instance = null;
		
		gameManager.shutdown();
		pvpManager.shutdown();
		dbManager.shutdown();
	}

	@Override
	public void onEnable() {
		instance = this;
		
		setupEconomy();
		setupChat();
		setupPermission();

		dbManager = new DBManager(this);
		signButtonListener = new SignButtonListener(this);
		tokenManager = new TokenManager(this);
		
		pvpManager = new PvPManager(this);
		pveManager = new PvEManager(this);
		
		arenaManager = new ArenaManager(this);
		partyManager = new PartyManager(this);
		gameManager = new GameManager(this);
	}
	
	public YamlConfiguration loadConfig(String filename, boolean loadDefault) {
		File f = new File(getDataFolder(), filename);
		if (!f.canRead() && loadDefault)
			saveResource(filename, false);

		if (!f.canRead()) {
			return null;
		}
		
		return YamlConfiguration.loadConfiguration(f);
	}

/*	public NBTTagCompound loadNBTConfig(String filename, boolean loadDefault) {
		File f = new File(getDataFolder(), filename);
		if (!f.canRead() && loadDefault)
			saveResource(filename, false);

		if (!f.canRead()) {
			return null;
		}
		
		try {
			InputStream fs = new FileInputStream(f);
			return NBTCompressedStreamTools.a(fs);
		} catch (FileNotFoundException e) {
			return null;
		}
	}*/
		
	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}

	private boolean setupChat() {
		RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) {
			chat = chatProvider.getProvider();
		}

		return (chat != null);
	}

	private boolean setupPermission() {
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}

	public Economy getEconomy() {
		return economy;
	}

	public Chat getChat() {
		return chat;
	}

	public Permission getPermission() {
		return permission;
	}
	
	public PvPManager getPvPManager() {
		return pvpManager;
	}
	
	public PvEManager getPvEManager() {
		return pveManager;
	}
	
	public DBManager getDBManager() {
		return dbManager;
	}
	
	public ArenaManager getArenaManager() {
		return arenaManager;
	}
	
	public PartyManager getPartyManager() {
		return partyManager;
	}
	
	public GameManager getGameManager() {
		return gameManager;
	}
	
	public TokenManager getTokenManager() {
		return tokenManager;
	}
	
	public SignButtonListener getSignButtonListener() {
		return signButtonListener;
	}
}
