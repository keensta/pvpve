package com.lrns123.pvpve.pve;

import java.util.ArrayList;

import com.lrns123.pvpve.util.WeightedRandom;

public class PvELootTable {
	
	private double content = 0;
	private double factor = 1;
	private ArrayList<PvELootTableItem> items = new ArrayList<PvELootTableItem>();
	
	public String toString()
	{
		return "PvELootTable(content:" + content + ",items:" + items + ")";
	}
	
	public void addItemToTable(PvELootTableItem item)
	{
		items.add(item);
	}
	
	public void clearTable()
	{
		items.clear();
	}
	
	public PvELootTableItem getWeightedRandomItem()
	{
		WeightedRandom rn = new WeightedRandom();
		rn.setOptions(items.toArray(new PvELootTableItem[0]));
		
		PvELootTableItem item = (PvELootTableItem)rn.getWeightedRandom();
		
		return item;
	}

	public void setContent(double content) {
		this.content = content;
	}

	public double getContent() {
		return content;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

	public double getFactor() {
		return factor;
	}	
}
