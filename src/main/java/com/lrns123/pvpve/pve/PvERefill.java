package com.lrns123.pvpve.pve;

public class PvERefill implements Runnable {

	private PvEManager manager = null;
	
	public PvERefill(PvEManager manager)
	{
		this.manager = manager;
	}
	
	@Override
	public void run() {
		manager.refillAll();
		manager.queueRefillTask();
	}

}
