package com.lrns123.pvpve.pve;

import java.awt.Point;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pve.modifiers.BookModifier;
import com.lrns123.pvpve.pve.modifiers.ColorModifier;
import com.lrns123.pvpve.pve.modifiers.DurabilityModifier;
import com.lrns123.pvpve.pve.modifiers.EnchantModifier;
import com.lrns123.pvpve.pve.modifiers.FixedDurabilityModifier;
import com.lrns123.pvpve.pve.modifiers.FixedQuantityModifier;
import com.lrns123.pvpve.pve.modifiers.IPvEModifier;
import com.lrns123.pvpve.pve.modifiers.LoreModifier;
import com.lrns123.pvpve.pve.modifiers.NameModifier;
import com.lrns123.pvpve.pve.modifiers.PotionModifier;
import com.lrns123.pvpve.pve.modifiers.QuantityModifier;
import com.lrns123.pvpve.pve.modifiers.RepairCostModifier;
import com.lrns123.pvpve.pve.modifiers.SkullNameModifier;
import com.lrns123.pvpve.pve.modifiers.TagModifier;
import com.lrns123.pvpve.pve.scoring.PvEScoreTracker;
import com.lrns123.pvpve.pve.scoring.ScoreboardSign;
import com.lrns123.pvpve.signbutton.SignButtonHandler;
import com.lrns123.pvpve.util.PolygonRegion;
import com.lrns123.pvpve.util.Region;
import com.lrns123.pvpve.util.RegionType;
import com.lrns123.pvpve.util.Util;

public class PvEManager implements CommandExecutor, Listener {

    public HashMap<String, List<Region>> protectedAreas = new HashMap<String, List<Region>>();
    public HashMap<String, List<PolygonRegion>> polygonProtectedAreas = new HashMap<String, List<PolygonRegion>>();
    public HashMap<String, List<Region>> expProtectedAreas = new HashMap<String, List<Region>>();
    public HashMap<String, PvEArea> pveAreas = new HashMap<String, PvEArea>();
    private Set<String> blockedCommands = new HashSet<String>();

    private HashMap<Player, Location> customRespawn = new HashMap<Player, Location>();

    private final PvPvE plugin;
    private int refillInterval = 3600;
    private int refillTaskId = -1;

    @SuppressWarnings("unused")
    private PvEScoreTracker scoreTracker;

    public PvEManager(PvPvE plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        plugin.getCommand("pve").setExecutor(this);

        loadConfiguration();
        refillAll();
        queueRefillTask();
        createSignButtons();

        scoreTracker = new PvEScoreTracker(plugin, this);
    }

    private void createSignButtons() {
        plugin.getSignButtonListener().registerHandler("[ForceUpdate]", new SignButtonHandler() {

            @Override
            public void buttonPressed(Player activator, String[] lines) {
                String area = lines[1];
                if(pveAreas.containsKey(area)) {
                    PvEArea pveArea = pveAreas.get(area);

                    pveArea.getScoreboard().run();
                    activator.sendMessage(ChatColor.BLUE + "ForceUpdating " + area + " leaderboards.");
                } else {
                    activator.sendMessage(ChatColor.RED + "Invalid PvE area specified");
                }

            }
        });

        plugin.getSignButtonListener().registerHandler("[PvERefill]", new SignButtonHandler() {

            @Override
            public void buttonPressed(Player activator, String[] lines) {
                String area = lines[1];
                if(pveAreas.containsKey(area)) {
                    refillArea(area);
                    activator.sendMessage(ChatColor.BLUE + "Refilling " + area + "...");
                } else {
                    activator.sendMessage(ChatColor.RED + "Invalid PvE area specified");
                }

            }
        });

        plugin.getSignButtonListener().registerHandler("[PvELootSet]", new SignButtonHandler() {

            @Override
            public void buttonPressed(Player activator, String[] lines) {
                String areaName = lines[1];
                String set = lines[2];
                if(pveAreas.containsKey(areaName)) {
                    PvEArea area = getPvEArea(areaName);
                    area.setLootSet(set);
                    activator.sendMessage(ChatColor.BLUE + "Loot set of " + areaName + " changed to " + set + "...");
                } else {
                    activator.sendMessage(ChatColor.RED + "Invalid PvE area specified");
                }

            }
        });

        plugin.getSignButtonListener().registerHandler("[PvEMobDmg]", new SignButtonHandler() {

            @Override
            public void buttonPressed(Player activator, String[] lines) {
                String area = lines[1];
                try {
                    double multiplier = Double.parseDouble(lines[2]);
                    if(pveAreas.containsKey(area)) {
                        pveAreas.get(area).setMobDamageMultiplier(multiplier);
                        activator.sendMessage(ChatColor.BLUE + "Mob damage multiplier for " + area + " set to "
                                + multiplier + "...");
                    } else {
                        activator.sendMessage(ChatColor.RED + "Invalid PvE area specified");
                    }
                } catch(NumberFormatException e) {
                    activator.sendMessage(ChatColor.RED + "Invalid multiplier specified");
                } catch(Exception e) {
                    activator.sendMessage(ChatColor.RED + "Could not change mob damage multiplier!");
                }
            }
        });

        plugin.getSignButtonListener().registerHandler("[PvEKillMobs]", new SignButtonHandler() {

            @Override
            public void buttonPressed(Player activator, String[] lines) {
                String areaName = lines[1];
                boolean lightning = lines[2].equalsIgnoreCase("lightning");

                if(pveAreas.containsKey(areaName)) {
                    PvEArea area = pveAreas.get(areaName);

                    World world = Bukkit.getWorld(area.getWorld());
                    if(world == null) {
                        activator.sendMessage(ChatColor.RED + "PvE area is in a non-existant world");
                    } else {
                        for(LivingEntity ent : world.getLivingEntities()) {
                            if(ent instanceof HumanEntity)
                                continue;

                            if(area.getBoundingBox().containsLocationFast(ent.getLocation())) {
                                if(lightning) {
                                    world.strikeLightningEffect(ent.getLocation());
                                }
                                ent.remove();
                            }
                        }
                    }

                    activator.sendMessage(ChatColor.BLUE + "Killed all mobs in " + areaName + "...");
                } else {
                    activator.sendMessage(ChatColor.RED + "Invalid PvE area specified");
                }
            }
        });

    }

    /**
     * Refills all PvE areas that are auto-refilling
     */
    public void refillAll() {
        plugin.getLogger().info("Refilling all PvE areas (except manual refill areas)");
        for(final PvEArea area : pveAreas.values()) {
            if(area.isAutomaticRefill())
                refillArea(area);
        }
    }

    public void refillArea(String area) {
        if(pveAreas.containsKey(area))
            refillArea(pveAreas.get(area));
    }

    public void refillArea(PvEArea area) {
        Random rn = new Random();
        PvEChest[] chests = area.getLootChests();
        for(final PvEChest chest : chests) {
            if(!chest.findChest())
                continue;

            chest.clearChest();

            PvELootTable lootTable = area.getLootTable(chest.getLootId(area.getLootSet()));
            if(lootTable == null)
                continue;
            double content = lootTable.getContent();
            double factor = lootTable.getFactor();

            do {
                PvELootTableItem lootEntry = lootTable.getWeightedRandomItem();
                if(lootEntry == null)
                    break;

                ItemStack newItem = lootEntry.getGeneratedItem();

                if(!chest.addItem(newItem))
                    break;

                content /= factor;
            } while((rn.nextDouble() * 50.0) < content);
        }
    }

    public PvEArea getPvEArea(String name) {
        return pveAreas.get(name);
    }

    public void queueRefillTask() {
        if(refillTaskId != -1 && plugin.getServer().getScheduler().isQueued(refillTaskId)
                || plugin.getServer().getScheduler().isCurrentlyRunning(refillTaskId)) {
            plugin.getServer().getScheduler().cancelTask(refillTaskId);

        }

        PvERefill task = new PvERefill(this);
        refillTaskId = plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, task, 20 * refillInterval);
    }

    private IPvEModifier createModifier(String modifier) {
        if(modifier.equalsIgnoreCase("quantity"))
            return new QuantityModifier();
        else if(modifier.equalsIgnoreCase("fixedquantity"))
            return new FixedQuantityModifier();
        else if(modifier.equalsIgnoreCase("enchant"))
            return new EnchantModifier();
        else if(modifier.equalsIgnoreCase("name"))
            return new NameModifier();
        else if(modifier.equalsIgnoreCase("lore"))
            return new LoreModifier();
        else if(modifier.equalsIgnoreCase("potionfx"))
            return new PotionModifier();
        else if(modifier.equalsIgnoreCase("color"))
            return new ColorModifier();
        else if(modifier.equalsIgnoreCase("tag"))
            return new TagModifier();
        else if(modifier.equalsIgnoreCase("skullname"))
            return new SkullNameModifier();
        else if(modifier.equalsIgnoreCase("book"))
            return new BookModifier();
        else if(modifier.equalsIgnoreCase("repaircost"))
            return new RepairCostModifier();
        else if(modifier.equalsIgnoreCase("durability"))
            return new DurabilityModifier();
        else if(modifier.equalsIgnoreCase("fixeddurability"))
            return new FixedDurabilityModifier();
        else
            return null;
    }

    private void loadConfiguration() {
        protectedAreas.clear();
        pveAreas.clear();
        blockedCommands.clear();

        YamlConfiguration config = plugin.loadConfig("pve.yml", true);

        if(config == null) {
            plugin.getLogger().severe("Could not find PvE configuration");
            return;
        }

        refillInterval = config.getInt("refillInterval", 3600);

        if(!config.contains("areas") || !config.isConfigurationSection("areas"))
            return; // Nothing to load

        Set<String> areas = config.getConfigurationSection("areas").getKeys(false);

        for(final String name : areas) {
            plugin.getLogger().info("Loading PvE area " + name);
            ConfigurationSection areaEntry = config.getConfigurationSection("areas." + name);

            PvEArea area = new PvEArea();
            area.setName(name);

            String world = areaEntry.getString("world");
            if(world == null) {
                plugin.getLogger().severe("PvE area '" + name + "' has an invalid world, ignoring...");
                continue;
            }

            area.setWorld(world);
            String regiontype = areaEntry.getString("regiontype");

            if(regiontype.equalsIgnoreCase("square")) {
                List<Double> bbox = areaEntry.getDoubleList("bbox");
                if(bbox == null || bbox.size() != 6) {
                    plugin.getLogger().severe("PvE area '" + name + "' has an invalid bounding box, ignoring...");
                    continue;
                }

                Vector min = new Vector(bbox.get(0), bbox.get(1), bbox.get(2));
                Vector max = new Vector(bbox.get(3), bbox.get(4), bbox.get(5));

                area.setBoundingBox(new Region(world, min, max));

                int expRadius = areaEntry.getInt("explosionRadius", 0);
                Vector emin = new Vector(bbox.get(0) - expRadius, bbox.get(1) - expRadius, bbox.get(2) - expRadius);
                Vector emax = new Vector(bbox.get(3) + expRadius, bbox.get(4) + expRadius, bbox.get(5) + expRadius);

                area.setExpBoundingbox(new Region(world, emin, emax));
                area.setRegionType(RegionType.CUBOID);
            } else {
                List<String> points = areaEntry.getStringList("points");
                if(points == null || points.size() < 2) {
                    plugin.getLogger().severe("PvE area '" + name + "' has an invalid polygon region, ignoring...");
                    continue;
                }

                area.setPolygonRegion(points);

                int celling = areaEntry.getInt("celling");
                int floor = areaEntry.getInt("floor");

                if(celling == 0 || floor == 0) {
                    plugin.getLogger().severe("PvE area '" + name + "' has an invalid polygon region, ignoring...");
                    continue;
                }

                area.getPolygonRegion().setCelling(celling);
                area.getPolygonRegion().setFloor(floor);
                area.setRegionType(RegionType.POLYGON);
            }

            if(areaEntry.getBoolean("protected", true)) {
                area.setProtected(true);
                addProtectedArea(area);
            } else {
                area.setProtected(false);
            }

            area.setLockDispensers(areaEntry.getBoolean("lockDispensers", false));
            area.setBlockCreeperFriendlyFire(areaEntry.getBoolean("blockCreeperFriendlyFire", false));
            area.setNoSleeping(areaEntry.getBoolean("noSleeping", false));
            area.setNoVehicles(areaEntry.getBoolean("noVehicles", false));
            area.setNoPearls(areaEntry.getBoolean("noPearls", false));
            area.setFireSpread(areaEntry.getBoolean("fireSpread", true));
            area.setKeepDeathItems(areaEntry.getBoolean("keepDeathItems", false));
            area.setTrackScore(areaEntry.getBoolean("TrackScore", false));
            area.setMobDamageMultiplier(areaEntry.getDouble("mobDamageMultiplier", 1));
            area.setPriority(areaEntry.getInt("priority", 0));

            if(areaEntry.contains("blockedCmds")) {
                for(String cmd : areaEntry.getStringList("blockedCmds")) {
                    blockedCommands.add("/" + cmd);
                    area.addBlockedCommand(cmd);
                }
            }

            if(areaEntry.contains("scoreSigns")) {
                if(area.isTrackScore()) {
                    ScoreboardSign ss = new ScoreboardSign(areaEntry.getStringList("scoreSigns"), area);
                    Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, ss, 20 * 15, (20 * 60) * 5);
                    area.setScoreboard(ss);
                }
            }

            if(areaEntry.getBoolean("manualRefill", false)) {
                area.setAutomaticRefill(false);
            } else {
                area.setAutomaticRefill(true);
            }

            if(areaEntry.contains("customRespawnLoc") && areaEntry.contains("customRespawnWorld")) {
                List<Double> respawnLoc = areaEntry.getDoubleList("customRespawnLoc");
                if(respawnLoc == null || respawnLoc.size() != 3) {
                    plugin.getLogger().severe("PvE area '" + name + "' has an invalid respawn location, ignoring...");
                    continue;
                }

                String respawnWorld = areaEntry.getString("customRespawnWorld");
                area.setCustomRespawn(new Vector(respawnLoc.get(0), respawnLoc.get(1), respawnLoc.get(2)), respawnWorld);
            }

            if(areaEntry.contains("lootTables")) {
                Set<String> lootTables = areaEntry.getConfigurationSection("lootTables").getKeys(false);
                for(final String lootTableId : lootTables) {
                    ConfigurationSection lootDescriptor = areaEntry
                            .getConfigurationSection("lootTables." + lootTableId);
                    PvELootTable lootTable = new PvELootTable();
                    lootTable.setContent(lootDescriptor.getDouble("content", 20.0));
                    lootTable.setFactor(lootDescriptor.getDouble("factor", 1.0));

                    // Handle items
                    List<Map<?, ?>> lootItems = lootDescriptor.getMapList("loot");
                    for(final Map<?, ?> lootItem : lootItems) {
                        ConfigurationSection lootConfig = Util.mapToConfiguration(lootItem);

                        int id = lootConfig.getInt("itemId", -1);
                        if(id == -1)
                            continue;

                        PvELootTableItem item = new PvELootTableItem();
                        item.setItemId(id);
                        item.setDamageId(lootConfig.getInt("subId", 0));
                        item.setWeight(lootConfig.getInt("weight", 1));

                        if(lootConfig.contains("modifier")) {
                            // Deprecated
                            String modifierId = lootConfig.getString("modifier", "none");
                            IPvEModifier modifier = createModifier(modifierId);

                            if(modifier != null) {
                                try {
                                    modifier.loadConfiguration(lootConfig);
                                } catch(InvalidParameterException e) {
                                    plugin.getLogger().severe("Failed initializing PvE handler: " + e.getMessage());
                                    e.printStackTrace();
                                    modifier = null;
                                }
                            }

                            if(modifier != null)
                                item.addModifier(modifier);
                        } else if(lootConfig.contains("modifiers")) {
                            List<Map<?, ?>> modifiersList = lootConfig.getMapList("modifiers");
                            for(Map<?, ?> modifierMap : modifiersList) {
                                ConfigurationSection modifierEntry = Util.mapToConfiguration(modifierMap);
                                String modifierId = modifierEntry.getString("modifier");

                                IPvEModifier modifier = createModifier(modifierId);

                                if(modifier != null) {
                                    try {
                                        modifier.loadConfiguration(modifierEntry);
                                    } catch(InvalidParameterException e) {
                                        plugin.getLogger().severe("Failed initializing PvE handler: " + e.getMessage());
                                        e.printStackTrace();
                                        modifier = null;
                                    }
                                }

                                if(modifier != null)
                                    item.addModifier(modifier);
                            }

                        }

                        lootTable.addItemToTable(item);
                    }

                    area.addLootTable(lootTableId, lootTable);
                }
            }

            // Read loot locations (chests)
            if(areaEntry.contains("lootLocations")) {
                List<Map<?, ?>> locations = areaEntry.getMapList("lootLocations");
                for(final Map<?, ?> location : locations) {
                    ConfigurationSection locConfig = Util.mapToConfiguration(location);

                    List<Integer> loc = locConfig.getIntegerList("location");
                    if(loc == null || loc.size() != 3)
                        continue;

                    String loot = locConfig.getString("loot");
                    if(loot == null)
                        continue;

                    PvEChest chest = new PvEChest();
                    chest.setWorld(world);
                    chest.setLocation(new Vector(loc.get(0), loc.get(1), loc.get(2)));
                    chest.setLootId(loot);

                    if(locConfig.contains("sets")) {

                        try {
                            @SuppressWarnings("unchecked")
                            Map<String, String> sets = (Map<String, String>) locConfig.get("sets");

                            if(sets == null) {
                                plugin.getLogger().severe("Could not load lootsets!");
                            } else {
                                for(String set : sets.keySet()) {
                                    chest.addLootSet(set, sets.get(set));
                                }
                            }
                        } catch(Exception e) {
                            plugin.getLogger().severe("Could not load lootsets!");
                        }
                    }

                    area.addLootChest(chest);
                }
            }

            pveAreas.put(name, area);
        }
    }

    private void addProtectedArea(PvEArea area) {

        if(area.getRegionType() == RegionType.CUBOID) {
            List<Region> regions = protectedAreas.get(area.getWorld());
            if(regions == null) {
                regions = new ArrayList<Region>();
            }

            regions.add(area.getBoundingBox());
            protectedAreas.put(area.getWorld(), regions);

            List<Region> expRegions = expProtectedAreas.get(area.getWorld());
            if(expRegions == null) {
                expRegions = new ArrayList<Region>();
            }

            expRegions.add(area.getExpBoundingbox());
            expProtectedAreas.put(area.getWorld(), expRegions);
        } else {
            List<PolygonRegion> regions = polygonProtectedAreas.get(area.getWorld());
            if(regions == null) {
                regions = new ArrayList<PolygonRegion>();
            }

            regions.add(area.getPolygonRegion());
            polygonProtectedAreas.put(area.getWorld(), regions);
        }

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("pve") && sender.hasPermission("pvpve.admin")) {
            if(args.length > 0) {
                if(args[0].equalsIgnoreCase("refill")) {
                    if(args.length == 2) {
                        if(pveAreas.containsKey(args[1])) {
                            sender.sendMessage(ChatColor.BLUE + "Refilling " + args[1] + "...");
                            refillArea(pveAreas.get(args[1]));
                            sender.sendMessage(ChatColor.BLUE + "Refill completed");
                        } else {
                            sender.sendMessage(ChatColor.RED + "Specified PvE area not found!");
                        }
                    } else {
                        sender.sendMessage(ChatColor.BLUE + "Refilling all PvE areas...");
                        refillAll();
                        sender.sendMessage(ChatColor.BLUE + "Refill completed");
                    }
                } else if(args[0].equalsIgnoreCase("reload")) {
                    sender.sendMessage(ChatColor.BLUE + "Reloading config");
                    loadConfiguration();
                    queueRefillTask();
                    sender.sendMessage(ChatColor.BLUE + "Config reloaded");
                } else if(args[0].equalsIgnoreCase("debug")) {
                    plugin.getLogger().info(protectedAreas.toString());
                    plugin.getLogger().info(pveAreas.toString());
                } else if(args[0].equalsIgnoreCase("clearitems") && args.length >= 3) {
                    String player = args[1];
                    String tag = args[2];

                    Player ply = Bukkit.getPlayerExact(player);
                    if(ply != null) {
                        ItemStack contents[] = ply.getInventory().getContents();
                        for(int i = 0; i < contents.length; i++) {
                            if(contents[i] == null)
                                continue;

                            if(itemHasName(contents[i], tag)) {
                                contents[i] = null;
                            }
                        }
                        ply.getInventory().setContents(contents);

                        ItemStack armor[] = ply.getInventory().getArmorContents();
                        for(int i = 0; i < armor.length; i++) {
                            if(armor[i] == null)
                                continue;

                            if(!armor[i].hasItemMeta())
                                continue;

                            if(!armor[i].getItemMeta().hasDisplayName())
                                continue;

                            if(itemHasName(armor[i], tag)) {
                                armor[i] = null;
                            }
                        }
                        ply.getInventory().setArmorContents(armor);
                    }

                    if(sender instanceof Player)
                        sender.sendMessage(ChatColor.BLUE + ply.getName() + " inventory cleared of " + tag + " Items.");
                }
            }
            return true;
        }
        return false;
    }

    private boolean itemHasName(ItemStack item, String tag) {
        ItemMeta im = item.getItemMeta();

        if(im.hasDisplayName()) {
            if(im.getDisplayName().contains(tag))
                return true;
        }
        return false;
    }

    public HashMap<String, PvEArea> getPvEArea() {
        return pveAreas;
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent ev) {
        if(!(ev.getEntity() instanceof Player)) {
            if(ev.getCause() == DamageCause.ENTITY_EXPLOSION) {
                final Location loc = ev.getEntity().getLocation();
                Point p = new Point(loc.getBlockX(), loc.getBlockZ());

                for(final PvEArea area : pveAreas.values()) {
                    if(area.getRegionType() == RegionType.CUBOID) {
                        if(area.getBoundingBox().containsLocationFast(loc) && area.getBlockCreeperFriendlyFire()) {
                            ev.setCancelled(true);
                        }
                    } else {
                        if(area.getPolygonRegion().containsLocation(p) && area.getBlockCreeperFriendlyFire()) {
                            if(area.getPolygonRegion().insideLimit(loc)) {
                                ev.setCancelled(true);
                            }
                        }
                    }

                }
            }
        } else {
            if(ev instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent ev2 = (EntityDamageByEntityEvent) ev;

                if(ev2.getDamager() instanceof Player) {
                    return;
                } else if(ev2.getDamager() instanceof Projectile) {
                    Projectile proj = (Projectile) ev2.getDamager();
                    if(proj.getShooter() instanceof Player)
                        return;
                }

                double multiplier = 1.0;
                int maxPriority = -1;
                final Location loc = ev.getEntity().getLocation();
                Point p = new Point(loc.getBlockX(), loc.getBlockZ());

                for(final PvEArea area : pveAreas.values()) {
                    if(area.getRegionType() == RegionType.CUBOID) {
                        if(area.getBoundingBox().containsLocationFast(loc)) {
                            if(area.getPriority() > maxPriority) {
                                maxPriority = area.getPriority();
                                multiplier = area.getMobDamageMultiplier();
                            }
                        }
                    } else {
                        if(area.getPolygonRegion().containsLocation(p)) {
                            if(area.getPolygonRegion().insideLimit(loc)) {
                                if(area.getPriority() > maxPriority) {
                                    maxPriority = area.getPriority();
                                    multiplier = area.getMobDamageMultiplier();
                                }
                            }
                        }
                    }
                }

                if(multiplier != 1.0)
                    ev.setDamage((int) (ev.getDamage() * multiplier));
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent ev) {
        Location loc = ev.getBlock().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        if(!protectedAreas.containsKey(loc.getWorld().getName())
                && !polygonProtectedAreas.containsKey(loc.getWorld().getName()))
            return;

        if(ev.getPlayer().hasPermission("pvpve.pvebuild"))
            return;

        boolean allowBuild = false;
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    Player ply = ev.getPlayer();
                    if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                        allowBuild = true;
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        Player ply = ev.getPlayer();
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                            allowBuild = true;
                    }
                }
            }
        }

        if(allowBuild)
            return;

        if(protectedAreas.get(loc.getWorld().getName()) != null) {
            for(final Region reg : protectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocationFast(loc)) {
                    ev.setCancelled(true);
                    Player ply = ev.getPlayer();
                    ply.sendMessage(ChatColor.RED + "Breaking blocks is not allowed in this area");
                }
            }
        }
        
        if(polygonProtectedAreas.get(loc.getWorld().getName()) != null) {
            for(PolygonRegion reg : polygonProtectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocation(p)) {
                    if(reg.insideLimit(loc)) {
                        ev.setCancelled(true);
                        Player ply = ev.getPlayer();
                        ply.sendMessage(ChatColor.RED + "Breaking blocks is not allowed in this area");
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPlace(BlockPlaceEvent ev) {

        if(ev.getItemInHand() == null || ev.getItemInHand().getType() == Material.AIR)
            return;

        Location loc = ev.getBlock().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        if(!protectedAreas.containsKey(loc.getWorld().getName())
                && !polygonProtectedAreas.containsKey(loc.getWorld().getName()))
            return;

        if(ev.getPlayer().hasPermission("pvpve.pvebuild"))
            return;

        boolean allowBuild = false;
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    Player ply = ev.getPlayer();
                    if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                        allowBuild = true;
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        Player ply = ev.getPlayer();
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                            allowBuild = true;
                    }
                }
            }
        }

        if(allowBuild)
            return;

        if(protectedAreas.get(loc.getWorld().getName()) != null) {
            for(final Region reg : protectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocationFast(loc)) {
                    ev.setCancelled(true);
                    Player ply = ev.getPlayer();
                    ply.sendMessage(ChatColor.RED + "Placing blocks is not allowed in this area");
                    ItemStack is = ply.getItemInHand();
                    ply.setItemInHand(is);
                }
            }
        }

        if(polygonProtectedAreas.get(loc.getWorld().getName()) != null) {
            for(PolygonRegion reg : polygonProtectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocation(p)) {
                    if(reg.insideLimit(loc)) {
                        ev.setCancelled(true);
                        Player ply = ev.getPlayer();
                        ply.sendMessage(ChatColor.RED + "Placing blocks is not allowed in this area");
                        ItemStack is = ply.getItemInHand();
                        ply.setItemInHand(is);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBucketEmpty(PlayerBucketEmptyEvent ev) {
        Location loc = ev.getBlockClicked().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        if(!protectedAreas.containsKey(loc.getWorld().getName()))
            return;

        if(ev.getPlayer().hasPermission("pvpve.pvebuild"))
            return;

        boolean allowBuild = false;
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    Player ply = ev.getPlayer();
                    if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                        allowBuild = true;
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        Player ply = ev.getPlayer();
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                            allowBuild = true;
                    }
                }
            }
        }

        if(allowBuild)
            return;

        if(protectedAreas.get(loc.getWorld().getName()) != null) {
            for(final Region reg : protectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocationFast(loc)) {
                    ev.setCancelled(true);
                    Player ply = ev.getPlayer();
                    ply.sendMessage(ChatColor.RED + "Using buckets is not allowed in this area");
                }
            }
        }

        if(polygonProtectedAreas.get(loc.getWorld().getName()) != null) {
            for(PolygonRegion reg : polygonProtectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocation(p)) {
                    if(reg.insideLimit(loc)) {
                        ev.setCancelled(true);
                        Player ply = ev.getPlayer();
                        ply.sendMessage(ChatColor.RED + "Using buckets is not allowed in this area");
                    }
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBucketFill(PlayerBucketFillEvent ev) {
        Location loc = ev.getBlockClicked().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        if(!protectedAreas.containsKey(loc.getWorld().getName())
                && !polygonProtectedAreas.containsKey(loc.getWorld().getName()))
            return;

        if(ev.getPlayer().hasPermission("pvpve.pvebuild"))
            return;

        boolean allowBuild = false;
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    Player ply = ev.getPlayer();
                    if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                        allowBuild = true;
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        Player ply = ev.getPlayer();
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                            allowBuild = true;
                    }
                }
            }
        }

        if(allowBuild)
            return;

        if(protectedAreas.get(loc.getWorld().getName()) != null) {
            for(final Region reg : protectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocationFast(loc)) {
                    ev.setCancelled(true);
                    Player ply = ev.getPlayer();
                    ply.sendMessage(ChatColor.RED + "Using buckets is not allowed in this area");
                }
            }
        }

        if(polygonProtectedAreas.get(loc.getWorld().getName()) != null) {
            for(PolygonRegion reg : polygonProtectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocation(p)) {
                    if(reg.insideLimit(loc)) {
                        ev.setCancelled(true);
                        Player ply = ev.getPlayer();
                        ply.sendMessage(ChatColor.RED + "Using buckets is not allowed in this area");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent ev) {
        Location loc = ev.getEntity().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());
        Player ply = (Player) ev.getEntity();

        for(final PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc) && area.hasCustomRespawn()) {
                    customRespawn.put(ev.getEntity(), area.getRespawnLocation());
                }
                
                if(area.getKeepDeathItems()) {
                	ply.getInventory().setContents((ItemStack[]) ev.getDrops().toArray());
                	ev.getDrops().clear();
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p) && area.hasCustomRespawn()) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        customRespawn.put(ev.getEntity(), area.getRespawnLocation());
                    }
                    
                    if(area.getKeepDeathItems()) {
                    	ply.getInventory().setContents((ItemStack[]) ev.getDrops().toArray());
                    	ev.getDrops().clear();
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent ev) {
        if(customRespawn.containsKey(ev.getPlayer())) {
            ev.setRespawnLocation(customRespawn.get(ev.getPlayer()));
            customRespawn.remove(ev.getPlayer());
        }
    }

    // Creeper/TNT nerf
    @EventHandler(priority = EventPriority.HIGH)
    public void onExplosion(EntityExplodeEvent ev) {
        Location loc = ev.getLocation();

        if(!expProtectedAreas.containsKey(loc.getWorld().getName()))
            return;

        if(expProtectedAreas.containsKey(loc.getWorld().getName())) {
            for(final Region reg : expProtectedAreas.get(loc.getWorld().getName())) {
                if(reg.containsLocationFast(loc))
                    ev.blockList().clear();
            }
        }
        
        if(polygonProtectedAreas.containsKey(loc.getWorld().getName())) {
            Point p = new Point(loc.getBlockX(), loc.getBlockZ());
            for(PolygonRegion reg : polygonProtectedAreas.get(loc.getWorld().getName())) {
                if(reg.insideBounds(p)) {
                    ev.blockList().clear();
                }
            }
        }
    }

    // Handle locked dispensers
    @EventHandler
    public void onOpenInventory(InventoryOpenEvent ev) {
        if(ev.getView().getType() != InventoryType.DISPENSER)
            return;

        if(ev.getPlayer().hasPermission("pvpve.pvebuild"))
            return;

        Location loc = ev.getPlayer().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        for(final PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc) && area.getLockDispensers()) {
                    if(ev.getPlayer() instanceof Player) {
                        Player ply = (Player) ev.getPlayer();
                        if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                            return;
                        ev.setCancelled(true);
                        ply.sendMessage(ChatColor.RED + "You are not allowed to open dispensers in this area.");
                    }
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p) && area.getLockDispensers()) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        if(ev.getPlayer() instanceof Player) {
                            Player ply = (Player) ev.getPlayer();
                            if(ply.hasPermission("pvpve.arenabuild." + area.getName()))
                                return;
                            ev.setCancelled(true);
                            ply.sendMessage(ChatColor.RED + "You are not allowed to open dispensers in this area.");
                        }
                    }
                }
            }
        }
    }

    // Handle sleeping
    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent ev) {
        Location loc = ev.getPlayer().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        for(final PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc) && area.getNoSleeping()) {
                    ev.setCancelled(true);
                    ev.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to sleep in this area.");
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p) && area.getNoSleeping()) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        ev.setCancelled(true);
                        ev.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to sleep in this area.");
                    }
                }
            }

        }
    }

    // Handle blocked commands
    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent ev) {

        if(ev.getPlayer().hasPermission("pvpve.pvecommand"))
            return;

        String cmd = ev.getMessage().toLowerCase();
        for(String blockedCommand : blockedCommands) {
            if(cmd.startsWith(blockedCommand)) {
                Location loc = ev.getPlayer().getLocation();
                Point p = new Point(loc.getBlockX(), loc.getBlockZ());

                for(final PvEArea area : pveAreas.values()) {
                    if(area.getRegionType() == RegionType.CUBOID) {
                        if(area.getBoundingBox().containsLocation(loc) && area.isBlockedCommmand(cmd)) {
                            ev.setCancelled(true);
                            Player ply = ev.getPlayer();
                            ply.sendMessage(ChatColor.RED + "You are not allowed to use this command in this area.");
                        }
                    } else {
                        if(area.getPolygonRegion().containsLocation(p) && area.isBlockedCommmand(cmd)) {
                            if(area.getPolygonRegion().insideLimit(loc)) {
                                ev.setCancelled(true);
                                Player ply = ev.getPlayer();
                                ply.sendMessage(ChatColor.RED + "You are not allowed to use this command in this area.");
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void OnPlayerInteract(PlayerInteractEvent ev) {
        Location loc = ev.getPlayer().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());
        for(final PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {

                    if((ev.getAction() == Action.RIGHT_CLICK_BLOCK) || (ev.getAction() == Action.RIGHT_CLICK_AIR)) {
                        ItemStack item = ev.getPlayer().getItemInHand();
                        if(item == null)
                            continue;

                        if(area.getNoVehicles()
                                && (item.getType() == Material.MINECART || item.getType() == Material.BOAT)) {
                            ev.setCancelled(true);
                            ev.getPlayer().sendMessage(
                                    ChatColor.RED + "You are not allowed to use vehicles in this area.");
                        } else if(area.getNoPearls() && item.getType() == Material.ENDER_PEARL) {
                            ev.setCancelled(true);
                            ev.getPlayer().sendMessage(
                                    ChatColor.RED + "You are not allowed to use ender pearls in this area.");
                        }
                    }
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        if((ev.getAction() == Action.RIGHT_CLICK_BLOCK) || (ev.getAction() == Action.RIGHT_CLICK_AIR)) {
                            ItemStack item = ev.getPlayer().getItemInHand();
                            if(item == null)
                                continue;

                            if(area.getNoVehicles()
                                    && (item.getType() == Material.MINECART || item.getType() == Material.BOAT)) {
                                ev.setCancelled(true);
                                ev.getPlayer().sendMessage(
                                        ChatColor.RED + "You are not allowed to use vehicles in this area.");
                            } else if(area.getNoPearls() && item.getType() == Material.ENDER_PEARL) {
                                ev.setCancelled(true);
                                ev.getPlayer().sendMessage(
                                        ChatColor.RED + "You are not allowed to use ender pearls in this area.");
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent ev) {
        IgniteCause cause = ev.getCause();
        Location loc = ev.getBlock().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());

        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc)) {
                    if(!area.getFireSpread() && cause == IgniteCause.SPREAD) {
                        ev.setCancelled(true);
                    }
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p)) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        if(!area.getFireSpread() && cause == IgniteCause.SPREAD) {
                            ev.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent ev) {
        Location loc = ev.getBlock().getLocation();
        Point p = new Point(loc.getBlockX(), loc.getBlockZ());
        for(PvEArea area : pveAreas.values()) {
            if(area.getRegionType() == RegionType.CUBOID) {
                if(area.getBoundingBox().containsLocation(loc) && !area.getFireSpread()) {
                    ev.setCancelled(true);
                }
            } else {
                if(area.getPolygonRegion().containsLocation(p) && !area.getFireSpread()) {
                    if(area.getPolygonRegion().insideLimit(loc)) {
                        ev.setCancelled(true);
                    }
                }
            }
        }

    }

}
