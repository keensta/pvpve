package com.lrns123.pvpve.pve;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.lrns123.pvpve.pve.modifiers.IPvEModifier;
import com.lrns123.pvpve.util.IWeightedItem;

public class PvELootTableItem implements IWeightedItem {
	private int itemId = -1;
	private int damageId = 0;
	private int weight = 1;
	private List<IPvEModifier> modifiers = new ArrayList<IPvEModifier>();
	
	public String toString()
	{
		return "PvELootTableItem(id:" + itemId + ",dmg:" + damageId + ",weight:" + weight + ",modifiers:" + modifiers + ")";
	}
	
	public int getItemId() {
		return itemId;
	}
	
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	
	public int getDamageId() {
		return damageId;
	}
	
	public void setDamageId(int damageId) {
		this.damageId = damageId;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public void addModifier(IPvEModifier handler) {
		this.modifiers.add(handler);
	}
	
	public void clearModifiers() {
		this.modifiers.clear();
	}

	public IPvEModifier[] getModifiers() {
		return modifiers.toArray(new IPvEModifier[0]);
	}
	
    public ItemStack getGeneratedItem()
	{
		@SuppressWarnings("deprecation")
        ItemStack item = new ItemStack(itemId, 1, (short)damageId);
		for (IPvEModifier mod : modifiers)
			item = mod.processItem(item);
		
		return item;
	}

	@Override
	public int getRandomWeight() {
		return weight;
	}
}
