package com.lrns123.pvpve.pve.scoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import com.lrns123.pvpve.pve.PvEArea;

public class ScoreboardSign implements Runnable{
	
	private PvEArea area;
	
	private List<String> signLocations = new ArrayList<String>();
	Logger log = Logger.getLogger("Minecraft");
	
	public ScoreboardSign(List<String> locations, PvEArea area) {
		this.area = area;
		this.signLocations = locations;
	}

	@Override
	public void run() {
		log.info("Updating " + area.getName() + " scoreboards!");
		
		List<PlayerScoreCard> scoreCards = area.getScorecards();
		
		if(scoreCards == null)
			return;
		
		if(!scoreCards.isEmpty())
			Collections.sort(scoreCards, Collections.reverseOrder(new ScoreComparator()));
		
		for(int i=0; i < scoreCards.size(); i++) {
			if(i < signLocations.size()) {
				String[] splitLoc = signLocations.get(i).split(",");
				Location loc = new Location(Bukkit.getWorld(area.getWorld()), Double.parseDouble(splitLoc[0]), 
						Double.parseDouble(splitLoc[1]), Double.parseDouble(splitLoc[2]));
				
				Block b = loc.getBlock();
				
				if(b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST) {
					Sign sign = (Sign) b.getState();
					
					PlayerScoreCard psc = scoreCards.get(i);
					
					sign.setLine(0, ChatColor.GOLD +" Rank #"+(i+1));
					sign.setLine(1, ChatColor.AQUA + psc.getPlayerName());
					sign.setLine(2, "Score: " + psc.getScore());
					sign.setLine(3, ChatColor.GREEN + "K: " + psc.getKills() + ChatColor.RED + " D: " + psc.getDeaths());
					sign.update();
					sign.update(true);
				}
				
			}
		}
		
		for(int i = scoreCards.size(); i < signLocations.size(); i++) {
			
			String[] splitLoc = signLocations.get(i).split(",");
			Location loc = new Location(Bukkit.getWorld(area.getWorld()), Double.parseDouble(splitLoc[0]), 
					Double.parseDouble(splitLoc[1]), Double.parseDouble(splitLoc[2]));
			
			Block b = loc.getBlock();
			
			if(b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST) {
				Sign sign = (Sign) b.getState();
				
				sign.setLine(0, ChatColor.GOLD +" Rank #"+(i+1));
				sign.setLine(1, ChatColor.AQUA + "No Ranked Player");
				sign.setLine(2, "Score: " + "---");
				sign.setLine(3, ChatColor.GREEN + "K: " + "--" + ChatColor.RED + " D: " + "--");
				sign.update();
				sign.update(true);
			}
			
		}
		
	}

}
