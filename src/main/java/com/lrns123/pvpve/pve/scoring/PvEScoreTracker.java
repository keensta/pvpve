package com.lrns123.pvpve.pve.scoring;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.pve.PvEArea;
import com.lrns123.pvpve.pve.PvEManager;
import com.lrns123.pvpve.util.RegionType;

public class PvEScoreTracker implements Listener, CommandExecutor{
	
	public PvPvE plugin;
	private PvEManager pveManager;
	
	private HashMap<PvEArea, HashMap<String, PlayerScoreCard>> areaScoreCards = new HashMap<PvEArea, HashMap<String,PlayerScoreCard>>();
	
	public PvEScoreTracker(PvPvE plugin, PvEManager pveManager) {
		this.plugin = plugin;
		this.pveManager = pveManager;
		
		loadScores();
		
		plugin.getCommand("score").setExecutor(this);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	public void savePlayerCard(PlayerScoreCard psc, PvEArea area) {
	    FileConfiguration cfg = loadConfig("scores");
	    
	    cfg.set("areas." + area.getName() + "." + psc.getPlayerName() + ".scores", psc.getScore());
	    cfg.set("areas." + area.getName() + "." + psc.getPlayerName() + ".kills", psc.getKills());
	    cfg.set("areas." + area.getName() + "." + psc.getPlayerName() + ".deaths", psc.getDeaths());
	    saveConfig(cfg, "scores");
	}
	
	public void loadScores() {
	    areaScoreCards.clear();
	    
	    FileConfiguration cfg = loadConfig("scores");
	    
	    if(!cfg.contains("areas"))
	        return;
	    
	    Set<String> areas = cfg.getConfigurationSection("areas").getKeys(false);
	    
	    for(String area : areas) {
	        plugin.getLogger().info("Loading area score " + area);
	        
	        PvEArea pveArea = pveManager.getPvEArea(area);
	        
	        if(pveArea == null)
	            continue;
	        
	        Set<String> players = cfg.getConfigurationSection("areas." + area).getKeys(false);
	        HashMap<String, PlayerScoreCard> scorecards = new HashMap<String, PlayerScoreCard>();
	        
	        for(String player : players) {
	            int score = cfg.getInt("areas." + area + "." + player + ".scores");
	            int kills = cfg.getInt("areas." + area + "." + player + ".kills");
	            int deaths = cfg.getInt("areas." + area + "." + player + ".deaths");
	            PlayerScoreCard psc = new PlayerScoreCard(player, score, kills, deaths);
	            
	            scorecards.put(player, psc);
	            pveArea.addScoreCard(psc);
	        }
	        
	        plugin.getLogger().info("Size " + scorecards.size());
	        areaScoreCards.put(pveArea, scorecards);
	    }
	}
	
	   
    public void saveConfig(FileConfiguration cfg, String name) {
        try {
            cfg.save(new File(plugin.getDataFolder(), name+".yml"));
        } catch (IOException e) {
            Logger.getLogger("Minecraft").severe("[PvPvE] Failed to save " + name +".yml!");
            e.printStackTrace();
        }
    }
    
    public FileConfiguration loadConfig(String filename) {
        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), filename+".yml"));
    }
	
/*	public void resetAreaData(PvEArea pveArea) {
		NBTTagCompound data = plugin.loadNBTConfig("score.nbt", false);
		
		if(data == null)
			return;
		
		NBTTagCompound areas = data.getCompound("areas");
		
		if(areas == null)
			return;
		
		NBTTagCompound area = areas.getCompound(pveArea.getName());
		
		if(area == null)
			return;
		
		areas.remove(area.getName());
		data.set(areas.getName(), areas);
		
		File f = new File(plugin.getDataFolder(), "score.nbt");
		
		try {
			FileOutputStream out = new FileOutputStream(f);
			NBTCompressedStreamTools.a(data, out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removePlayerData(String playerName, PvEArea pveArea) {
		NBTTagCompound data = plugin.loadNBTConfig("score.nbt", false);
		
		if(data == null)
			return;
		
		NBTTagCompound areas = data.getCompound("areas");
		
		if(areas == null)
			return;
		
		NBTTagCompound area = areas.getCompound(pveArea.getName());
		
		if(area == null)
			return;
		
		NBTTagCompound player = area.getCompound(playerName);
		
		if(player == null)
			return;
		
		area.remove(player.getName());
		areas.set(area.getName(), area);
		data.set(areas.getName(), areas);
		
		File f = new File(plugin.getDataFolder(), "score.nbt");
		
		try {
			FileOutputStream out = new FileOutputStream(f);
			NBTCompressedStreamTools.a(data, out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("score") && sender.hasPermission("pvpve.admin")) {
			if(args.length < 0) {
				sender.sendMessage(ChatColor.RED + "No help yet!!");
				return false;
			} else {
				if(args[0].equalsIgnoreCase("reset")) {
					if(args.length < 1) {
						sender.sendMessage(ChatColor.RED + "Include a area to reset. Usage '/score reset [AreaName]'");
						return false;
					}
					
					PvEArea area = pveManager.getPvEArea(args[1]);
					
					if(area == null) {
						sender.sendMessage(ChatColor.RED + "Area doesn't exist!");
						return false;
					}
					
					if(!area.isTrackScore()) {
						sender.sendMessage(ChatColor.RED + "Area not score tracked!");
						return false;
					}
					
					//resetAreaData(area);
					area.getScorecards().clear();
					areaScoreCards.remove(area);
					sender.sendMessage(ChatColor.GREEN + area.getName() + " scores has been reset!");
				}
				
				if(args[0].equalsIgnoreCase("removeplayer")) {
					if(args.length < 1) {
						sender.sendMessage(ChatColor.RED + "Include player to remove. Usage '/score removeplayer [AreaName] [playername]'");
						return false;
					}
					
					PvEArea area = pveManager.getPvEArea(args[1]);
					
					if(area == null) {
						sender.sendMessage(ChatColor.RED + "Area doesn't exist!");
						return false;
					}
					
					if(!area.isTrackScore()) {
						sender.sendMessage(ChatColor.RED + "Area not score tracked!");
						return false;
					}
					
					//removePlayerData(args[2], area);
					HashMap<String, PlayerScoreCard> playerScoreCards = (areaScoreCards.containsKey(area) ? 
							areaScoreCards.get(area) : new HashMap<String, PlayerScoreCard>());
					PlayerScoreCard psc = (playerScoreCards.containsKey(args[2]) ? 
							playerScoreCards.get(args[2]) : new PlayerScoreCard(args[2]));
					
					if(area.hasScoreCard(psc))
						area.removeScoreCard(psc);
					
					if(playerScoreCards.containsKey(args[2]))
						playerScoreCards.remove(args[2]);
					
					areaScoreCards.put(area, playerScoreCards);
					
					sender.sendMessage(ChatColor.GREEN + args[2] + " has been removed.");
				}
			}
			
		}
		return false;
	}
	
	@EventHandler
	public void onMobDeath(EntityDeathEvent ev) {
		Entity victim = ev.getEntity();
		Player killer = ev.getEntity().getKiller();
		
		if(killer == null)
			return;
		
		if(!(victim instanceof Player)) {
			Location loc = killer.getLocation();
			Point p = new Point(loc.getBlockX(), loc.getBlockZ());
			
			for(PvEArea area : pveManager.pveAreas.values()) {
			    if(area.getRegionType() == RegionType.CUBOID) {
			        if(area.getBoundingBox().containsLocation(loc) && area.isTrackScore()) {
	                    HashMap<String, PlayerScoreCard> playerScoreCards = (areaScoreCards.containsKey(area) ? 
	                            areaScoreCards.get(area) : new HashMap<String, PlayerScoreCard>());
	                    PlayerScoreCard psc = (playerScoreCards.containsKey(killer.getName()) ? 
	                            playerScoreCards.get(killer.getName()) : new PlayerScoreCard(killer.getName()));
	                    
	                    if(area.hasScoreCard(psc))
	                        area.removeScoreCard(psc);
	                    
	                    psc.setScore(psc.getScore() + 5);
	                    psc.setKills(psc.getKills() + 1);
	                    
	                    playerScoreCards.put(psc.getPlayerName(), psc);
	                    areaScoreCards.put(area, playerScoreCards);
	                    area.addScoreCard(psc);
	                    savePlayerCard(psc, area);
	                }
			    } else {
			        if(area.getPolygonRegion().containsLocation(p) && area.isTrackScore()) {
			            if(area.getPolygonRegion().insideLimit(loc)) {
		                     HashMap<String, PlayerScoreCard> playerScoreCards = (areaScoreCards.containsKey(area) ? 
		                                areaScoreCards.get(area) : new HashMap<String, PlayerScoreCard>());
		                        PlayerScoreCard psc = (playerScoreCards.containsKey(killer.getName()) ? 
		                                playerScoreCards.get(killer.getName()) : new PlayerScoreCard(killer.getName()));
		                        
		                        if(area.hasScoreCard(psc))
		                            area.removeScoreCard(psc);
		                        
		                        psc.setScore(psc.getScore() + 5);
		                        psc.setKills(psc.getKills() + 1);
		                        
		                        playerScoreCards.put(psc.getPlayerName(), psc);
		                        areaScoreCards.put(area, playerScoreCards);
		                        area.addScoreCard(psc);
		                        savePlayerCard(psc, area);
			            }
			        }
			    }
			}
			
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent ev) {
		Player victim = ev.getEntity();
		Entity killer = ev.getEntity().getKiller();
		
		if(!(killer instanceof Player)) {
			Location loc = victim.getLocation();
			Point p = new Point(loc.getBlockX(), loc.getBlockZ());
			
			for(PvEArea area : pveManager.pveAreas.values()) {
			    if(area.getRegionType() == RegionType.CUBOID) {
	                if(area.getBoundingBox().containsLocation(loc) && area.isTrackScore()) {
	                    HashMap<String, PlayerScoreCard> playerScoreCards = (areaScoreCards.containsKey(area) ? 
	                            areaScoreCards.get(area) : new HashMap<String, PlayerScoreCard>());
	                    PlayerScoreCard psc = (playerScoreCards.containsKey(victim.getName()) ? 
	                            playerScoreCards.get(victim.getName()) : new PlayerScoreCard(victim.getName()));
	                    
	                    if(area.hasScoreCard(psc))
	                        area.removeScoreCard(psc);
	                    
	                    psc.setScore(psc.getScore() - 10);
	                    psc.setDeaths(psc.getDeaths() + 1);
	                    
	                    playerScoreCards.put(psc.getPlayerName(), psc);
	                    areaScoreCards.put(area, playerScoreCards);
	                    area.addScoreCard(psc);
	                    savePlayerCard(psc, area);
	                }
			    } else {
			        if(area.getPolygonRegion().containsLocation(p) && area.isTrackScore()) {
			            if(area.getPolygonRegion().insideLimit(loc)) {
			                HashMap<String, PlayerScoreCard> playerScoreCards = (areaScoreCards.containsKey(area) ? 
	                                areaScoreCards.get(area) : new HashMap<String, PlayerScoreCard>());
	                        PlayerScoreCard psc = (playerScoreCards.containsKey(victim.getName()) ? 
	                                playerScoreCards.get(victim.getName()) : new PlayerScoreCard(victim.getName()));
	                        
	                        if(area.hasScoreCard(psc))
	                            area.removeScoreCard(psc);
	                        
	                        psc.setScore(psc.getScore() - 10);
	                        psc.setDeaths(psc.getDeaths() + 1);
	                        
	                        playerScoreCards.put(psc.getPlayerName(), psc);
	                        areaScoreCards.put(area, playerScoreCards);
	                        area.addScoreCard(psc);
	                        savePlayerCard(psc, area);
			            }
			        }
			    }
			}
		}
	}


}
