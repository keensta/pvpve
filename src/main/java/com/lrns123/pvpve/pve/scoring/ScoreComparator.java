package com.lrns123.pvpve.pve.scoring;

import java.util.Comparator;

public class ScoreComparator implements Comparator<PlayerScoreCard> {

	@Override
	public int compare(PlayerScoreCard psc1, PlayerScoreCard psc2) {
		return (psc1.getScore() < psc2.getScore()) ? -1 : (psc1.getScore() > psc2.getScore()) ? 1:0;
	}

}
