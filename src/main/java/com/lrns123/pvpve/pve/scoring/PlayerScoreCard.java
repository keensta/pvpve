package com.lrns123.pvpve.pve.scoring;

public class PlayerScoreCard implements Comparable<PlayerScoreCard>{
	
	private String playerName = "";
	private Integer score = 0;
	private int kills = 0;
	private int deaths = 0;
	
	public PlayerScoreCard(String playerName) {
		this.playerName = playerName;
	}
	
	public PlayerScoreCard(String playerName, int score, int kills, int deaths) {
		this.playerName = playerName;
		this.score = score;
		this.kills = kills;
		this.deaths = deaths;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getKills() {
		return kills;
	}
	public void setKills(int kills) {
		this.kills = kills;
	}
	public int getDeaths() {
		return deaths;
	}
	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	@Override
	public int compareTo(PlayerScoreCard pSC) {
		return this.score.compareTo(pSC.score);
	}
	
	public String toString() {
		return "PlayerScoreCard(" + playerName + ", Score: " + score + ", K: " + kills + ", D: " + deaths + ")";
	}

}
