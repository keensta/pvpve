package com.lrns123.pvpve.pve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class PvEChest {
	private String world = null;
	private Vector location = null;
	private String lootId = null;
	private Map<String, String> lootSets = new HashMap<String, String>();

	private Inventory inventory = null;
	private ArrayList<Integer> emptySlots = null;

	public String toString() {
		return "PvEChest(at:" + world + "(" + location + "), loot:" + lootId + ", sets:" + lootSets.toString() + ")";
	}

	public boolean findChest() {
		inventory = null;

		World world = Bukkit.getWorld(this.world);
		if (world == null)
			return false;

		Block block = world.getBlockAt(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		if (block == null)
			return false;

		if (block.getState() instanceof InventoryHolder) {
			inventory = ((InventoryHolder) block.getState()).getInventory();
			enumEmptySlots();
			return true;
		}

		return false;
	}

	private void enumEmptySlots() {
		emptySlots = new ArrayList<Integer>();

		if (inventory != null) {
			ItemStack[] items = inventory.getContents();
			for (int i = 0; i < items.length; i++) {
				if (items[i] == null || items[i].getType() == Material.AIR) {
					emptySlots.add(i);
				}
			}
		}
	}

	public void clearChest() {
		if (inventory != null)
			inventory.clear();
		enumEmptySlots();
	}

	public boolean addItem(ItemStack item) {
		if (emptySlots == null)
			enumEmptySlots();

		if (emptySlots.size() == 0)
			return false;

		Random rn = new Random();
		int slotIdx = rn.nextInt(emptySlots.size());

		int slot = emptySlots.get(slotIdx);

		if (inventory != null) {
			inventory.setItem(slot, item);
			emptySlots.remove(slotIdx);
		}
		return true;
	}

	public void setLocation(Vector location) {
		this.location = location;
	}

	public Vector getLocation() {
		return location;
	}

	public void setLootId(String lootId) {
		this.lootId = lootId;
	}

	public String getLootId() {
		return lootId;
	}
	
	public String getLootId(String set) {
		if (lootSets.containsKey(set)) {
			return lootSets.get(set);
		} else {
			return lootId;
		}
	}
	
	public void addLootSet(String set, String lootId) {
		lootSets.put(set, lootId);
	}
	
	public void clearLootSets() {
		lootSets.clear();
	}

	public void setWorld(String world) {
		this.world = world;
	}

	public String getWorld() {
		return world;
	}
}
