package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;

import org.bukkit.Color;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class ColorModifier implements IPvEModifier {

	private int color = -1;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		int red = entry.getInt("red", 255);
		int green = entry.getInt("green", 255);
		int blue = entry.getInt("blue", 255);
		
		color = red << 16 | green << 8 | blue;
	}
	
	/*@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("color"))
			return;
		
		color = lootEntry.getInt("color");
	}*/

	@Override
	public ItemStack processItem(ItemStack item) {
		
		if (color == -1)
			return item;
		
		try {
			LeatherArmorMeta meta = (LeatherArmorMeta)item.getItemMeta();
			meta.setColor(Color.fromRGB(color));
			
			item.setItemMeta(meta);
		} catch (Exception e) {}
		
		return item;
	}
}
