package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import net.minecraft.server.v1_7_R1.NBTTagCompound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import com.lrns123.pvpve.util.Util;

public class TagModifier implements IPvEModifier {

	private String tagid = null;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("tagid"))
			return;
		
		tagid = entry.getString("tagid");
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("tagid"))
			return;
		
		tagid = lootEntry.getString("tagid");
	}
*/
	@Override
	public ItemStack processItem(ItemStack item) {
		
		if (tagid == null)
			return item;
		
		NBTTagCompound tag = Util.getOrCreateItemTag(item);
		tag.setString("PvETag", tagid);
		
		return item;
	}


	

}
