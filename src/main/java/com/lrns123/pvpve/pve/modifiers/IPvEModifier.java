package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;


import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public interface IPvEModifier {
	
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException;
	//public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException;
	public ItemStack processItem(ItemStack item);
	
}
