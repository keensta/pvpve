package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Random;


import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public class QuantityModifier implements IPvEModifier {

	private int min;
	private int max;
	
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException
	{
		List<Integer> range = entry.getIntegerList("range");
		if (range == null || range.size() != 2) {
			throw new InvalidParameterException("No range specified");
		}
		
		min = range.get(0);
		max = range.get(1);
		
		if (min < 1) min = 1;
		else if (min > 64) min = 64;
		
		if (max < 1) max = 1;
		else if (max > 64) max = 64;
		
		if (min > max) {
			int temp = min;
			min = max;
			max = temp;
		}
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		NBTTagList rangeList = lootEntry.getList("range");
		if (rangeList.size() != 2) {
			throw new InvalidParameterException("No range specified");
		}
		
		try {
			min = ((NBTTagInt)rangeList.get(0)).data;
			max = ((NBTTagInt)rangeList.get(1)).data;
			
			if (min < 1) min = 1;
			else if (min > 64) min = 64;
			
			if (max < 1) max = 1;
			else if (max > 64) max = 64;
			
			if (min > max) {
				int temp = min;
				min = max;
				max = temp;
			}
		} catch (Exception e) {
			throw new InvalidParameterException("Invalid range specified");
		}
		
	}*/

	@Override
	public ItemStack processItem(ItemStack item)
	{
		Random rn = new Random();
		
		int cmax = max - min + 1;
		int num = rn.nextInt(cmax) + min;
		
		if (num < 1) num = 1;
		if (num > 64) num = 64;
		
		item.setAmount(num);
		
		return item;
	}

	
}
