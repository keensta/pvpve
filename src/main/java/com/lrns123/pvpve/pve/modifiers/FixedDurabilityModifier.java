package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public class FixedDurabilityModifier implements IPvEModifier {

	private int durability;
	private boolean reverse;
	private boolean percentage;
	
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException
	{
		durability = entry.getInt("durability", 0);
		
		reverse = entry.getBoolean("reverse", false);
		percentage = entry.getBoolean("percentage", false);
		
		if (percentage) {
			if (durability < 0) durability = 0;
			else if (durability > 100) durability = 100;
		}		
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		durability = lootEntry.getInt("durability");
		
		reverse = lootEntry.getBoolean("reverse");
		percentage = lootEntry.getBoolean("percentage");
		
		if (percentage) {
			if (durability < 0) durability = 0;
			else if (durability > 100) durability = 100;
		}		
	}*/

	@Override
	public ItemStack processItem(ItemStack item)
	{
		int num = durability;
		
		if (item.getType().getMaxDurability() != 0) {
			if (reverse)
				num = item.getType().getMaxDurability() - num;
			
			if (percentage)
				num = (int)((double)num * ((double)item.getType().getMaxDurability() / 100.0));
		}
		
		item.setDurability((short)num);
		
		return item;
	}

	
}
