package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullNameModifier implements IPvEModifier {

	private String name = null;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("skullname"))
			return;
		
		name = entry.getString("skullname");
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("skullname"))
			return;
		
		name = lootEntry.getString("skullname");
	}*/

	@Override
	public ItemStack processItem(ItemStack item) {
		
		if (name == null)
			return item;
		
		try {
			SkullMeta meta = (SkullMeta)item.getItemMeta();
			meta.setOwner(name);
			
			item.setItemMeta(meta);
		} catch (Exception e) {
			
		}
		
		return item;
	}


	

}
