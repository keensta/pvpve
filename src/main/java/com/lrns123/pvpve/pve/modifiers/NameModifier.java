package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NameModifier implements IPvEModifier {

	private String name = null;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("name"))
			return;
		
		name = entry.getString("name");
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("name"))
			return;
		
		name = lootEntry.getString("name");
	}*/

	@Override
	public ItemStack processItem(ItemStack item) {
		
		if (name == null)
			return item;
		
		ItemMeta meta = item.getItemMeta();
		String displayName = item.getType().toString().toLowerCase();
		
		meta.setDisplayName(name + " " + displayName);
		
		item.setItemMeta(meta);

		return item;
	}


	

}
