package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public class FixedQuantityModifier implements IPvEModifier {
	
	private int quantity;
	
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException
	{
		quantity = entry.getInt("quantity", 0);
		
		if (quantity < 1) quantity = 1;
		else if (quantity > 64 ) quantity = 64;
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		quantity = lootEntry.getInt("quantity");

		if (quantity < 1) quantity = 1;
		else if (quantity > 64 ) quantity = 64;
	}*/

	@Override
	public ItemStack processItem(ItemStack item)
	{
		item.setAmount(quantity);
		
		return item;
	}



}
