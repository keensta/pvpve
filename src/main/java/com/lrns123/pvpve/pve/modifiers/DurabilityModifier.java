package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Random;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public class DurabilityModifier implements IPvEModifier {

	private int min;
	private int max;
	private boolean reverse;
	private boolean percentage;
	
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException
	{
		List<Integer> range = entry.getIntegerList("range");
		if (range == null || range.size() != 2) {
			throw new InvalidParameterException("No range specified");
		}
		
		min = range.get(0);
		max = range.get(1);
		
		reverse = entry.getBoolean("reverse", false);
		percentage = entry.getBoolean("percentage", false);
		
		if (percentage) {
			if (min < 0) min = 0;
			else if (min > 100) min = 100;
			
			if (max < 0) max = 0;
			else if (max > 100) max = 100;
		}
		
		if (min > max) {
			int temp = min;
			min = max;
			max = temp;
		}
		
		
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		NBTTagList rangeList = lootEntry.getList("range");
		if (rangeList.size() != 2) {
			throw new InvalidParameterException("No range specified");
		}
		
		try {
			min = ((NBTTagInt)rangeList.get(0)).data;
			max = ((NBTTagInt)rangeList.get(1)).data;
			
			reverse = lootEntry.getBoolean("reverse");
			percentage = lootEntry.getBoolean("percentage");
			
			if (percentage) {
				if (min < 0) min = 0;
				else if (min > 100) min = 100;
				
				if (max < 0) max = 0;
				else if (max > 100) max = 100;
			}

			if (min > max) {
				int temp = min;
				min = max;
				max = temp;
			}
		} catch (Exception e) {
			throw new InvalidParameterException("Invalid range specified");
		}
		
	}
*/
	@Override
	public ItemStack processItem(ItemStack item)
	{
		Random rn = new Random();
		
		int cmax = max - min + 1;
		int num = rn.nextInt(cmax) + min;
		
		if (item.getType().getMaxDurability() != 0) {
			if (reverse)
				num = item.getType().getMaxDurability() - num;
			
			if (percentage)
				num = (int)((double)num * ((double)item.getType().getMaxDurability() / 100.0));
		}
		
		item.setDurability((short)num);
		
		return item;
	}

	
}
