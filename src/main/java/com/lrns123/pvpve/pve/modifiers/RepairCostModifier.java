package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;

public class RepairCostModifier implements IPvEModifier {

	private int cost = 0;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		cost = entry.getInt("cost", 0);
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		cost = lootEntry.getInt("cost");
	}
*/
	@Override
	public ItemStack processItem(ItemStack item) {
		
		if (cost == 0)
			return item;
		
		try {
			Repairable meta = (Repairable)item.getItemMeta();
			meta.setRepairCost(cost);
			
			item.setItemMeta((ItemMeta) meta);
		} catch (Exception e) {}

		return item;
	}


	

}
