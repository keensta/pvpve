package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LoreModifier implements IPvEModifier {

	private List<String> lore = null;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("lore"))
			return;
		
		lore = entry.getStringList("lore");
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("lore"))
			return;
			
		try {
			List<String> loreArray = new ArrayList<String>();
			NBTTagList loreList = lootEntry.getList("lore");
			for (int i = 0; i < loreList.size(); i++) {
				loreArray.add(((NBTTagString)loreList.get(i)).data);
			}
			lore = loreArray;
		} catch (Exception e) {
			
		}
	}*/

	@Override
	public ItemStack processItem(ItemStack item) {
				
		if (lore == null)
			return item;
		
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lore);
		
		item.setItemMeta(meta);
		
		return item;
	}


	

}
