package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class BookModifier implements IPvEModifier {

	private String title = null;
	private String author = null;
	private List<String> pages = null;
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("title"))
			return;
		
		title = entry.getString("title");
		author = entry.getString("author");
		pages = entry.getStringList("pages");
	}
	
	/*@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("title"))
			return;
		
		title = lootEntry.getString("title");
		author = lootEntry.getString("author");
		
		if (lootEntry.hasKey("pages")) {
			try {
				NBTTagList pagesList = lootEntry.getList("pages");
				List<String> pagesArray = new ArrayList<String>();
				for (int i = 0; i < pagesList.size(); i++) {
					pagesArray.add( ((NBTTagString)pagesList.get(i)).data );
				}
				pages = pagesArray;
			} catch (Exception e) {
				
			}
		}
	}*/

	@Override
	public ItemStack processItem(ItemStack item) {
		
		if (title == null)
			return item;
		
		try {
			BookMeta meta = (BookMeta)item.getItemMeta();
			meta.setTitle(title);
			meta.setAuthor(author);
			meta.setPages(pages);
			
			item.setItemMeta(meta);
		} catch (Exception e) {}
		
		return item;
	}


	

}
