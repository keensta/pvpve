package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.lrns123.pvpve.util.Util;

public class EnchantModifier implements IPvEModifier {

	private HashMap<Enchantment, Integer> enchants = new HashMap<Enchantment, Integer>();
	
	@SuppressWarnings("deprecation")
    @Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("enchantments"))
			return;
		
		List<Map<?,?>> entries = entry.getMapList("enchantments");
		for (Map<?,?> enchantData : entries) {
			final ConfigurationSection cfg = Util.mapToConfiguration(enchantData);
			final int id = cfg.getInt("id", -1);
			final int lvl = cfg.getInt("lvl", 0);
			if (id < 0 || lvl <= 0)
				continue;
			
			enchants.put(Enchantment.getById(id), lvl);
		}

	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("ench"))
			return;
		
		NBTTagList enchantList = lootEntry.getList("ench");
		
		for (int i = 0; i < enchantList.size(); i++) {
			NBTTagCompound ench = null;
			try {
				ench = (NBTTagCompound)enchantList.get(i); 
			} catch (Exception e) {
				continue;
			}
			
			final int id = ench.getInt("id");
			final int lvl = ench.getInt("lvl");
			if (id < 0 || lvl <= 0)
				continue;
			
			enchants.put(Enchantment.getById(id), lvl);
		}		
	}*/

	@Override
	public ItemStack processItem(ItemStack item) {
		item.addUnsafeEnchantments(enchants);
		return item;
	}


	

}
