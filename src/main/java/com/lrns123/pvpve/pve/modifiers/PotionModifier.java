package com.lrns123.pvpve.pve.modifiers;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.lrns123.pvpve.util.Util;

public class PotionModifier implements IPvEModifier {

	private class PotionEffectEntry {
		public int id;
		public int amplifier;
		public int duration;
		
		PotionEffectEntry(int id, int amplifier, int duration) {
			this.id = id;
			this.amplifier = amplifier;
			this.duration = duration;
		}
	}
	
	private List<PotionEffectEntry> potionEffects = new ArrayList<PotionEffectEntry>();
		
	@Override
	public void loadConfiguration(ConfigurationSection entry) throws InvalidParameterException {
		
		if (!entry.contains("potionfx"))
			return;
		
		potionEffects.clear();
		
		List<Map<?,?>> potionfxList = entry.getMapList("potionfx");
		for (Map<?,?> potionfxMap : potionfxList) {
			ConfigurationSection potionfxEntry = Util.mapToConfiguration(potionfxMap);
			
			potionEffects.add(new PotionEffectEntry(potionfxEntry.getInt("id"), potionfxEntry.getInt("amplifier"), potionfxEntry.getInt("duration")));
		}
	}
	
/*	@Override
	public void loadNBTConfiguration(NBTTagCompound lootEntry) throws InvalidParameterException {
		if (!lootEntry.hasKey("potionfx"))
			return;
		
		potionEffects.clear();
		
		try {
			NBTTagList potionfxList = lootEntry.getList("potionfx");
			for (int i = 0; i < potionfxList.size(); i++) {
				NBTTagCompound potionfxEntry = (NBTTagCompound)potionfxList.get(i);
				
				potionEffects.add(new PotionEffectEntry(potionfxEntry.getInt("id"), potionfxEntry.getInt("amplifier"), potionfxEntry.getInt("duration")));
			}
		} catch (Exception e) {
			
		}
	}
*/
	@SuppressWarnings("deprecation")
    @Override
	public ItemStack processItem(ItemStack item) {
		
		try {
			PotionMeta meta = (PotionMeta)item.getItemMeta();
			
			for (PotionEffectEntry potionEntry : potionEffects) {
				meta.addCustomEffect(new PotionEffect(PotionEffectType.getById(potionEntry.id), potionEntry.duration, potionEntry.amplifier), true);
			}
			
			item.setItemMeta(meta);
		} catch (Exception e) {}
				
		return item;
	}


	

}
