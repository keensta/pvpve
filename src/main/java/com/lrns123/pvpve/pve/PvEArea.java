package com.lrns123.pvpve.pve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.lrns123.pvpve.pve.scoring.PlayerScoreCard;
import com.lrns123.pvpve.pve.scoring.ScoreboardSign;
import com.lrns123.pvpve.util.PolygonRegion;
import com.lrns123.pvpve.util.Region;
import com.lrns123.pvpve.util.RegionType;

public class PvEArea {
	
	private String name = "";
	private String world = null;
	private Region boundingbox = null;	
	private boolean protect = true;
	private boolean lockDispensers = false;
	private boolean blockCreeperFriendlyFire = false;
	private double mobDamageMultiplier = 1;
	private boolean noSleeping = false;
	private boolean noVehicles = false;
	private boolean noPearls = false;
	private boolean fireSpread = true;
	private boolean keepDeathItems = false;
	private PolygonRegion polyR = null;
	private RegionType regionT = RegionType.NONE;
	
	private int priority = 0;
	private boolean customRespawn = false;
	private Vector respawnLocation = null;
	private String respawnWorld = null;

	private ScoreboardSign scoreboard = null;
	private boolean TrackScore = false;
	private Region expBoundingbox = null;
	private HashMap<String, PvELootTable> lootTables = new HashMap<String, PvELootTable>();
	private List<PvEChest> lootChests = new ArrayList<PvEChest>();
	private Set<String> blockedCommands = new HashSet<String>();
	private List<PlayerScoreCard> scorecards = new ArrayList<PlayerScoreCard>();
	private boolean automaticRefill = true;
	
	private String lootSet = "";
	
	public String toString() {
		return "PvEArea(" + name + " in " + boundingbox + ", LootTables:" + lootTables + ", Chests: " + lootChests + ")";
	}

	public void setWorld(String world) {
		this.world = world;
	}

	public String getWorld() {
		return world;
	}

	public void setBoundingBox(Region boundingbox) {
		this.boundingbox = boundingbox;
	}

	public Region getBoundingBox() {
		return boundingbox;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setProtected(boolean protect) {
		this.protect = protect;
	}

	public boolean isProtected() {
		return protect;
	}
	
	public void addLootTable(String id, PvELootTable table)
	{
		lootTables.put(id, table);
	}
	
	public void addLootChest(PvEChest chest)
	{
		lootChests.add(chest);
	}
	
	public PvEChest[] getLootChests()
	{
		return lootChests.toArray(new PvEChest[0]);
	}
	
	public PvELootTable getLootTable(String id)
	{
		return lootTables.get(id);
	}

	public void setExpBoundingbox(Region expBoundingbox) {
		this.expBoundingbox = expBoundingbox;
	}

	public Region getExpBoundingbox() {
		return expBoundingbox;
	}

	public boolean isAutomaticRefill() {
		return automaticRefill;
	}

	public void setAutomaticRefill(boolean automaticRefill) {
		this.automaticRefill = automaticRefill;
	}

	public void addBlockedCommand(String cmd) {
		blockedCommands.add("/"  + cmd.toLowerCase());
	}
	
	public boolean isBlockedCommmand(String command) {
		String cmd = command.toLowerCase();
		for (String blockedCommand : blockedCommands) {
			if (cmd.startsWith(blockedCommand))
				return true;
		}
		return false;
	}

	public boolean getLockDispensers() {
		return lockDispensers;
	}
	
	public void setLockDispensers(boolean lockDispensers) {
		this.lockDispensers = lockDispensers;
	}

	public boolean getBlockCreeperFriendlyFire() {
		return blockCreeperFriendlyFire;
	}

	public void setBlockCreeperFriendlyFire(boolean blockCreeperFriendlyFire) {
		this.blockCreeperFriendlyFire = blockCreeperFriendlyFire;
	}

	public double getMobDamageMultiplier() {
		return mobDamageMultiplier;
	}

	public void setMobDamageMultiplier(double mobDamageMultiplier) {
		this.mobDamageMultiplier = mobDamageMultiplier;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public boolean getNoSleeping() {
		return noSleeping;
	}

	public void setNoSleeping(boolean noSleeping) {
		this.noSleeping = noSleeping;
	}

	public String getLootSet() {
		return lootSet;
	}

	public void setLootSet(String lootSet) {
		this.lootSet = lootSet;
	}

	public boolean hasCustomRespawn() {
		return customRespawn;
	}
	
	public void setCustomRespawn(Vector loc, String world) {
		if (loc != null && world != null) {
			customRespawn = true;
			respawnLocation = loc;
			respawnWorld = world;
		} else {
			customRespawn = false;
			respawnLocation = null;
			respawnWorld = null;
		}
	}
	
	public Location getRespawnLocation() {
		if (customRespawn) {
			return new Location(Bukkit.getWorld(respawnWorld), respawnLocation.getX(), respawnLocation.getY(), respawnLocation.getZ());
		} else {
			return null;
		}
	}
	
	public boolean getNoVehicles() {
		return noVehicles;
	}

	public void setNoVehicles(boolean noVehicles) {
		this.noVehicles = noVehicles;
	}
	
	public boolean getNoPearls() {
		return noPearls;
	}

	public void setNoPearls(boolean noPearls) {
		this.noPearls = noPearls;
	}
	
	public boolean getFireSpread() {
		return fireSpread;
	}

	public void setFireSpread(boolean fireSpread) {
		this.fireSpread = fireSpread;
	}
	
	public boolean getKeepDeathItems() {
		return keepDeathItems;
	}
	
	public void setKeepDeathItems(boolean keepDeathItems) {
		this.keepDeathItems = keepDeathItems;
	}
	
	public PolygonRegion getPolygonRegion() {
		return polyR;
	}
	
	public void setPolygonRegion(List<String> s) {
		this.polyR = new PolygonRegion(s);
	}

	public ScoreboardSign getScoreboard() {
		return scoreboard;
	}

	public void setScoreboard(ScoreboardSign scoreboard) {
		this.scoreboard = scoreboard;
	}

	public List<PlayerScoreCard> getScorecards() {
		return scorecards;
	}

	public void setScorecards(List<PlayerScoreCard> scorecards) {
		this.scorecards = scorecards;
	}
	
	public void addScoreCard(PlayerScoreCard scorecards) {
		this.scorecards.add(scorecards);
	}
	
	public void removeScoreCard(PlayerScoreCard scorecards) {
		this.scorecards.remove(scorecards);
	}
	
	public boolean hasScoreCard(PlayerScoreCard scorecards) {
		return this.scorecards.contains(scorecards);
	}

	public boolean isTrackScore() {
		return TrackScore;
	}

	public void setTrackScore(boolean TrackScore) {
		this.TrackScore = TrackScore;
	}
	
	public void setRegionType(RegionType rt) {
	    this.regionT = rt;
	}
	
	public RegionType getRegionType() {
	    return regionT;
	}
}
