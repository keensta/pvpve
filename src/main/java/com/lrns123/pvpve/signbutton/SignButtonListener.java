package com.lrns123.pvpve.signbutton;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Button;

import com.lrns123.pvpve.PvPvE;

public class SignButtonListener implements Listener {
	
	private Map<String, SignButtonHandler> handlers = new HashMap<String, SignButtonHandler>();
	
	private static BlockFace lateralFaces[] = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
	
	public SignButtonListener(PvPvE plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent ev) {
    	Block block = ev.getClickedBlock();
    	
    	if (block != null && block.getType() == Material.STONE_BUTTON) {
    		Block placedOn = block.getRelative(((Button)block.getState().getData()).getAttachedFace());
    		for (BlockFace face : lateralFaces) {
    			Block lateralBlock = placedOn.getRelative(face);
    			if (lateralBlock.getState() instanceof Sign) {
    				Sign sign = (Sign)lateralBlock.getState();
    				String tag = sign.getLine(0).toLowerCase();
    				if (handlers.containsKey(tag)) {
    					handlers.get(tag).buttonPressed(ev.getPlayer(), sign.getLines());
    				}
    			}
    		}
    	}
    }
    
    @EventHandler
    public void onSignChange(SignChangeEvent ev) {
    	if (handlers.containsKey(ev.getLine(0).toLowerCase())) {
   			if (!ev.getPlayer().hasPermission("pvpve.signbutton.create")) {
				ev.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to create sign buttons");
				ev.getBlock().breakNaturally();
				ev.setCancelled(true);
			}
   		}
    }
        
    public void registerHandler(String tag, SignButtonHandler handler) {
    	handlers.put(tag.toLowerCase(), handler);
    }
}
