package com.lrns123.pvpve.arena.arenatypes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.game.Game;

/**
 * Base class for arenas
 * 
 * @author Lourens
 * 
 */
public abstract class Arena {
	protected final String id;
	protected String name;
	protected String world;
	protected Location exitLocation;
	protected Set<String> supportedGameModes = new HashSet<String>();
	protected Game activeGame = null;

	public Arena(String id) {
		this.id = id;

	}
	
	protected double castToDouble(Object number) {
		if (number instanceof Integer) {
			return ((Integer)number).doubleValue();
		} else if (number instanceof Double) {
			return ((Double)number).doubleValue();
		}
		return (Double)number;
	}
	

	public boolean loadArena(ConfigurationSection cfg)
	{
		
		name = cfg.getString("name");
		world = cfg.getString("world");
		String exitWorld = cfg.getString("exitWorld");
		
		
		supportedGameModes.clear();
		List<String> gms = cfg.getStringList("gamemodes");
		if (gms != null) {
			for (String gm : gms) {
				supportedGameModes.add(gm.toLowerCase());
			}
		} else {	
			PvPvE.getInstance().getLogger().severe("No supported gamemodes specified for arena '" + id + "'");
		}
		
		List<Double> exitloc = cfg.getDoubleList("exitLocation");
		if (exitloc != null) {
			if (exitloc.size() == 3) {
				exitLocation = new Location(Bukkit.getWorld(exitWorld), exitloc.get(0), exitloc.get(1), exitloc.get(2));
			} else if (exitloc.size() == 4) {
				exitLocation = new Location(Bukkit.getWorld(exitWorld), exitloc.get(0), exitloc.get(1), exitloc.get(2), exitloc.get(3).floatValue(), 0);
			}
		} else {
			exitLocation = new Location(Bukkit.getWorld(world), 0, 0, 0);
			PvPvE.getInstance().getLogger().severe("No exit location specified for arena '" + id + "'");
		}
		
		return true;
	}
	
	
	/**
	 * Returns the human-friendly name of the arena 
	 * @return The arena name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return The internal ID of the arena
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @return the world
	 */
	public String getWorld() {
		return world;
	}
	
	/**
	 * @return The active game in this arena, null if no game is on-going.
	 */
	public Game getActiveGame() {
		return activeGame;
	}
	
	/**
	 * @param game The active game in this arena, null if no game is on-going.
	 */
	public void setActiveGame(Game game) {
		activeGame = game;
	}
	
	/**
	 * @return The active game in this arena, null if no game is on-going.
	 */
	public Location getExitLocation() {
		return exitLocation;
	}
	
	
	public boolean isSupportedGameMode(String gamemode) {
		return supportedGameModes.contains(gamemode);
	}
	/**
	 * Returns the minimal amount of players required in this arena
	 * @return
	 */
	public abstract int getMinPlayers();
	
	
	/**
	 * Returns the maximal amount of players allowed in this arena
	 * @return
	 */
	public abstract int getMaxPlayers();
	
	
	/**
	 * Returns the spawn locations to use for the specified team (if applicable)
	 * @param party
	 * @return
	 */
	public abstract Location[] getSpawnLocations(int team);
	
	/**
	 * Returns whether or not this area supports teams
	 * @return
	 */
	public abstract boolean supportsTeams();
	
	/**
	 * Returns the maximum number of teams this arena supports
	 * @return
	 */
	public abstract int getMaxTeams();

}
