package com.lrns123.pvpve.arena.arenatypes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class CombatArena extends Arena {
	
	protected int minPlayers;
	protected List<Location> spawnPoints = new ArrayList<Location>();

	public CombatArena(String id) {
		super(id);
	}
		
	@Override
	@SuppressWarnings("unchecked")
	public boolean loadArena(ConfigurationSection cfg)
	{
		super.loadArena(cfg);
	
		spawnPoints.clear();
		minPlayers = cfg.getInt("minPlayers");
		
		List<?> spawns = cfg.getList("spawns");
		if (spawns != null && spawns.size() > 0) {
			
			try {
				for (List<?> spawn : (List<List<?>>)spawns) {					
					if (spawn.size() == 3) {
						spawnPoints.add(new Location(Bukkit.getWorld(world), castToDouble((Object)spawn.get(0)), castToDouble((Object)spawn.get(1)), castToDouble((Object)spawn.get(2))));
					} else if (spawn.size() == 4) {
						spawnPoints.add(new Location(Bukkit.getWorld(world), castToDouble((Object)spawn.get(0)), castToDouble((Object)spawn.get(1)), castToDouble((Object)spawn.get(2)), (float)castToDouble((Object)spawn.get(3)), 0));
					}
				}
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
		}		
		return true;
	}

	@Override
	public int getMinPlayers() {
		return minPlayers;
	}

	@Override
	public int getMaxPlayers() {
		return spawnPoints.size();
	}

	@Override
	public Location[] getSpawnLocations(int team) {
		if (team != 0) {
			return new Location[0];
		}
		
		return spawnPoints.toArray( new Location[0] );
	}

	@Override
	public boolean supportsTeams() {
		return false;
	}

	@Override
	public int getMaxTeams() {
		return 0;
	}
	
	

}
