package com.lrns123.pvpve.arena.arenatypes;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class TeamCombatArena extends CombatArena {

	protected int minPlayers;
	protected int maxPlayers;
	protected List<List<Location>> spawns = new ArrayList<List<Location>>();
	
	public TeamCombatArena(String id) {
		super(id);
	}
	
	@Override
	public boolean loadArena(ConfigurationSection cfg)
	{
		super.loadArena(cfg);
		// TODO
		
		return true;
	}

	@Override
	public int getMinPlayers() {
		return minPlayers;
	}

	@Override
	public int getMaxPlayers() {
		return maxPlayers;
	}

	@Override
	public Location[] getSpawnLocations(int team) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean supportsTeams() {
		return true;
	}
	
	@Override
	public int getMaxTeams(){
		return spawns.size();
	}

}
