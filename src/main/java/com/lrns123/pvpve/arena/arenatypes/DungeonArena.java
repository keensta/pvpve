package com.lrns123.pvpve.arena.arenatypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class DungeonArena extends Arena {

	protected int minPlayers;
	protected List<Location> spawnPoints = new ArrayList<Location>();
	protected Map<String, DungeonMode> modes = new HashMap<String, DungeonMode>();
	protected String pveArea = null;
	
	public DungeonArena(String id) {
		super(id);
	}
	
	public class DungeonMode {
		private double mobDamageMultiplier;
		private int tokenReward;
		
		public double getMobDamageMultiplier() {
			return mobDamageMultiplier;
		}

		public int getTokenReward() {
			return tokenReward;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean loadArena(ConfigurationSection cfg)
	{
		super.loadArena(cfg);
		
		spawnPoints.clear();
		minPlayers = cfg.getInt("minPlayers");
		
		List<?> spawns = cfg.getList("spawns");
		if (spawns != null && spawns.size() > 0) {
			
			try {
				for (List<?> spawn : (List<List<?>>)spawns) {					
					if (spawn.size() == 3) {
						spawnPoints.add(new Location(Bukkit.getWorld(world), castToDouble((Object)spawn.get(0)), castToDouble((Object)spawn.get(1)), castToDouble((Object)spawn.get(2))));
					} else if (spawn.size() == 4) {
						spawnPoints.add(new Location(Bukkit.getWorld(world), castToDouble((Object)spawn.get(0)), castToDouble((Object)spawn.get(1)), castToDouble((Object)spawn.get(2)), (float)castToDouble((Object)spawn.get(3)), 0));
					}
				}
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
		}
		
		pveArea = cfg.getString("pveArea", "");
		
		modes.clear();
		if (cfg.contains("modes")) {
			Set<String> modeIds = cfg.getConfigurationSection("modes").getKeys(false);
			for (String modeId : modeIds) {
				ConfigurationSection modeCfg = cfg.getConfigurationSection("modes." + modeId);
				DungeonMode mode = new DungeonMode();
				mode.tokenReward = modeCfg.getInt("tokenReward", 0);
				mode.mobDamageMultiplier = modeCfg.getInt("mobDamageMultiplier", 0);
				
				modes.put(modeId, mode);
			}	
		}
		
		return true;
	}

	@Override
	public int getMinPlayers() {
	
		return minPlayers;
	}

	@Override
	public int getMaxPlayers() {

		return spawnPoints.size();
	}

	@Override
	public Location[] getSpawnLocations(int team) {
		if (team != 0) {
			return new Location[0];
		}
		
		return spawnPoints.toArray( new Location[0] );
	}

	@Override
	public boolean supportsTeams() {
		return false;
	}

	@Override
	public int getMaxTeams() {
		return 0;
	}
	
	public String getPvEArea() {
		return pveArea;
	}

	public DungeonMode getDungeonMode(String mode) {
		return modes.get(mode);
	}
	
	public Set<String> getDungeonModes() {
		return modes.keySet();
	}
}
