package com.lrns123.pvpve.arena;

import java.util.HashMap;
import java.util.Set;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.arena.arenatypes.Arena;
import com.lrns123.pvpve.arena.arenatypes.CombatArena;
import com.lrns123.pvpve.arena.arenatypes.DungeonArena;
import com.lrns123.pvpve.arena.arenatypes.TeamCombatArena;

/**
 * Arena Dueling Management System
 * @author Lourens
 *
 */
public class ArenaManager {

	private HashMap<String, Arena> arenas = new HashMap<String, Arena>();
	private final PvPvE plugin;
	
	public ArenaManager(PvPvE plugin)
	{
		this.plugin = plugin;
		
		loadArenas();
	}
	
	public void loadArenas() {
		arenas.clear();
		
		YamlConfiguration cfg = plugin.loadConfig("arenas.yml", true);
		if (cfg == null)
			return;
		
		if (cfg.contains("arenas")) {
			Set<String> arenas = cfg.getConfigurationSection("arenas").getKeys(false);
			for (String arenaId : arenas) {
				ConfigurationSection arenaCfg = cfg.getConfigurationSection("arenas." + arenaId);
				Arena arena;

				String type = arenaCfg.getString("type", "invalid");
				
				if (type.equalsIgnoreCase("combat")) {
					arena = new CombatArena(arenaId);
				} else if (type.equalsIgnoreCase("dungeon")) {
					arena = new DungeonArena(arenaId);
				} else if (type.equalsIgnoreCase("teamcombat")) {
					arena = new TeamCombatArena(arenaId);
				} else {
					plugin.getLogger().severe("Arena " + arenaId + " has invalid type '" + type + "'");
					continue;
				}

				plugin.getLogger().info("Loading arena " + arenaId);
				// Load arena-specific entries
				arena.loadArena(arenaCfg);
				
				this.arenas.put(arenaId, arena);
			}
		}
	}
	
	public Arena getArena(String name) {
		return arenas.get(name);
	}
}
