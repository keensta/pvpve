package com.lrns123.pvpve.game;

public enum GameModes {
	Duel,
	FFA,
	TFFA,
	DungeonCoop
}
