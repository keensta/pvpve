package com.lrns123.pvpve.game.exceptions;

public class GameException extends Exception {
	
	private static final long serialVersionUID = 4047325614231506454L;
	
	private String errorMsg;

	
	public GameException() {
		super();
		errorMsg = "unknown";
	}

	public GameException(String err) {
		super(err);
		errorMsg = err; // save message
	}

	public String getError() {
		return errorMsg;
	}
}
