package com.lrns123.pvpve.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.arena.ArenaManager;
import com.lrns123.pvpve.arena.arenatypes.Arena;
import com.lrns123.pvpve.game.exceptions.GameException;
import com.lrns123.pvpve.game.gamemodes.Duel;
import com.lrns123.pvpve.game.gamemodes.DungeonCoop;
import com.lrns123.pvpve.game.gamemodes.FFA;
import com.lrns123.pvpve.game.gamemodes.GameMode;
import com.lrns123.pvpve.game.gamemodes.TFFA;
import com.lrns123.pvpve.party.Party;
import com.lrns123.pvpve.party.PartyManager;
import com.lrns123.pvpve.party.PartyParticipant;
import com.lrns123.pvpve.party.vote.StartGameVote;
import com.lrns123.pvpve.signbutton.SignButtonHandler;

public class GameManager implements CommandExecutor, Listener {
	
	private final PvPvE plugin;
	private final PartyManager partyManager;
	private final ArenaManager arenaManager;
	
	private List<Game> games = new ArrayList<Game>(); 
	private HashMap<String, Game> gameByPlayer = new HashMap<String, Game>();
	
	
	public GameManager(PvPvE plugin) {
		this.plugin = plugin;
		this.partyManager = plugin.getPartyManager();
		this.arenaManager = plugin.getArenaManager();
		
		plugin.getCommand("game").setExecutor(this);
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		plugin.getSignButtonListener().registerHandler("[StartGame]", new SignButtonHandler() {
			
			@Override
			public void buttonPressed(Player activator, String[] lines) {
				if (!partyManager.hasParty(activator)) {
					activator.sendMessage(ChatColor.RED + "You must be in a party to use this button");
					return;
				}
				
				Party party = partyManager.getParty(activator);
				if (!party.getParticipant(activator).isLeader()) {
					// Send a request to the party instead
					for (PartyParticipant pcp : party.getParticipants()) {
						if (pcp.isOnline()) {
							pcp.getPlayer().sendMessage(ChatColor.AQUA + activator.getName() + ChatColor.BLUE + " has requested to play " + ChatColor.AQUA + lines[1] + ChatColor.BLUE + " in " + ChatColor.AQUA + lines[2]);
						}
					}
					return;
				}
				Arena arena = arenaManager.getArena(lines[2]);
				if (arena == null) {
					activator.sendMessage(ChatColor.RED + "Internal error: Invalid arena");
					return;
				}
				
				
				GameModes gm = null;
				try {
					gm = GameModes.valueOf(lines[1]);
				} catch (Exception e) {
					activator.sendMessage(ChatColor.RED + "Internal error: Invalid gamemode");
					return;
				}
				
				Game game = null;
				try {
					game = createGame(party, arena, gm, lines[3]);
				} catch (GameException e) {
					activator.sendMessage(ChatColor.RED + "Could not initialize the game: " + e.getError());
					return;
				}
				
				
				try {
					queueGame(game, false);
				} catch (GameException e) {
					activator.sendMessage(ChatColor.RED + "Could not queue the game: " + e.getError());
					return;
				}
			}
		});
		
		plugin.getSignButtonListener().registerHandler("[FinishGame]", new SignButtonHandler() {
			
			@Override
			public void buttonPressed(Player activator, String[] lines) {
							
				Party party = partyManager.getParty(activator);
				if (party == null) {
					activator.sendMessage(ChatColor.RED + "You must be the in a party to use this button");
					return;
				}
				
				Game game = party.getGame();
				if (game == null) {
					activator.sendMessage(ChatColor.RED + "You must be the in a game to use this button");
					return;
				}
				
				game.onFinishSign();
			}
		});
	}
	
	
	public Game createGame(Party party, Arena arena, GameModes gameMode, String extraArg) throws GameException {
		
		if (party == null) {
			throw new GameException("No party specified");
		}
		
		if (arena == null) {
			throw new GameException("No arena specified");
		}
		
		if (arena.getActiveGame() != null) {
			throw new GameException("Arena is already in use");
		}			

		Game game = new Game(this, plugin);
		game.setParty(party);
		game.setArena(arena);
		
		GameMode gm;
		switch (gameMode) {
		case Duel:
			gm = new Duel(game);
			break;
		case FFA:
			gm = new FFA(game);
			break;
		case TFFA:
			gm = new TFFA(game);
			break;
		case DungeonCoop:
			gm = new DungeonCoop(game, extraArg);
			break;
		default:
			throw new GameException("Invalid gamemode specified");
		}
		
		game.setGameMode(gm);

		return game;	
	}
	
	public void queueGame(Game game, boolean immediate) throws GameException {
		if (games.contains(game))
			return;
		
		Game active = game.getArena().getActiveGame();
		if (active != null && active != game) {
			throw new GameException("Arena is in use");
		}
		game.getArena().setActiveGame(game);
		
		game.getParty().setGame(game);
		
		games.add(game);
		for (PartyParticipant pcp : game.getParty().getParticipants()) {
			gameByPlayer.put(pcp.getName(), game);
		}
		
		if (!immediate) {
			if (game.getParty().hasPartyVote()) {
				throw new GameException("A vote is already in progress");
			}
			StartGameVote vote = new StartGameVote(game.getParty(), game, this, plugin);
			game.getParty().setPartyVote(vote);
			vote.queue();
		} else {
			startGame(game);
		}
	}
	
	public void shutdown() {
		// Abort all pending games
		for (Game game : games) {
			abortGame(game, true);
		}
	}
	
	public void startGame(Game game) {
		if (!games.contains(game)) {
			plugin.getLogger().severe("Tried to start unqueued game");
			return;
		}
		String error = game.startGame();
		if (error != null) {
			plugin.getLogger().severe("Failed to start game: " + error);
			abortGame(game, true);
		}
	}
	
	public void abortGame(Game game, boolean dispose) {
		game.abortGame();
		game.getArena().setActiveGame(null);
		if (dispose) disposeGame(game);
	}
	
	public void finishGame(Game game, boolean dispose) {
		game.finishGame();
		game.getArena().setActiveGame(null);
		if (dispose) disposeGame(game);
	}
	
	public void disposeGame(Game game) {
		games.remove(game);
		for (PartyParticipant pcp : game.getParty().getParticipants()) {
			gameByPlayer.remove(pcp.getName());
		}
		game.getParty().setGame(null);
	}
	
	public Game getPlayerGame(Player player) {
		return gameByPlayer.get(player.getName());
	}
	
	@EventHandler
	void onPlayerDeath(PlayerDeathEvent ev) {
		Player ply = ev.getEntity();
		
		Game game = getPlayerGame(ply);
		if (game == null)
			return;		
		
		game.onPlayerDeath(ply);
	}


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length < 1)
			return true;
		
		if (!sender.hasPermission("game.admin"))
			return true;
		
		if (args[0].equalsIgnoreCase("abortAll")) {
			for (Game game : games) {
				abortGame(game, false);
				game.getParty().setGame(null);
			}
			
			games.clear();
		}
		return true;
	}		
}
