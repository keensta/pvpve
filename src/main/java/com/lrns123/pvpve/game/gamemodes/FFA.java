package com.lrns123.pvpve.game.gamemodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.lrns123.pvpve.arena.arenatypes.Arena;
import com.lrns123.pvpve.arena.arenatypes.CombatArena;
import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.exceptions.GameException;
import com.lrns123.pvpve.party.Party;
import com.lrns123.pvpve.party.PartyParticipant;


public class FFA extends GameMode {
	private CombatArena arena;
	
	private boolean fightStarted = false;
	private int countdownTaskId = -1;
	
	
	private List<Player> players = new ArrayList<Player>();;
	//private List<ItemStack[]> invBackup = new ArrayList<ItemStack[]>;
	private Set<String> fighters = new HashSet<String>();
	private Map<String, Integer> stakes = new HashMap<String, Integer>();
	
	public FFA(Game game) throws GameException {
		super(game);
		
		final Party party = game.getParty();
		final Arena arena = game.getArena();
		
		if (party.getParticipantCount() < arena.getMinPlayers()) {
			throw new GameException("This arena needs at least " + arena.getMinPlayers() + " players.");
		}
		
		if (party.getParticipantCount() > arena.getMaxPlayers()) {
			throw new GameException("This arena does not support more than " + arena.getMaxPlayers() + " players.");
		}
		
		if (!(arena instanceof CombatArena)) {
			throw new GameException("FFA requires a combat arena");
		}
		
		if (!arena.isSupportedGameMode("ffa")) {
			throw new GameException("The arena does not support the FFA gamemode");
		}
		
		this.arena = (CombatArena)arena;
	}
		
	@Override
	public boolean canAttackEachother(OfflinePlayer attacker, OfflinePlayer victim) {
		Party party = game.getParty();
		
		if (party.getParticipant(attacker) != null && party.getParticipant(victim) != null) {
			return fightStarted;
		} else {
			return false;
		}
	}

	@Override
	public void onPlayerDeath(OfflinePlayer player) {
		if (player.isOnline()) {
			Player ply = player.getPlayer();
			
			if (fighters.contains(ply.getName())) {
				ply.sendMessage(ChatColor.RED + "You lost the match! Better luck next time.");
				fighters.remove(ply.getName());
			} else {
				return;
			}
			
			if (fighters.size() == 1) {
				String winner = fighters.iterator().next();
				Player winPly = Bukkit.getPlayerExact(winner);
				
				if (winner == null) {
					game.getManager().abortGame(game, true);
				} else {
					if (winPly.isOnline()) {
						winPly.sendMessage(ChatColor.AQUA + "Congratulations! You won the match!");
						
						Economy eco = game.getPlugin().getEconomy();
						
						if (eco != null) {
							int winnings = 0;
							for (Integer stake : stakes.values()) {
								winnings += stake;
							}
							
							winPly.sendMessage(ChatColor.BLUE + "You've won " + ChatColor.AQUA + winnings + ChatColor.BLUE + " " + (winnings == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()) + " from stakes");
							eco.depositPlayer(winPly.getName(), winnings );
						}
						
						winPly.sendMessage(ChatColor.BLUE + "You will be sent back to the hub in a few seconds");
						
						Bukkit.getScheduler().scheduleSyncDelayedTask(game.getPlugin(), new Runnable() {							
							@Override
							public void run() {
								game.getManager().finishGame(game, true);
							}
						}, 100);
					}
				}
			} else if (fighters.size() < 1) {
				// Failsafe, this should never happen
				game.getManager().abortGame(game, true);
			}
		}
	}

	@Override
	public void onStartGame() throws GameException {
		Location[] locs = arena.getSpawnLocations(0);
		PartyParticipant[] pcps = game.getParty().getOnlineParticipants();
		
		if (locs.length < pcps.length)
			throw new GameException("Cannot start game, insufficient spawn points");
		
		Economy eco = game.getPlugin().getEconomy();
		
		stakes.clear();
		fighters.clear();
		
		int i = 0;
		for (PartyParticipant pcp : pcps) {
			Player ply = pcp.getPlayer();
			ply.teleport(locs[i]);
			if (eco != null && game.getParty().getStake() != 0) {
				int balance = (int)eco.getBalance(pcp.getName());
				if (balance > game.getParty().getStake())
					balance =  game.getParty().getStake();
				
				ply.sendMessage(ChatColor.BLUE + "You've paid " + ChatColor.AQUA + balance + ChatColor.BLUE +" " + (balance == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()) + " as stake");
				eco.withdrawPlayer(ply.getName(), balance );
				
				stakes.put(pcp.getName(), balance);
			}
			ply.sendMessage(ChatColor.BLUE + "Fight starting in " + ChatColor.AQUA + "5" + ChatColor.BLUE + "...");
			//invBackup[i] = pcps[i].getPlayer().getInventory().getContents();
			players.add(ply);
			fighters.add(pcp.getName());
			i++;
		}
		
		game.setInProgress(true);
		fightStarted = false;
		
		// Countdown
		countdownTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(game.getPlugin(), new Runnable() {
			
			int countdown = 4;
			
			@Override
			public void run() {
				if (countdown <= 0) { 
					for (Player p : players) {
						p.sendMessage(ChatColor.BLUE + "FIGHT!");
						fightStarted = true;
						
						Bukkit.getScheduler().cancelTask(countdownTaskId);
						countdownTaskId = -1;
					}
				} else {
					for (Player p : players) {
						p.sendMessage(ChatColor.AQUA + "" + countdown + ChatColor.BLUE + "...");
					}
					countdown--;
				}
			}
			
		}, 20, 20);
	}

	@Override
	public void onAbortGame() {
		if (countdownTaskId != -1) {
			Bukkit.getScheduler().cancelTask(countdownTaskId);
		}
		
		if (game.inProgress()) {	
			for (Player player : players) {
				//players.getInventory().setContents(invBackup[i]);
				player.teleport(arena.getExitLocation());
				player.sendMessage(ChatColor.BLUE + "Game aborted, returning to the hub...");
				
				Economy eco = game.getPlugin().getEconomy();
				
				if (eco != null && stakes.containsKey(player)) {
					int stake = stakes.get(player);
					
					player.sendMessage(ChatColor.BLUE + "You've got your stake of  " + ChatColor.AQUA + stake + " " + (stake == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()) + " back");
					eco.depositPlayer(player.getName(), stake );
				}
			}
		} else {
			return;
		}
	}

	@Override
	public void onFinishGame() {
		if (countdownTaskId != -1) {
			Bukkit.getScheduler().cancelTask(countdownTaskId);
		}
		
		if (game.inProgress()) {
			for (Player player : players) {
				player.teleport(arena.getExitLocation());
				player.sendMessage(ChatColor.BLUE + "Returning to the hub...");
			}
		} else {
			return;
		}
	}

	@Override
	public void onFinishSign() {
		// Do nothing
	}
	
	@Override
	public String getName() {
		return "FFA";
	}
}
