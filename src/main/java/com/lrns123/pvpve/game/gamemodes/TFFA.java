package com.lrns123.pvpve.game.gamemodes;

import org.bukkit.OfflinePlayer;

import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.exceptions.GameException;
import com.lrns123.pvpve.party.Party;
import com.lrns123.pvpve.party.PartyParticipant;

public class TFFA extends GameMode {

	public TFFA(Game game) throws GameException {
		super(game);
		
		throw new GameException("Gamemode not yet supported");
	}

	@Override
	public boolean canAttackEachother(OfflinePlayer attacker, OfflinePlayer victim) {
		Party party = game.getParty();
		PartyParticipant att = party.getParticipant(attacker);
		PartyParticipant vic = party.getParticipant(victim);
		
		if (att != null && vic != null) {
			if (att.getTeam() != vic.getTeam()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public void onPlayerDeath(OfflinePlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartGame() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAbortGame() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinishGame() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onFinishSign() {
		// Do nothing
	}

	@Override
	public String getName() {
		return "TFFA";
	}

}
