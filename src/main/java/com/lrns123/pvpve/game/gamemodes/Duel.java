package com.lrns123.pvpve.game.gamemodes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lrns123.pvpve.arena.arenatypes.Arena;
import com.lrns123.pvpve.arena.arenatypes.CombatArena;
import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.exceptions.GameException;
import com.lrns123.pvpve.party.Party;
import com.lrns123.pvpve.party.PartyParticipant;

/**
 * Implements the mechanics for a duel
 * @author Lourens
 *
 */
public class Duel extends GameMode {
	
	private CombatArena arena;
	
	private boolean fightStarted = false;
	private int countdownTaskId = -1;
	
	
	private Player players[] = new Player[2];
	private ItemStack invBackup[][] = new ItemStack[2][];
	private Set<String> fighters = new HashSet<String>();
	private Map<String, Integer> stakes = new HashMap<String, Integer>();
	
	public Duel(Game game) throws GameException {
		super(game);
		
		final Party party = game.getParty();
		final Arena arena = game.getArena();
		
		if (party.getParticipantCount() != 2) {
			throw new GameException("Duel games require exactly two players");
		}
		
		if (!(arena instanceof CombatArena)) {
			throw new GameException("Dueling requires a combat arena");
		}
		
		if (!arena.isSupportedGameMode("duel")) {
			throw new GameException("The arena does not support the duel gamemode");
		}
		
		if (arena.getMaxPlayers() < 2) {
			throw new GameException("The arena does not support the required amount of players");
		}
		
		this.arena = (CombatArena)arena;
	}

	@Override
	public boolean canAttackEachother(OfflinePlayer attacker, OfflinePlayer victim) {
		Party party = game.getParty();
		
		if (party.getParticipant(attacker) != null && party.getParticipant(victim) != null) {
			return fightStarted;
		} else {
			return false;
		}
	}
	

	@Override
	public void onPlayerDeath(OfflinePlayer player) {
		if (player.isOnline()) {
			Player ply = player.getPlayer();
			
			if (fighters.contains(ply.getName())) {
				ply.sendMessage(ChatColor.RED + "You lost the match! Better luck next time.");
				fighters.remove(ply.getName());
			} else {
				return;
			}
			
			if (fighters.size() == 1) {
				String winner = fighters.iterator().next();
				Player winPly = Bukkit.getPlayerExact(winner);
				
				if (winner == null) {
					game.getManager().abortGame(game, true);
				} else {
					if (winPly.isOnline()) {
						winPly.sendMessage(ChatColor.AQUA + "Congratulations! You won the match!");
						
						Economy eco = game.getPlugin().getEconomy();
						
						if (eco != null) {
							int winnings = 0;
							for (Integer stake : stakes.values()) {
								winnings += stake;
							}
							
							winPly.sendMessage(ChatColor.BLUE + "You've won " + ChatColor.AQUA + winnings + ChatColor.BLUE + " " + (winnings == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()) + " from stakes");
							eco.depositPlayer(winPly.getName(), winnings );
						}
						
						winPly.sendMessage(ChatColor.BLUE + "You will be sent back to the hub in a few seconds");
						
						Bukkit.getScheduler().scheduleSyncDelayedTask(game.getPlugin(), new Runnable() {							
							@Override
							public void run() {
								game.getManager().finishGame(game, true);
							}
						}, 100);
					}
				}
			} else if (fighters.size() < 1) {
				// Failsafe, this should never happen
				game.getManager().abortGame(game, true);
			}
		}
	}

	@Override
	public void onStartGame() throws GameException {		
		Location[] locs = arena.getSpawnLocations(0);
		PartyParticipant[] pcps = game.getParty().getOnlineParticipants();
		
		if (locs.length < 2)
			throw new GameException("Cannot start game, insufficient spawn points");
		
		if (pcps.length != 2)
			throw new GameException("Cannot start game, incorrect number of online participants");
		
		Economy eco = game.getPlugin().getEconomy();
		
		stakes.clear();
		fighters.clear();
		
		for (int i = 0; i < 2; i++) {
			players[i] = pcps[i].getPlayer();
			players[i].teleport(locs[i]);
			if (eco != null && game.getParty().getStake() != 0) {
				int balance = (int)eco.getBalance(pcps[i].getName());
				if (balance > game.getParty().getStake())
					balance =  game.getParty().getStake();
				
				players[i].sendMessage(ChatColor.BLUE + "You've paid " + ChatColor.AQUA + balance + ChatColor.BLUE +" " + (balance == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()) + " as stake");
				eco.withdrawPlayer(players[i].getName(), balance );
				
				stakes.put(pcps[i].getName(), balance);
			}
			players[i].sendMessage(ChatColor.BLUE + "Fight starting in " + ChatColor.AQUA + "5" + ChatColor.BLUE + "...");
			invBackup[i] = pcps[i].getPlayer().getInventory().getContents();
			fighters.add(pcps[i].getName());
		}
		
		game.setInProgress(true);
		fightStarted = false;
		
		// Countdown
		countdownTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(game.getPlugin(), new Runnable() {
			
			int countdown = 4;
			
			@Override
			public void run() {
				if (countdown <= 0) { 
					for (Player p : players) {
						p.sendMessage(ChatColor.BLUE + "FIGHT!");
						fightStarted = true;
						
						Bukkit.getScheduler().cancelTask(countdownTaskId);
						countdownTaskId = -1;
					}
				} else {
					for (Player p : players) {
						p.sendMessage(ChatColor.AQUA + "" + countdown + ChatColor.BLUE + "...");
					}
					countdown--;
				}
			}
			
		}, 20, 20);
	}

	@Override
	public void onAbortGame() {
		if (countdownTaskId != -1) {
			Bukkit.getScheduler().cancelTask(countdownTaskId);
		}
		
		if (game.inProgress()) {	
			for (int i = 0; i < 2; i++) {
				players[i].getInventory().setContents(invBackup[i]);
				players[i].teleport(arena.getExitLocation());
				players[i].sendMessage(ChatColor.BLUE + "Game aborted, returning to the hub...");
				
				Economy eco = game.getPlugin().getEconomy();
				
				if (eco != null && stakes.containsKey(players[i])) {
					int stake = stakes.get(players[i]);
					
					players[i].sendMessage(ChatColor.BLUE + "You've got your stake of  " + ChatColor.AQUA + stake + " " + (stake == 1 ? eco.currencyNameSingular() : eco.currencyNamePlural()) + " back");
					eco.depositPlayer(players[i].getName(), stake );
				}
			}
		} else {
			return;
		}
	}

	@Override
	public void onFinishGame() {
		if (countdownTaskId != -1) {
			Bukkit.getScheduler().cancelTask(countdownTaskId);
		}
		
		if (game.inProgress()) {
			for (int i = 0; i < 2; i++) {
				players[i].teleport(arena.getExitLocation());
				players[i].sendMessage(ChatColor.BLUE + "Returning to the hub...");
			}
		} else {
			return;
		}
	}
	
	@Override
	public void onFinishSign() {
		// Do nothing
	}

	@Override
	public String getName() {
		return "Duel";
	}


}
