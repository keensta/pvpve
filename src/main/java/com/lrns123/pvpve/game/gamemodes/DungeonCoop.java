package com.lrns123.pvpve.game.gamemodes;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.lrns123.pvpve.arena.arenatypes.Arena;
import com.lrns123.pvpve.arena.arenatypes.DungeonArena;
import com.lrns123.pvpve.arena.arenatypes.DungeonArena.DungeonMode;
import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.exceptions.GameException;
import com.lrns123.pvpve.party.Party;
import com.lrns123.pvpve.party.PartyParticipant;
import com.lrns123.pvpve.party.vote.FinishGameVote;
import com.lrns123.pvpve.pve.PvEArea;

public class DungeonCoop extends GameMode {

	private DungeonArena dungeon;
	private DungeonMode mode;
	private Set<String> players = new HashSet<String>();

	private boolean everyoneDied = false; 
	
	public DungeonCoop(Game game, String mode) throws GameException {
		super(game);
				
		final Party party = game.getParty();
		final Arena arena = game.getArena();
		
		if (party.getParticipantCount() < arena.getMinPlayers()) {
			throw new GameException("This dungeon needs at least " + arena.getMinPlayers() + " players.");
		}
		
		if (party.getParticipantCount() > arena.getMaxPlayers()) {
			throw new GameException("This dungeon does not support more than " + arena.getMaxPlayers() + " players.");
		}
		
		if (!(arena instanceof DungeonArena)) {
			throw new GameException("Dungeon Coop requires a dungeon arena");
		}
		
		if (!arena.isSupportedGameMode("dungeoncoop")) {
			throw new GameException("The arena does not support the Dungeon Coop gamemode");
		}
		
		this.dungeon = (DungeonArena)arena;
		this.mode = dungeon.getDungeonMode(mode);
		if (mode == null)
			throw new GameException("Invalid mode specified");
	}

	@Override
	public boolean canAttackEachother(OfflinePlayer attacker, OfflinePlayer victim) {
		// No PvP in dungeons
		return false;
	}

	@Override
	public void onPlayerDeath(OfflinePlayer player) {
		if (player.isOnline()) {
			Player ply = player.getPlayer();
			
			if (players.contains(ply.getName())) {
				ply.sendMessage(ChatColor.RED + "You died! Better luck next time.");
				players.remove(ply.getName());
			} else {
				return;
			}
			
			if (players.size() < 1) {
				// Everyone died
				everyoneDied = true;
				game.getManager().finishGame(game, true);				
			}
		}		
	}

	@Override
	public void onStartGame() throws GameException {
		Location[] locs = dungeon.getSpawnLocations(0);
		PartyParticipant[] pcps = game.getParty().getOnlineParticipants();
		
		if (locs.length < pcps.length)
			throw new GameException("Cannot start game, insufficient spawn points");
		
		PvEArea pve = game.getPlugin().getPvEManager().getPvEArea(dungeon.getPvEArea());
		if (pve == null)
			throw new GameException("Cannot start game, cannot find dungeon");

		pve.setMobDamageMultiplier(mode.getMobDamageMultiplier());
		game.getPlugin().getPvEManager().refillArea(pve);
		
		players.clear();
		
		int i = 0;
		for (PartyParticipant pcp : pcps) {
			Player ply = pcp.getPlayer();
			ply.teleport(locs[i]);
			
			//invBackup[i] = pcps[i].getPlayer().getInventory().getContents();
			players.add(pcp.getName());
			i++;
		}
		
		game.setInProgress(true);	
	}

	@Override
	public void onAbortGame() {
		if (game.inProgress()) {	
			for (String playerName : players) {
				Player player = Bukkit.getPlayerExact(playerName);
				if (player == null)
					continue;
				//players.getInventory().setContents(invBackup[i]);
				player.teleport(dungeon.getExitLocation());
				player.sendMessage(ChatColor.BLUE + "Game aborted, returning to the hub...");
			}
		} else {
			return;
		}		
	}

	@Override
	public void onFinishGame() {
		if (game.inProgress()) {
			for (String playerName : players) {
				Player player = Bukkit.getPlayerExact(playerName);
				if (player == null)
					continue;
				
				player.teleport(dungeon.getExitLocation());
				if (everyoneDied)	// Probably wont get here if this is true
					continue;
				
				player.sendMessage(ChatColor.BLUE + "Dungeon completed! You have recieved " + ChatColor.AQUA + mode.getTokenReward() + ChatColor.BLUE + " tokens!");
				player.sendMessage(ChatColor.BLUE + "Returning to the hub...");
				
				game.getPlugin().getTokenManager().addTokens(playerName, mode.getTokenReward());
			}
		} else {
			return;
		}
		
	}
	
	@Override
	public void onFinishSign() {
		if (game.getParty().hasPartyVote()) {
			return;
		}
		FinishGameVote vote = new FinishGameVote(game.getParty(), game, game.getManager(), game.getPlugin());
		game.getParty().setPartyVote(vote);
		vote.queue();
	}

	@Override
	public String getName() {
		return "Dungeon Coop";
	}
	
}
