package com.lrns123.pvpve.game.gamemodes;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.lrns123.pvpve.game.Game;
import com.lrns123.pvpve.game.exceptions.GameException;

/**
 * Base class for all game modes
 * @author Lourens
 *
 */
public abstract class GameMode {
	protected final Game game;

	public GameMode(Game game) {
		this.game = game;
	}
	
	public abstract String getName();
	
	public abstract void onStartGame() throws GameException;
	public abstract void onAbortGame();
	public abstract void onFinishGame();
	
	public abstract void onFinishSign();
	
	public abstract boolean canAttackEachother(OfflinePlayer attacker, OfflinePlayer victim);
	public abstract void onPlayerDeath(OfflinePlayer player);
	
	
	public void ejectPlayer(Player player) {
		player.teleport(game.getArena().getExitLocation());
	}
}
