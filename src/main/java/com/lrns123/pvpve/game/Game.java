package com.lrns123.pvpve.game;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.lrns123.pvpve.PvPvE;
import com.lrns123.pvpve.arena.arenatypes.Arena;
import com.lrns123.pvpve.game.exceptions.GameException;
import com.lrns123.pvpve.game.gamemodes.GameMode;
import com.lrns123.pvpve.party.Party;

public class Game {
	
	private Party party;
	private Arena arena;
	private GameMode gameMode;
	private final GameManager manager;
	private final PvPvE plugin;
	
	public Game(GameManager manager, PvPvE plugin) {
		this.manager = manager;
		this.plugin = plugin;
	}
	
	private boolean pending = false;
	private boolean inProgress = false;

	void setParty(Party party) {
		this.party = party;
	}
	
	void setArena(Arena arena) {
		this.arena = arena;
	}
	
	void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
	}
	
	public Party getParty() {
		return party;
	}
	
	public Arena getArena() {
		return arena;
	}
	
	public GameMode getGameMode() {
		return gameMode;
	}
	
	public boolean isPending() {
		return pending;
	}
	
	public boolean inProgress() {
		return inProgress;
	}
	
	public void setPending(boolean pending) {
		this.pending = pending;
	}
	
	public void setInProgress(boolean inProgress) {
		this.inProgress = inProgress;
	}
	
	private boolean claimArena() {
		if (arena.getActiveGame() != null && arena.getActiveGame() != this) {
			return false;
		}
		arena.setActiveGame(this);
		return true;
	}
	
	public boolean canAttackEachother(OfflinePlayer attacker, OfflinePlayer victim) {
		return gameMode.canAttackEachother(attacker, victim);
	}
	
	public String startGame() {
		if (!claimArena())
			return "Arena in use";
		
		try {
			gameMode.onStartGame();
		} catch (GameException e) {
			return e.getError();
		}
		return null;
	}
	
	public void abortGame() {
		gameMode.onAbortGame();
	}
	
	public void finishGame() {
		gameMode.onFinishGame();
	}
	
	public void onPlayerDeath(Player player) {
		gameMode.onPlayerDeath(player);
	}
	
	public void onFinishSign() {
		gameMode.onFinishSign();
	}

	public GameManager getManager() {
		return manager;
	}

	public PvPvE getPlugin() {
		return plugin;
	}
}
